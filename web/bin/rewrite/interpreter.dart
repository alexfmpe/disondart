part of rewrite;

class Interpreter{
  static List<Module<ModuleFunction>> modules = [];

  static start(String path){}

  static xec(){
    var th = Thread.current;
    while(th.quanta > 0 && th.state == ThreadState.Pready){
      th.MP.code[th.PC]();
      th.PC++;
      th.quanta--;
    }

    if(th.state == ThreadState.Pready)
      Thread.ready.add(th);

    if(Thread.ready.isEmpty && Thread.asleep.isEmpty)
      return;

    while(Thread.ready.isEmpty)
      sleep(new Duration(milliseconds: 100));

    Thread.current = Thread.ready.first;
    Thread.current.quanta = Thread.PQUANTA;
  }

  static link(String path, int index){
    Module m = modules.firstWhere((m) => m.path == path, orElse: () => load(path));
    var MP = Thread.current.MP;
    MP.loaded.add(m);

    MP.imports[index].forEach((l) =>
        l.target = m.exports.firstWhere((e) =>
            l.name == e.name && l.hash == e.hash));
  }

  static load(String path) =>
      path[0] == r'$' ? new BuiltinModule(path) : new LibraryModule(path);
}

