part of rewrite;

typedef T Reader<T>(int);
typedef void Writer<T>(int, T);

abstract class Semantics<R, W>{
  Descriptor t;
  ByteData data;
  int get size => data.lengthInBytes;

  Semantics(this.data, [this.t]){
    if(t == null)
      t = new Descriptor(size, []);
  }
  R read();
  write(W w);
}

abstract class Atom<T> extends Semantics<T,T>{
  Atom (ByteData d) : super(d);
}
/////////////////////////////////////////////////////////////////////////////////
abstract class Number<T extends num> extends Atom<T> {
  Number  (ByteData d, this.reader, this.writer) : super(d);

  Reader<T> reader;
  Writer<T> writer;

  T read() => reader(0);
  write(T t) => writer(0, t);
}

abstract class Integer  extends Number<int> {
  Integer (ByteData d, Reader r, Writer w) : super(d,r,w);
}
abstract class Float    extends Number<double> {
  Float   (ByteData d, Reader r, Writer w) : super(d,r,w);
}


class Char   extends Integer { Char   (ByteData d) : super(d, d.getInt8,    d.setInt8)    ;}
class Short  extends Integer { Short  (ByteData d) : super(d, d.getInt16,   d.setInt16)   ;}
class Word   extends Integer { Word   (ByteData d) : super(d, d.getInt32,   d.setInt32)   ;}
class Long   extends Integer { Long   (ByteData d) : super(d, d.getInt64,   d.setInt64)   ;}
class Byte   extends Integer { Byte   (ByteData d) : super(d, d.getUint8,   d.setUint8)   ;}
class UShort extends Integer { UShort (ByteData d) : super(d, d.getUint16,  d.setUint16)  ;}
class UWord  extends Integer { UWord  (ByteData d) : super(d, d.getUint32,  d.setUint32)  ;}
class ULong  extends Integer { ULong  (ByteData d) : super(d, d.getUint64,  d.setUint64)  ;}
class Single extends Float   { Single (ByteData d) : super(d, d.getFloat32, d.setFloat32) ;}
class Double extends Float   { Double (ByteData d) : super(d, d.getFloat64, d.setFloat64) ;}

/////////////////////////////////////////////////////////////////////////////////
abstract class Indirect<T> extends Atom<T>{
  Word w;
  Indirect (ByteData d) : super(d){
    w = new Word(data);
  }
}

class Reference<T extends Dis_Object> extends Indirect<T>{
  static ReferenceMap map = new ReferenceMap();
  Reference(ByteData d) : super(d);

  T read() =>
      map.get(w.read());

  write(T obj) {
    updateRefs(read(), obj);
    w.write(obj.id);
  }

  updateRefs(oldObj, newObj){
    if(oldObj != null) oldObj.decRef();
    if(newObj != null) newObj.incRef();
  }
}

///ugly: subclass ByteData?
class Address extends Indirect<ByteData>{
  static AddressMap map = new AddressMap();
  Address(ByteData d) : super(d);

  ByteData read()   => map.get(w.read());
  write(ByteData b) => w.write(map.add(b));
}


/////////////////////////////////////////////////////////////////////////////////
abstract class Index<T> extends Indirect<T>{
  List<T> list;
  Index(ByteData d, this.list) : super(d);

  T read() => list[w.read()];
  write(T t) => w.write(list.indexOf(t));
}

class FramePointer extends Index<Frame>{
  FramePointer(ByteData d) : super(d, Thread.current.frames);
}
class ModulePointer extends Index<Module>{
  ModulePointer(ByteData d) : super(d, Thread.current.MP.loaded);
}

/////////////////////////////////////////////////////////////////////////////////
abstract class Description extends Indirect<Descriptor>{
  Description (ByteData d) : super(d);
  write(Descriptor t) => throw new UnsupportedError('');
}


class SizeDescription extends Description{
  SizeDescription(ByteData d) : super(d);
  Descriptor read() => new Descriptor(w.read(), []);
}


class TypeDescription extends Description{
  TypeDescription(ByteData d) : super(d);
  Descriptor read() => Thread.current.MP.descriptors[w.read()];
}
/////////////////////////////////////////////////////////////////////////////////
class Memory extends Semantics<ByteData, DescribedData>{
  Memory(ByteData d) : super(d);
  ByteData read() => data;
  write(DescribedData dd) => dd.writeTo(data);
}
/////////////////////////////////////////////////////////////////////////////////
class Trash extends Semantics{
  Trash(ByteData d) : super(d, null);
  read() => null;
  write(w) {}
}
var devNull = new Trash(new ByteData(0));
