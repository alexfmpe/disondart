part of rewrite;


abstract class Dis_Sequence<T> implements Dis_Object{
  int get length;
  T slice(int start, int end);
  operator [](int n);
}

abstract class Dis_Comparable<T extends Dis_Comparable> implements Comparable<T>, Dis_Object{
  int compareTo(T that);
  bool operator < (T that) => compareTo(that) <  0;
  bool operator > (T that) => compareTo(that) >  0;
  bool operator <=(T that) => compareTo(that) <= 0;
  bool operator >=(T that) => compareTo(that) >= 0;
  bool operator ==(T that) => compareTo(that) == 0;
}
