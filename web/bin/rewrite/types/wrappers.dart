part of rewrite;

class $<T extends Semantics>{
  T call(ByteData x) =>
      reflectClass(T).newInstance(new Symbol(''), [x]).reflectee;

  //for convenience
  int get size => call(null).size;
  Descriptor get t => call(null).t;
}

$<Char>     $c  = new $<Char>   ();
$<Short>    $s  = new $<Short>  ();
$<Word>     $w  = new $<Word>   ();
$<Long>     $l  = new $<Long>   ();
$<Byte>     $b  = new $<Byte>   ();
$<UShort>   $us = new $<UShort> ();
$<UWord>    $uw = new $<UWord>  ();
$<ULong>    $ul = new $<ULong>  ();
$<Single>   $r  = new $<Single> ();
$<Double>   $f  = new $<Double> ();

$<Memory>   $m  = new $<Memory> ();

$<Address>  $a  = new $<Address>();

$<SizeDescription>  $sd = new $<SizeDescription>();
$<TypeDescription>  $td = new $<TypeDescription>();

$<FramePointer>   $FP = new $<FramePointer>();
$<ModulePointer>  $MP = new $<ModulePointer>();


$<Reference<Dis_Object>>  $p  = new $<Reference<Dis_Object>>();

$<Reference<Dis_List>>    $L  = new $<Reference<Dis_List>>();
$<Reference<Dis_Array>>   $A  = new $<Reference<Dis_Array>>();
$<Reference<Dis_String>>  $S  = new $<Reference<Dis_String>>();
$<Reference<Dis_Channel>> $C  = new $<Reference<Dis_Channel>>();
