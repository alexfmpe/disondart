part of rewrite;


abstract class Dis_Object{
  int id;
  int refs = 0;

  static compareTypes(Dis_Object s, Dis_Object d){
      if(s == null) return;
      if(d == null) Thread.current.error(exTcheck);
      if(s.runtimeType != d.runtimeType) Thread.current.error(exTcheck);
    }

  Dis_Object(){
    id = Reference.map.add(this);
  }

  incRef() =>
      refs++;

  decRef() {
    refs--;
    if(refs < 1)
      destroy();
  }

  destroy(){
    Reference.map.remove(this);
    cleanup();
  }

  cleanup(){}
}

class Dis_ADT extends Dis_Object{
  DescribedData data;
  Dis_ADT(Descriptor t){
    data = new DescribedData(t);
  }

  cleanup() =>
    data.clear();
}



class Dis_String extends Dis_Object with Dis_Sequence<Dis_String>, Dis_Comparable<Dis_String>{
  String text;
  Dis_String(this.text);

  Dis_String beget(String s) =>
      new Dis_String(s);

  operator [](int n) =>
      text.codeUnitAt(n);

  operator []=(int n, int code) =>
      beget(text.substring(0,n) + new String.fromCharCode(n) + text.substring(n+1));

  operator + (Dis_String that) =>
      beget(text + that.text);

  int compareTo(Dis_String that) =>
      this.text.compareTo(that.text);

  int get length =>
      text.length;

  Dis_String slice(int s, int e) =>
      beget(text.substring(s,e));

  Dis_Array asArray() =>
      new Dis_Array.fromString(text);
}



class Dis_List extends Dis_Object with Dis_Sequence<Dis_List>{
  Dis_List tail;
  DescribedData data;
  Dis_List(Descriptor t, ByteData d, [this.tail = null]){
    data = new DescribedData(t,d).copy();
    if(tail != null)
      tail.refs++;
  }

  ///necessary to receive descriptor?
  ///can compiler retrieve byte from list of another type?
  DescribedData head([Descriptor t]) =>
      data.copy();

  int get length =>
      (tail == null) ? 1 : 1 + tail.length;

  Dis_List slice(int s, [int e = -1]){
      if(e != -1)
        throw new UnimplementedError();
      return (s == 0) ? this : tail.slice(s-1);
  }

  operator [](int n) =>
      slice(n).head();

  cleanup(){
    tail.decRef();
    data.clear();
  }
}


///bytedata filled with zeros by default
///no need to initialize pointers
class Dis_Array extends Dis_Object with Dis_Sequence<Dis_Array>{
  int length;
  Descriptor t;
  Dis_Array root;
  List<DescribedData> contents;

  Dis_Array(this.t, this.length){
    contents = new List<DescribedData>.generate(length, (int i) => new DescribedData(t));
  }

  Dis_Array.view(this.t, this.root, int start, int end){
    contents = root.contents.sublist(start, end);
    length = contents.length;
    if(root.root != null)
      root = root.root;
    root.refs++;
  }

  factory Dis_Array.fromString(String text){
    var utf = UTF8.encode(text);
    return new Dis_Array($c.t, utf.length)..writeBytes(utf);
  }

  slice(int start, int end) =>
      new Dis_Array.view(t, this, start, end);

  replace(Dis_Array from, int start) =>
      range(0, from.length).map((i) =>
          contents[start + i] = from.contents[i].copy());

  int operator[](int i) =>
      contents[i ~/ t.size][i % t.size];

  operator[]= (int i, int value) =>
      contents[i ~/ t.size][i % t.size] = value;

  void writeBytes(List<int> bytes, [int start = 0]) =>
      range(0, bytes.length).forEach((i) =>
          this[i] = bytes[i]);

  List<int> asBytes() =>
      contents.map((d) => d[0]).toList();

  Dis_String asString() =>
      new Dis_String(UTF8.decode(asBytes()));

  cleanup() =>
      contents.map((d) => d.clear());
}



class Dis_Channel extends Dis_Object{
  Descriptor t;
  ThreadGroup senders   = new ThreadGroup(ThreadState.Psend),
              receivers = new ThreadGroup(ThreadState.Precv);

  Dis_Channel(this.t);

  Thread get invoker =>
      Thread.current;

  transfer(Thread from, Thread to, bool insideSend){
      $m(to.receiving).write(from.sending);
      Thread.ready.add(insideSend ? receivers.first : senders.first);
  }

  send(ByteData b){
    invoker.sending = new DescribedData(t, b).copy();
    receivers.isEmpty ? senders.add(invoker)
                      : transfer(invoker, receivers.first, true);
  }

  receive(ByteData b){
    invoker.receiving = b;
    senders.isEmpty   ? receivers.add(invoker)
                      : transfer(senders.first, invoker, false);
  }
}
