part of rewrite;

/// Potencial improvement: convert these functions to callable objects
/// Then we can relate argument types through type variables
/// As of now, passing incoherent argument types yields a runtime error

///////////////////////////////////////////////////////////////////////////////
I__wrap1( $<Semantics> w1,
          f(Semantics n1)) =>
    (ByteData b1) => f(w1(b1));

I__wrap2( $<Semantics> w1, $<Semantics> w2,
          f(Semantics n1, Semantics n2)) =>
    (ByteData b1, ByteData b2) => f(w1(b1), w2(b2));

I__wrap3( $<Semantics> w1, $<Semantics> w2, $<Semantics> w3,
          f(Semantics n1, Semantics n2, Semantics n3)) =>
    (ByteData b1, ByteData b2, ByteData b3) => f(w1(b1), w2(b2), w3(b3));
///////////////////////////////////////////////////////////////////////////////
I__op1($ w1, f(x), {int storeAt : 1}) =>
    I__wrap1(w1, (Semantics s1) =>
        [devNull, s1][storeAt].write(f(s1.read())));

I__op2($ w1, $ w2, f(x,y), {int storeAt : 2}) =>
    I__wrap2(w1, w2, (Semantics s1, Semantics s2) =>
        [devNull, s1, s2][storeAt].write(f(s1.read(), s2.read())));

I__op3($ w1, $ w2, $ w3, f(x,y,z), {int storeAt : 3}) =>
    I__wrap3(w1, w2, w3, (Semantics s1, Semantics s2, Semantics s3) =>
        [devNull, s1, s2, s3][storeAt].write(f(s1.read(), s2.read(), s3.read())));
/////////////////////////////////////////////////////////////////////////////////
// feedN      : z <- f(a,b,..,y)
// feedbackN  : z <- f(a,b,..,z)
// nofeedN    : _ <- f(a,b,..,z)

I__feed1($ w1, f()) =>
    I__op1(w1, (x) => f());

I__feed2($ w1, $ w2, [f(x) = identity]) =>
    I__op2(w1, w2, (x,y) => f(x));

I__feed3($ w1, $ w2, $ w3, f(x,y)) =>
    I__op3(w1, w2, w3, (x,y,z) => f(x,y));


I__feedback1($ w1, f(x)) =>
    I__op1(w1, f);

I__feedback2($ w1, $ w2, f(x,y)) =>
    I__op2(w1, w2, f);

I__feedback3($ w1, $ w2, $ w3, f(x,y,z)) =>
    I__op3(w1, w2, w3, f);


I__nofeed1($ w1, f(x)) =>
    I__op1(w1, f, storeAt:0);

I__nofeed2($ w1, $ w2, f(x,y)) =>
    I__op2(w1, w2, f, storeAt:0);

I__nofeed3($ w1, $ w2, $ w3, f(x,y,z)) =>
    I__op3(w1, w2, w3, f, storeAt:0);


/////////////////////////////////////////////////////////////////////////////////
I__alu($<Number> w, num f(num x, num y)) =>
    I__feed3(w,w,w,f);

I__exp($<Integer> ws, $<Number> wm, $<Number> wd) =>
    I__feed3(ws, wm, wd, pow);

I__len($<Reference<Dis_Sequence>> w) =>
    I__feed2(w, $w, (Dis_Sequence s) =>
        s.length);

I__slice($<Reference<Dis_Sequence>> w) =>
    I__feedback3($w, $w, w, (int s, int m, Dis_Sequence d) =>
        d.slice(s, m));

I__moveAtom($<Atom> w) =>
    I__feed2(w, w);

I__moveMemory($<Description> w) =>
    I__feed3($m, w, $m, (ByteData s, Descriptor m) =>
        new DescribedData(m, s));

I__consAtom($<Atom> w) =>
    I__feedback2($m, $L, (ByteData s, Dis_List d) =>
        new Dis_List(w.t, s, d));

I__consMemory($<Description> w) =>
    I__feedback3($m, w, $L, (ByteData s, Descriptor m, Dis_List d) =>
        new Dis_List(m, s, d));

I__headAtom($<Atom> w) =>
    I__feed2($L, $m, (Dis_List s) =>
        s.head(w.t));

I__headMemory($<Description> w) =>
    I__feed3($L, w, $m, (Dis_List s, Descriptor m) =>
        s.head(m));

I__channelAtom($<Atom> w) =>
    I__feed1($C, () =>
        new Dis_Channel(w.t));

I__channelMemory($<Description> w) =>
    I__feed2(w, $C, (Descriptor s) =>
        new Dis_Channel(s));

I__index($<Reference<Dis_Sequence>> ws, $ wd) =>
    I__feed3(ws, $w, wd, (Dis_Sequence s, int m) =>
        s[m]);

I__branch($<Atom<Comparable>> w, Comparison f) =>
    I__nofeed3(w, w, $w, (Comparable s, Comparable m, int d) =>
        Thread.current.jump(d, f(s,m)));
