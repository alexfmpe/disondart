part of rewrite;

Function

  I_movb    = I__moveAtom($b),
  I_movw    = I__moveAtom($w),
  I_movl    = I__moveAtom($l),
  I_movf    = I__moveAtom($f),
  I_movp    = I__moveAtom($p),
  I_movm    = I__moveMemory($sd),
  I_movmp   = I__moveMemory($td),

  I_lea     = I__feed2($m, $a),

  I_cvtwb   = I__feed2($w, $b),
  I_cvtws   = I__feed2($w, $s),
  I_cvtwl   = I__feed2($w, $l),

  I_cvtbw   = I__feed2($b, $w),
  I_cvtsw   = I__feed2($s, $w),
  I_cvtlw   = I__feed2($l, $w),

  I_cvtfr   = I__feed2($f, $r),
  I_cvtrf   = I__feed2($r, $f),

  I_cvtwf   = I__feed2($w, $f, (int s) => s.toDouble()),
  I_cvtlf   = I__feed2($l, $f, (int s) => s.toDouble()),

  I_cvtfw   = I__feed2($f, $w, (double s) => s.round()),
  I_cvtfl   = I__feed2($f, $l, (double s) => s.round()),

  I_cvtwc   = I__feed2($w, $S, (int s)    => new Dis_String(s.toString())),
  I_cvtlc   = I__feed2($l, $S, (int s)    => new Dis_String(s.toString())),
  I_cvtfc   = I__feed2($f, $S, (double s) => new Dis_String(s.toString())),

  I_cvtcw   = I__feed2($S, $w, (Dis_String s) => int.parse(s.text)),
  I_cvtcl   = I__feed2($S, $l, (Dis_String s) => int.parse(s.text)),
  I_cvtcf   = I__feed2($S, $f, (Dis_String s) => double.parse(s.text)),

  I_cvtca   = I__feed2($S, $A, (Dis_String s) => s.asArray()),
  I_cvtac   = I__feed2($A, $S, (Dis_Array  s) => s.asString()),


  I_addb    = I__alu($b,	add),
  I_addw    = I__alu($w,	add),
  I_addl    = I__alu($l,	add),
  I_addf    = I__alu($f,	add),

  I_subb    = I__alu($b,	sub),
  I_subw    = I__alu($w,  sub),
  I_subl    = I__alu($l,	sub),
  I_subf    = I__alu($f,	sub),

  I_mulb    = I__alu($b,	mul),
  I_mulw    = I__alu($w,	mul),
  I_mull    = I__alu($l,	mul),
  I_mulf    = I__alu($f,	mul),

  I_divb    = I__alu($b,	quot),
  I_divw    = I__alu($w,	quot),
  I_divl    = I__alu($l,	quot),
  I_divf    = I__alu($f,	div),

  I_andb    = I__alu($b,	and),
  I_andw    = I__alu($w,	and),
  I_andl    = I__alu($l,	and),

  I_orb     = I__alu($b,	or),
  I_orw     = I__alu($w,	or),
  I_orl     = I__alu($l,	or),

  I_xorb    = I__alu($b,	xor),
  I_xorw    = I__alu($w,	xor),
  I_xorl    = I__alu($l,	xor),

  I_shlb    = I__alu($b,	shl),
  I_shlw    = I__alu($w,	shl),
  I_shll    = I__alu($l,	shl),

  I_shrb    = I__alu($b,	shr),
  I_shrw    = I__alu($w,	shr),
  I_shrl    = I__alu($l,	shr),

  I_lsrw    = I__alu($uw, shr),
  I_lsrl    = I__alu($ul, shr),

  I_expw    = I__exp($w, $w, $w),
  I_expl    = I__exp($w, $l, $l),
  I_expf    = I__exp($w, $f, $f),

  I_negf    = I__feedback1($f, neg),


  I_lenc    = I__len($S),
  I_lena    = I__len($A),
  I_lenl    = I__len($L),
  I_slicec  = I__slice($S),
  I_slicea  = I__slice($A),
  I_indc    = I__index($S, $w),
  I_indx    = I__index($A, $a),


  I_headb   = I__headAtom($b),
  I_headw   = I__headAtom($w),
  I_headl   = I__headAtom($l),
  I_headf   = I__headAtom($f),
  I_headp   = I__headAtom($f),
  I_headm   = I__headMemory($sd),
  I_headmp  = I__headMemory($td),

  I_consb   = I__consAtom($b),
  I_consw   = I__consAtom($w),
  I_consl   = I__consAtom($l),
  I_consf   = I__consAtom($f),
  I_consp   = I__consAtom($p),
  I_consm   = I__consMemory($sd),
  I_consmp  = I__consMemory($td),

  I_tail    = I__feed2($L, $L, (Dis_List s) => s.tail),


  I_addc    = I__feed3($S, $S, $S,   (Dis_String s, Dis_String m) => s + m),
  I_insc    = I__nofeed3($w, $w, $S, (int s, int m, Dis_String d) => d[m] = s),
  I_slicela = I__nofeed3($A, $w, $A, (Dis_Array s, int m, Dis_Array d) => s.replace(d, m)),


  I_new     = I__feed2($td, $p,     (Descriptor s) => new Dis_ADT(s)),
  I_newa    = I__feed3($w, $td, $A, (int s, Descriptor m) => new Dis_Array(m, s)),
  I_mnewz   = I__feed3($MP, $w, $p, (Module s, int m) => new Dis_ADT(s.descriptors[m])),

  I_newcb   = I__channelAtom($b),
  I_newcw   = I__channelAtom($w),
  I_newcl   = I__channelAtom($l),
  I_newcf   = I__channelAtom($f),
  I_newcp   = I__channelAtom($p),
  I_newcm   = I__channelMemory($sd),
  I_newcmp  = I__channelMemory($td),

  I_send    = I__nofeed2($a, $C,  (ByteData s, Dis_Channel d) => d.send(s)),
  I_recv    = I__nofeed2($C, $a,  (Dis_Channel s, ByteData d) => s.receive(d)),


  I_jmp     = I__nofeed1($w, Thread.current.jump),

  I_frame   = I__feed2  ($td, $FP, Thread.current.frame),
  I_call    = I__nofeed2($FP, $w,  Thread.current.call),
  I_spawn   = I__nofeed2($FP, $w,  Thread.current.spawn),

  I_mframe  = I__feed3  ($MP, $w, $FP, Thread.current.mframe),
  I_mcall   = I__nofeed3($FP, $w, $MP, Thread.current.mcall),
  I_mspawn  = I__nofeed3($FP, $w, $MP, Thread.current.mspawn),

  I_ret     = Thread.current.ret,
  I_exit    = Thread.current.error,
  I_nop     = (){},

  I_raise   = I__nofeed1($S, (Dis_String s) => Thread.current.error(s.text)),
  I_tcmp    = I__nofeed2($p, $p, Dis_Object.compareTypes),

  I_self    = I__feed1($MP, () => Thread.current.MP),

  I_load    = I__feed3($S, $w, $MP, (Dis_String s, int index) => Interpreter.link(s.text, index)),


  I_beqb    = I__branch($b, eq),
  I_beqw    = I__branch($w, eq),
  I_beql    = I__branch($l, eq),
  I_beqf    = I__branch($f, eq),
  I_beqc    = I__branch($S, eq),

  I_bneb    = I__branch($b, ne),
  I_bnew    = I__branch($w, ne),
  I_bnel    = I__branch($l, ne),
  I_bnef    = I__branch($f, ne),
  I_bnec    = I__branch($S, ne),

  I_bgeb    = I__branch($b, ge),
  I_bgew    = I__branch($w, ge),
  I_bgel    = I__branch($l, ge),
  I_bgef    = I__branch($f, ge),
  I_bgec    = I__branch($S, ge),

  I_bgtb    = I__branch($b, gt),
  I_bgtw    = I__branch($w, gt),
  I_bgtl    = I__branch($l, gt),
  I_bgtf    = I__branch($f, gt),
  I_bgtc    = I__branch($S, gt),

  I_bleb    = I__branch($b, le),
  I_blew    = I__branch($w, le),
  I_blel    = I__branch($l, le),
  I_blef    = I__branch($f, le),
  I_blec    = I__branch($S, le),

  I_bltb    = I__branch($b, lt),
  I_bltw    = I__branch($w, lt),
  I_bltl    = I__branch($l, lt),
  I_bltf    = I__branch($f, lt),
  I_bltc    = I__branch($S, lt),


  I_movpc   = I_movw,
  I_indb    = I_indx,
  I_indw    = I_indx,
  I_indl    = I_indx,
  I_indf    = I_indx,
  I_newz    = I_new,
  I_newaz   = I_newa,
  I_runt    = I_nop;
