library rewrite;

import 'dart:io';
import 'dart:math';
import 'dart:convert';
import 'dart:mirrors';
import 'dart:collection';
import 'dart:typed_data';

part 'tests.dart';
part 'utils.dart';
part 'frames.dart';
part 'threads.dart';
part 'modules.dart';
part 'resources.dart';
part 'exceptions.dart';
part 'interpreter.dart';
part 'descriptions.dart';

part 'instructions/isa.dart';
part 'instructions/patterns.dart';

part 'types/mixins.dart';
part 'types/builtin.dart';
part 'types/wrappers.dart';
part 'types/semantics.dart';

part 'builtin/math/mathmod.dart';
part 'builtin/sys/sysmod.dart';
part 'builtin/sys/formats.dart';
part 'builtin/sys/structs.dart';
part 'builtin/sys/implementations.dart';


other(){
  test1();
  print('');
  print('');
  test2();
  print('');
  print('');
  test3();
}
