part of rewrite;

class ResourceMap<R>{
  int count = 1;
  Map<int, R> objs = {};

  R get(int n) => objs[n];
  R set(int n, R r){
    var prev = get(n);
    objs[n] = r;
    return prev;
  }

  add(R r){
    set(count, r);
    return count++;
  }

  toString() =>
      '$objs';
}

class ReferenceMap extends ResourceMap<Dis_Object>{
  remove(Dis_Object r){
      objs.remove(r.id);
  }
}

class AddressMap extends ResourceMap<ByteData>{}
