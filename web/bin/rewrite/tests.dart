part of rewrite;

test1(){
  ByteData b;
  show(ByteData b) => print(b.buffer.asUint8List());
  view(x,y) => new ByteData.view(b.buffer, x,y);

  b = new ByteData(16)
    ..setInt32(0, 2)
    ..setInt32(4, 5);

  show(b);
  I_addw(view(0, 4), view(4, 4), view(8, 4));
  I_expw(view(0, 4), view(4, 4), view(12, 4));
  show(b);

  print('');
  print('');
/////////////////////////////////////////////////
  b = new ByteData(24)
    ..setFloat64(0, 0.5);

  show(b);
  I_negf(view(0, 8));
  I_negf(view(16, 8));
  show(b);

  print(b.getFloat64(0));
  print(b.getFloat64(16));
}
/////////////////////////////////////////////////

test2(){
  ByteData b;
  show(ByteData b) => print(b.buffer.asUint8List().map((x) => x.toRadixString(16).padLeft(1)));
  view(x,y) => new ByteData.view(b.buffer, x,y);

/////////////////////////////////////////////////
  b = new ByteData(16)
      ..setUint32(0, 255);

  var w1 = view( 0, 4);
  var w2 = view( 4, 4);
  var ws = view( 8, 4);
  var wn = view(12, 4);

  show(b);
  I_cvtwc(w1, ws);
  I_cvtcw(ws, w2);
  I_lenc(ws, wn);
  show(b);

  var ds1 = $S(ws).read();
  print(ds1.text);
/////////////////////////////////////////////////
  b = new ByteData(24)
  ..setFloat64(0, -4.5);

  var f1 = view( 0, 8);
  var f2 = view( 8, 8);
  var fs = view(16, 4);
  var fn = view(20, 4);

  show(b);
  I_cvtfc(f1, fs);
  I_cvtcf(fs, f2);
  I_lenc(fs, fn);
  show(b);

  var ds2 = $S(fs).read();
  print(ds2.text);
/////////////////////////////////////////////////
  print(Reference.map);

}

test3(){
  ByteData b;
  show(ByteData b) => print(b.buffer.asUint8List().map((x) => x.toRadixString(10).padLeft(1)));
  view(x,y) => new ByteData.view(b.buffer, x,y);

  b = new ByteData(24)
      ..setFloat64( 0, -4.5)
      ..setUint32 (16,    2);

  var f1 = view( 0, 8);
  var fs = view( 8, 4);
  var fc = view(12, 4);
  var n  = view(16, 4);

  show(b);
  I_cvtfc(f1, fs);
  I_indc(fs, n, fc);
  //I_movm(fs, n, fc);
  show(b);

  print($S(fs).read().text);
  print(new String.fromCharCode($w(fc).read()));

  print(Reference.map);
  var str = $S(fs).read();
  str.decRef();
  print(Reference.map);

  //print(runtabs);
}

