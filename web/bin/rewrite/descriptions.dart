part of rewrite;


class Descriptor{
  int size;
  List<int> pointerMap;
  Descriptor(this.size, [this.pointerMap = const []]);
  factory Descriptor.hexmap(int size, String pmap) =>
      new Descriptor(size, changeBase(16,2,pmap).padRight(size ~/ 4, '0').split('').map(int.parse).toList());
}

class DescribedData{
  Descriptor t;
  ByteData data;

  DescribedData(this.t, [this.data]){
    if(data == null)
      data = new ByteData(t.size);
  }
  factory DescribedData.from(ByteData data) =>
      new DescribedData(new Descriptor(data.lengthInBytes), data);

  DescribedData view([int start = 0, int len = -1]) =>
      new DescribedData(t, byteView(data, start, len % t.size));

  DescribedData copy([int start = 0, int len = -1]){
    len = len % t.size;
    var d = new DescribedData(t, new ByteData(len));
    return d..writeFrom(this, len);
  }

  ///bug: overlaps
  writeFrom(DescribedData that, [int len = -1]){
    len = len % t.size;
    for(var i = 0; i < t.size/4; i++){
      $<Semantics> semantics = [$w, $p][t.pointerMap[i]];
      Semantics w = semantics(this.nthWord(i)),
                r = semantics(that.nthWord(i));
      w.write(r.read());
    }
  }

  writeTo(ByteData bd) =>
      new DescribedData(t, bd).writeFrom(this);

  clear() =>
      writeFrom(devZero);

  ByteData nthWord(int n) =>
      byteView(data, $w.size * 4, $w.size);

  int operator [](int i) => data.getUint8(i);
  operator []=(int i, int b) => data.setUint8(i, b);
}


class ZeroData extends DescribedData{
  static ByteData nil = new ByteData($w.size);
  ZeroData() : super(new Descriptor(0), new ByteData(0));
  ByteData nthWord(int n) =>
      nil;
}
var devZero = new ZeroData();