part of rewrite;

class Thread {
  static Thread current;
  static int count = 0;
  static const int PQUANTA = 2048;
  static ThreadGroup
    threads = new Queue<Thread>(),
    ready   = new ThreadGroup(ThreadState.Pready),
    dead    = new ThreadGroup(ThreadState.Pexiting),
    asleep  = new ThreadGroup(ThreadState.Psleeping);

  int PC;
  int pid = count++;
  int quanta = PQUANTA;
  String errstr;
  ByteData receiving;
  DescribedData sending;
  Thread parent;
  ThreadGroup group = ready;
  ThreadState _state = ThreadState.Pready;
  List<Frame>  frames  = new List<Frame>();
  Frame FP;
  LibraryModule MP;
  bool debugThreads = true;

  ThreadState get state => _state;

  void set state(ThreadState s){
    _state = s;
    if(debugThreads)
      print('THREAD $pid : $state');
  }

  Thread(this.MP, this.PC, this.FP, [this.parent]){
    threads.add(this);
    acquire();
    frames.add(FP);
  }

  pause(int ms){
    release();
    sleep(new Duration(milliseconds : ms));
    acquire();
  }

  acquire() =>
    ready.add(this);

  release() =>
    asleep.add(this);

  wind(Frame f, [Module m]){
    FP.module = MP;
    FP.PC = PC;
    FP = f;
    if(m != null)
      MP = m;
  }

  unwind(){
    FP.cleanup();
    if(frames.length == 0)
      error('');
    frames.removeLast();
    FP = frames.last;
    MP = FP.module;
  }

  frame(Descriptor d) =>
      frames.add(new Frame(d));

  spawn(Frame f, int pc, [Module m]) =>
      new Thread(MP, pc, f, this);

  call(Frame f, int pc, [Module m]){
    wind(f, m);
    PC = pc;
  }

  ret(){
    PC = FP.PC;
    unwind();
  }


  ///ugly; fix
  ModuleFunction target(Module m, int index) =>
      MP.imports[MP.loaded.indexOf(m)][index].target;

  mframe(Module<ModuleFunction> m, int index) =>
      frame(target(m, index).t);

  ///ugly; fix
  mspawn(Frame f, int index, Module m){
   if(m is LibraryModule){
     var t = target(m, index);
     if(t is LibraryFunction)
       return spawn(f, t.PC, m);
   }

   throw new UnsupportedError('');
  }

  ///ugly; fix
  mcall(Frame f, int index, Module m){
    if(m is LibraryModule){
      var t = target(m, index);
      if(t is LibraryFunction){
        call(f, t.PC, m);
        return;
      }
    }
    if(m is BuiltinModule){
      var t = target(m, index);
      if(t is BuiltinFunction){
        wind(f, m);
        t.fn(f);
        unwind();
        return;
      }
    }
    throw new UnsupportedError('');
  }


  jump(int to, [bool condition = true]) =>
      PC = condition ? to : PC;

  error([String s = '']){
      dead.add(this);
      print('Thread $pid exiting: $s');
  }
}



class ThreadState extends Enum<String>{
  static const ThreadState
    Palt      = const ThreadState('Palt'),
    Psend     = const ThreadState('Psend'),
    Precv     = const ThreadState('Precv'),
    Pdebug    = const ThreadState('Pdebug'),
    Pready    = const ThreadState('Pready'),
    Pbroken   = const ThreadState('Pbroken'),
    Prelease  = const ThreadState('Prelease'),
    Pexiting  = const ThreadState('Pexiting'),
    Psleeping = const ThreadState('Psleeping');

  const ThreadState(String name) : super(name);
}



class ThreadGroup extends ListQueue<Thread>{
  ThreadState ts;
  ThreadGroup(this.ts);
  add(Thread t){
    super.add(t);
    t.group.remove(t);
    t.group = this;
    t.state = ts;
  }
}