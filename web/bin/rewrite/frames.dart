part of rewrite;

class Frame{
  int PC;
  Module module;
  DescribedData data;

  Frame(Descriptor t){
    data = new DescribedData(t);
  }

  Function cleanup() =>
      data.clear();
}
