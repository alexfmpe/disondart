part of rewrite;


typedef Arity1<T>(T x);
typedef Arity2<T>(T x, T y);
typedef Arity3<T>(T x, T y, T z);

typedef bool Predicate<T>(T t);
typedef bool Relation<T,U>(T t, U u);
typedef bool Comparison(Comparable x, Comparable y);
//////////////////////////////////////
identity(x) => x;



or  (int x, int y) => x | y;
and (int x, int y) => x & y;
xor (int x, int y) => x ^ y;

shl (int x, int y) => x << y;
shr (int x, int y) => x >> y;

add (num x, num y) => x + y;
sub (num x, num y) => x - y;
mul (num x, num y) => x * y;
div (num x, num y) => x / y;
quot(num x, num y) => x ~/ y;
mod (num x, num y) => x % y;
neg (num x) => -x;


Relation
  lt = (x,y) => x <  y,
  gt = (x,y) => x >  y,
  le = (x,y) => x <= y,
  ge = (x,y) => x >= y,
  eq = (x,y) => x == y,
  ne = (x,y) => x == y;

//////////////////////////////////////
List<int> range(int x, int y) =>
    new List<int>.generate(y-x, (i) => x + i);

String changeBase(int from, int to, String number) =>
  int.parse(number, radix:from).toRadixString(to);

ByteData byteView(ByteData data, int offset, [int length]) =>
    new ByteData.view(data.buffer, data.offsetInBytes + offset, length);

validateAccess(Dis_Array a, int n){
  if(a == null || n > a.length)
    Thread.current.error(exBounds);
}

//////////////////////////////////////
typedef T Transform<T> (T t);

Transform<String> appender(String suffix) =>
    (String str) => str + suffix;

Transform<String> preppender(String prefix) =>
    (String str) => prefix + str;

append(String suffix, String str) =>
    appender(suffix)(str);

preppend(String suffix, String str) =>
    preppender(suffix)(str);
//////////////////////////////////////
class Enum<T>{
  final T value;
  const Enum(this.value);
  toString() => value;
}
