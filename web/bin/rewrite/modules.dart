part of rewrite;

///type descriptor for module data?
abstract class Module<F extends ModuleFunction> extends Dis_Object{
  String path;
  String name;

  List<Module> loaded = [];
  List<F> exports = [];
  List<List<Link>> imports = [];
  List<Descriptor> descriptors = [];

  Module(this.path, this.name);
}

abstract class ModuleFunction{
  int hash;
  String name;
  Descriptor t;
  ModuleFunction(this.name, this.hash, this.t);
}
/////////////////////////////////////////////////////////////////////////////////

class LibraryModule extends Module<LibraryFunction>{
  int linknumber = -1;
  String source;
  ByteData data;
  int entryPC;
  Descriptor entryDescriptor;

  List<Function> code   = [];
  List<Function> pseudo = [];

  LibraryModule(String path) : super(libPath(path), libName(path));

  static String libPath(String path) =>
      path.replaceFirst('/dis/', '').replaceFirst('.dis', '.dart');

  static String libName(String path) =>
      new RegExp(r'(\w+)\.dis').firstMatch(path).group(1);

  ByteData atOffset(int offset) =>
      byteView(data, offset);


  P_module(String n) =>
      name = n;

  P_source(String s) =>
      source = s;

  P_entry(int n, int pc){
    entryPC = pc;
    entryDescriptor = descriptors[n];
  }

  P_desc(int size, String hexmap) =>
      descriptors.add(new Descriptor.hexmap(size, hexmap));

  P_ldts(String location, int size) =>
      imports = new List<List>.generate(size, (i) => []);

  P_ext(String location, int hash, String name) =>
      imports[linknumber].add(new Link(name, hash));

  P_link(int n, int pc, int hash, String name) =>
      exports.add(new LibraryFunction(name, hash, descriptors[n], pc));

  P_var(int size) =>
      data = new ByteData(size);

  P__atom($<Atom> w, String location, value){
    var re = new RegExp(r'^(\s+)\+(\d+)$');
    var g = re.firstMatch(location).group;
    String segment = g(0);
    int offset = int.parse(g(1));
    segment == 'ldt'  ? linknumber++
                      : w(atOffset(offset)).write(value);
  }

  P_byte  (String location, int value)     =>  P__atom($b, location, value);
  P_word  (String location, int value)     =>  P__atom($w, location, value);
  P_long  (String location, int value)     =>  P__atom($l, location, value);
  P_real  (String location, int value)     =>  P__atom($f, location, value);
  P_string(String location, String value)  =>  P__atom($S, location, new Dis_String(value));

}

class Link {
  int hash;
  String name;
  ModuleFunction target;
  Link(this.name, this.hash);
}

class LibraryFunction extends ModuleFunction {
  int PC;
  LibraryFunction(String name, int hash, Descriptor t, [this.PC]) :
    super(name,hash,t);
}

/////////////////////////////////////////////////////////////////////////////////

class BuiltinModule extends Module<BuiltinFunction>{
  BuiltinModule(String path) : super(path, path.substring(1)){
    exports = runtabs[name];
  }

  BuiltinFunction findRuntab(String name) =>
      exports.firstWhere((s) => s.name == name);
}

typedef FrameFunction(Frame f);

class BuiltinFunction extends ModuleFunction{
  FrameFunction fn;

  BuiltinFunction(String name, int hash, this.fn, int size, int np, List<int> pmap) :
    super(name, hash, new Descriptor(size, pmap));
}



BuiltinFunction buildRuntab(List l) =>
    new BuiltinFunction(l[0], l[1], l[2], l[3], l[4], l[5]);

List<BuiltinFunction> buildTab(List l) => l.map(buildRuntab).toList();

Map<String, List<BuiltinFunction>> runtabs =
{
  'Sys'     : buildTab(Sysmodtab),
  'Math'    : buildTab(Mathmodtab),
};





