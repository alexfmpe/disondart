part of rewrite;

class Struct{
  ByteData data;
  Struct(this.data);


  Semantics field($ w, int offset) =>
      w(byteView(data, offset));
}

abstract class Struct_Frame extends Struct{
  static const NREG = 5;
  Memory regs, temps;
  Address ret;
  $ retWrap;
  int offset = 0;

  Struct_Frame(Frame f, [this.retWrap]) : super(f.data.data){
    regs  = field($m, offset);  offset += $w.size * (NREG-1);
    ret   = field($a, offset);  offset += $w.size;
    temps = field($m, offset);  offset += $b.size * 12;
  }

  result(value) =>
      retWrap(ret.read()).write(value);
}

abstract class VarArgs_Frame extends Struct_Frame{
  VarArgs_Frame(Frame f) : super(f);
  VarArgs get varargs => new VarArgs(byteView(data, offset));
}


class F_Sys_aprint extends VarArgs_Frame{
  Reference<Dis_String> str;
  F_Sys_aprint(Frame f) : super(f){
    str = field($S, offset); offset += $w.size;
  }
}

class F_Sys_byte2char extends Struct_Frame{
  Word n;
  Reference<Dis_Array> buf;
  F_Sys_byte2char(Frame f) : super(f){
    buf = field($A, offset); offset += $w.size;
    n   = field($w, offset); offset += $w.size;
  }
}

class F_Sys_char2byte extends Struct_Frame{
  Word c, n;
  Reference<Dis_Array> buf;
  F_Sys_char2byte(Frame f) : super(f, $w){
    c   = field($w, offset); offset += $w.size;
    buf = field($A, offset); offset += $w.size;
    n   = field($w, offset); offset += $w.size;
  }
}

class F_Sys_millisec extends Struct_Frame{
  Semantics period;
  F_Sys_millisec(Frame f) : super(f, $w);
}

class F_Sys_print extends VarArgs_Frame{
  Reference<Dis_String> str;
  F_Sys_print(Frame f) : super(f){
    str = field($S, offset); offset += $w.size;
  }
}

class F_Sys_sleep extends Struct_Frame{
  Word period;
  F_Sys_sleep(Frame f) : super(f){
    period = field($w, offset); offset += $w.size;
  }
}

class F_Sys_sprint extends VarArgs_Frame{
  Reference<Dis_String> str;
  F_Sys_sprint(Frame f) : super(f){
    str = field($S, offset); offset += $w.size;
  }
}

class F_Sys_tokenize extends Struct_Frame{
  Reference<Dis_String> str, delim;
  F_Sys_tokenize(Frame f) : super(f){
    str   = field($S, offset);  offset += $w.size;
    delim = field($S, offset);  offset += $w.size;
  }
}

class F_Sys_utfbytes extends Struct_Frame{
  Word n;
  Reference<Dis_Array> buf;
  F_Sys_utfbytes(Frame f) : super(f, $w){
    buf = field($A, offset);  offset += $w.size;
    n   = field($w, offset);  offset += $w.size;
  }
}

class F_Sys_werrstr extends Struct_Frame{
  Reference<Dis_String> s;
  F_Sys_werrstr(Frame f) : super(f, $w){
    s = field($S, offset);  offset += $w.size;
  }
}




