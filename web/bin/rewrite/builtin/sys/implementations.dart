part of rewrite;

DateTime get currentTime => new DateTime.now();
Duration get ellapsedTime => currentTime.difference(startTime);
var startTime = currentTime;


Sys_aprint(Frame frame){
  var f   = new F_Sys_aprint(frame);
  var str = f.str.read();
  var va  = f.varargs;

  String s = formatOutput(str.text, va);
  Dis_Array a = new Dis_Array.fromString(s);
  f.result(a);
}


Sys_byte2char(Frame frame){
  var f = new F_Sys_byte2char(frame);
  var a = f.buf.read();
  var n = f.n.read();
  var r = f.ret.read();

}


Sys_char2byte(Frame frame){
  var f = new F_Sys_char2byte(frame);
  var c = f.c.read();
  var a = f.buf.read();
  var n = f.n.read();
  var l = a.length;
  validateAccess(a,n);

  var str = new String.fromCharCode(c);
  var utf = UTF8.encode(str);
  if(utf.length > l - n){
    f.result(0);
    return;
  }

  a.writeBytes(utf, n);
  f.result(utf.length);
}


Sys_millisec(Frame frame){
  var f = new F_Sys_millisec(frame);
  f.result(ellapsedTime.inMilliseconds);
}

Sys_print(Frame frame){
  var f   = new F_Sys_print(frame);
  var str = f.str.read();
  var va  = f.varargs;

  String s = formatOutput(str.text, va);
  print(s);
  f.result(s.length);
}


Sys_sleep(Frame frame){
  var f = new F_Sys_sleep(frame);
  var ms = f.period.read();
  Thread.current.pause(ms);
}


Sys_sprint(Frame frame){
  var f   = new F_Sys_sprint(frame);
  var str = f.str.read();
  var va  = f.varargs;

  String s = formatOutput(str.text, va);
  f.result(new Dis_String(s));
}


Sys_tokenize(Frame frame){
  var f = new F_Sys_tokenize(frame);
  var str = f.str.read();
  var delim = f.delim.read();
  var list = str.text.split(delim.text);
  //f.result
}


Sys_utfbytes(Frame frame){
  var f = new F_Sys_utfbytes(frame);
  var a = f.buf.read();
  var n = f.n.read();
  validateAccess(a, n);
  //f.result(value)
}

Sys_werrstr(Frame frame){
  var f = new F_Sys_werrstr(frame);
  var s = f.s.read();
  Thread.current.errstr = s.text;
  f.result(0);
}

class VarArgs{
  int offset = 0;
  ByteData data;
  VarArgs(this.data);

  advance($<Atom> w){
    var b = byteView(data, offset);
    offset += w.size;
    return w(b).read();
  }
}
