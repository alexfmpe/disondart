part of rewrite;

//%[flags][width][.precision]verb

formatOutput(String s, VarArgs va) =>
  Formatter.formatOutput(s, va);

abstract class Formatter{
  static const String
    F1          = r'(\*|\d+)',
    F2          = '\\.$F1',
    BIG         = 'b',
    UNSIGNED    = 'u',
    ALTERNATIVE = '#',
    LEFTJUSTIFY = '\\-',
    SHOWSIGN    = '\\+';

  static const String
    DECIMAL     = 'd',
    OCTAL       = 'o',
    HEXA        = 'x',
    FLOAT       = 'f',
    EXPONENT    = 'e',
    SUCCINT     = 'g',
    LASTERROR   = 'r',
    STRING      = 's',
    CHAR        = 'c',
    PERCENTILE  = '%';

  static String
    INTEGERS  = [DECIMAL, OCTAL, HEXA].join(),
    REALS     = [FLOAT, EXPONENT, SUCCINT].join(),
    NUMBERS   = [DECIMAL, OCTAL, HEXA, FLOAT, EXPONENT, SUCCINT].join(),
    VERBS     = [DECIMAL, OCTAL, HEXA, FLOAT, EXPONENT, SUCCINT, CHAR, LASTERROR, STRING, PERCENTILE].join(),
    FLAGS     = [BIG, UNSIGNED, ALTERNATIVE, LEFTJUSTIFY, SHOWSIGN].join();


  static formatOutput(String s, VarArgs va){
    var l = VERBS;
    var u = VERBS.toUpperCase();
    var vs = '[$l$u]';
    var fs = '[$FLAGS]';

    var mod = '$fs|$F1|$F2';
    var spec = new RegExp('%($mod)*$vs');

    Match m = spec.firstMatch(s);
    if(m == null)
      return s;

    String f = m.group(0);
    f = f.replaceAllMapped('*', (x) => va.advance($w));

    String before  = s.substring(0, m.start);
    String after   = s.substring(m.end);
    String out = formatConversion(f, f[f.length-1], va);

    return before + out + formatOutput(after, va);
  }

  static formatConversion(String spec, String verb, VarArgs va){

    bool supplied(Pattern flag) =>
            spec.contains(flag);

    fetch(String s, RegExp re) =>
        supplied(re) ? re.firstMatch(s).group(1) : null;

    String maybeFormat(String number, String patt, Transform<String> formatter) =>
      supplied(new RegExp(patt)) ? formatter(number) : number;

    Transform<String> padder(String widthSpec, String padding, bool onLeft) =>
        (String number){
          RegExp re = new RegExp(widthSpec);
          int width = int.parse(fetch(spec, re));
          return (onLeft ? number.padLeft : number.padRight)(width, padding);
        };

    formatText(Function f){
      String text = f();
      bool leftJust = supplied(new RegExp(LEFTJUSTIFY));
      return maybeFormat(text, F1, padder(F1, ' ', !leftJust));
    }

    formatNumber(Function f){
      String number = f(verb.toLowerCase());
      return verb == verb.toUpperCase() ? number.toUpperCase() : number;
    }

    formatInt(String verb){
      $ w = supplied(BIG) ? (supplied(UNSIGNED) ? $ul : $l)
                          : (supplied(UNSIGNED) ? $uw : $w);

      int r = (verb == HEXA)    ? 16 :
              (verb == DECIMAL) ? 10 :
                                   8;

      int i = va.advance(w);
      bool negative = (i < 0);
      String number = i.abs().toRadixString(r);

      number = maybeFormat(number, F2,          padder(F2, '0', true));
      number = maybeFormat(number, ALTERNATIVE, preppender('0x'));

      return (negative ? '-' : '') + number;
    }

    formatReal(String verb){
      double d = va.advance($f);

      Function f =  (verb == FLOAT)    ?  d.toStringAsFixed       :
                    (verb == EXPONENT) ?  d.toStringAsExponential :
                                          d.toStringAsPrecision   ;

      String f2 = fetch(spec, new RegExp(F2));
      String number = f(f2 == null ? 0 : int.parse(f2));

      if(d > 0 && supplied(new RegExp(SHOWSIGN)))
        number = '+$number';

      return number;
    }

    formatChar() =>
        new String.fromCharCode(va.advance($w));

    formatString(){
      String s = va.advance($S);

      String f2 = fetch(spec, new RegExp(F2));
      int len = min(s.length, f2 == null ? double.INFINITY : int.parse(f2));

      return s.substring(0, len);
    }


    Map<String, Function> formatters = {
      INTEGERS    : () => formatText(() => formatNumber(formatInt)),
      REALS       : () => formatText(() => formatNumber(formatReal)),
      CHAR        : () => formatText(formatChar),
      STRING      : () => formatText(formatString),
      LASTERROR   : () => null,
      PERCENTILE  : () => '%',
    };

    String k = formatters.keys.singleWhere((k) => k.contains(verb.toLowerCase()));
    return formatters[k]();
  }
}
