part of dis;

typedef Freer(Heap, int);
class TypeDescriptor{
  int ref = 1, size, np = 0;
  Function mark, destroy, initialize;
  Freer free;
  List<int> map;

  TypeDescriptor(this.size, this.np, this.map,
                 [this.free, this.mark, this.destroy, this.initialize]);
}

PrimitiveType(Type t) =>
    new TypeDescriptor(sizeof(t), 0, [0], nofree, nofree);

ReferenceType(Type t, List<int> map, Freer free, Function mark) =>
    new TypeDescriptor(sizeof(t), map.length, map, free, mark);

nofree(h,i){}
nomark(){}
noPointers(){}
markArray(){}
markList(){}
markHeap(){}
freeChannel(h,i){}

TypeDescriptor
  Tarray    = ReferenceType(VMArray,    [0],     freeArray,    markArray),
  Tlist     = ReferenceType(VMList,     [0],     freeList,     markList),
  Tstring   = ReferenceType(VMString,   [0],     freeString,   noPointers),
  Tchannel  = ReferenceType(VMChannel,  [0x80],  freeChannel,  markHeap),

  Tpointer  = new TypeDescriptor(sizeof(VMPointer), 1, [0x80], nofree, nomark),

  Tbyte     = PrimitiveType(VMByte),
  Tword     = PrimitiveType(VMWord),
  Tlong     = PrimitiveType(VMLong),
  Treal     = PrimitiveType(VMDouble);


List<TypeDescriptor> builtinTypes =
[
  Tbyte, Tword, Tlong, Treal,
  Tarray, Tlist, Tstring, // Tchannel,
];
List<TypeDescriptor> moduleTypes = [];

//int builtinOffset = 256;
typeWithID(int n) => (n >= 256) ? builtinTypes[n-256]
                                  : moduleTypes[n];

typeID(TypeDescriptor td){
  int n = builtinTypes.indexOf(td);
  return (n != -1) ? n+256 : moduleTypes.indexOf(td);
}

dtype(Freer free, int size, List<int> map) =>
  new TypeDescriptor(size, map.length, map, free, markHeap);
