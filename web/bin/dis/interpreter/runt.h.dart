part of dis;

class Sys_Qid extends VMStruct{
  VMLong _path;
  VMWord _vers;
  VMWord _qtype;
  fields(){
    _path = field(VMLong);
    _vers = field(VMWord);
    _qtype = field(VMWord);
  }
}
var Sys_Qid_size = 16;
var Sys_Qid_map = [0];
class Sys_Dir extends VMStruct{
  VMPointer<VMString> _name;
  VMPointer<VMString> _uid;
  VMPointer<VMString> _gid;
  VMPointer<VMString> _muid;
  Sys_Qid _qid;
  VMWord _mode;
  VMWord _atime;
  VMWord _mtime;
  Array<VMByte> __pad44;
  VMLong _length;
  VMWord _dtype;
  VMWord _dev;
  fields(){
    _name = new VMPointer<VMString>.field(this);
    _uid = new VMPointer<VMString>.field(this);
    _gid = new VMPointer<VMString>.field(this);
    _muid = new VMPointer<VMString>.field(this);
    _qid = field(Sys_Qid);
    _mode = field(VMWord);
    _atime = field(VMWord);
    _mtime = field(VMWord);
    __pad44 = new Array<VMByte>(this,4);
    _length = field(VMLong);
    _dtype = field(VMWord);
    _dev = field(VMWord);
  }
}
var Sys_Dir_size = 64;
var Sys_Dir_map = [0xf0,];
class Sys_FD extends VMStruct{
  VMWord _fd;
  fields(){
    _fd = field(VMWord);
  }
}
var Sys_FD_size = 4;
var Sys_FD_map = [0];
class Sys_Connection extends VMStruct{
  VMPointer<Sys_FD> _dfd;
  VMPointer<Sys_FD> _cfd;
  VMPointer<VMString> _dir;
  fields(){
    _dfd = new VMPointer<Sys_FD>.field(this);
    _cfd = new VMPointer<Sys_FD>.field(this);
    _dir = new VMPointer<VMString>.field(this);
  }
}
var Sys_Connection_size = 12;
var Sys_Connection_map = [0xe0,];
class Sys_Rread extends VMStruct{
  VMPointer<VMArray> _t0;
  VMPointer<VMString> _t1;
  fields(){
    _t0 = new VMPointer<VMArray>.field(this);
    _t1 = new VMPointer<VMString>.field(this);
  }
}
var Sys_Rread_size = 8;
var Sys_Rread_map = [0xc0,];
class Sys_Rwrite extends VMStruct{
  VMWord _t0;
  VMPointer<VMString> _t1;
  fields(){
    _t0 = field(VMWord);
    _t1 = new VMPointer<VMString>.field(this);
  }
}
var Sys_Rwrite_size = 8;
var Sys_Rwrite_map = [0x40,];
class Sys_FileIO extends VMStruct{
  VMPointer<VMChannel> _read;
  VMPointer<VMChannel> _write;
  fields(){
    _read = new VMPointer<VMChannel>.field(this);
    _write = new VMPointer<VMChannel>.field(this);
  }
}
class Sys_FileIO_read extends VMStruct{
  VMWord _t0;
  VMWord _t1;
  VMWord _t2;
  VMPointer<VMChannel> _t3;
  fields(){
    _t0 = field(VMWord);
    _t1 = field(VMWord);
    _t2 = field(VMWord);
    _t3 = new VMPointer<VMChannel>.field(this);
  }
}
var Sys_FileIO_read_size = 16;
var Sys_FileIO_read_map = [0x10,];
class Sys_FileIO_write extends VMStruct{
  VMWord _t0;
  VMPointer<VMArray> _t1;
  VMWord _t2;
  VMPointer<VMChannel> _t3;
  fields(){
    _t0 = field(VMWord);
    _t1 = new VMPointer<VMArray>.field(this);
    _t2 = field(VMWord);
    _t3 = new VMPointer<VMChannel>.field(this);
  }
}
var Sys_FileIO_write_size = 16;
var Sys_FileIO_write_map = [0x50,];
var Sys_FileIO_size = 8;
var Sys_FileIO_map = [0xc0,];
class Draw_Chans extends VMStruct{
  VMWord _desc;
  fields(){
    _desc = field(VMWord);
  }
}
var Draw_Chans_size = 4;
var Draw_Chans_map = [0];
class Draw_Point extends VMStruct{
  VMWord _x;
  VMWord _y;
  fields(){
    _x = field(VMWord);
    _y = field(VMWord);
  }
}
var Draw_Point_size = 8;
var Draw_Point_map = [0];
class Draw_Rect extends VMStruct{
  Draw_Point _min;
  Draw_Point _max;
  fields(){
    _min = field(Draw_Point);
    _max = field(Draw_Point);
  }
}
var Draw_Rect_size = 16;
var Draw_Rect_map = [0];
class Draw_Image extends VMStruct{
  Draw_Rect _r;
  Draw_Rect _clipr;
  VMWord _depth;
  Draw_Chans _chans;
  VMWord _repl;
  VMPointer<Draw_Display> _display;
  VMPointer<Draw_Screen> _screen;
  VMPointer<VMString> _iname;
  fields(){
    _r = field(Draw_Rect);
    _clipr = field(Draw_Rect);
    _depth = field(VMWord);
    _chans = field(Draw_Chans);
    _repl = field(VMWord);
    _display = new VMPointer<Draw_Display>.field(this);
    _screen = new VMPointer<Draw_Screen>.field(this);
    _iname = new VMPointer<VMString>.field(this);
  }
}
var Draw_Image_size = 56;
var Draw_Image_map = [0x0,0x1c,];
class Draw_Display extends VMStruct{
  VMPointer<Draw_Image> _image;
  VMPointer<Draw_Image> _white;
  VMPointer<Draw_Image> _black;
  VMPointer<Draw_Image> _opaque;
  VMPointer<Draw_Image> _transparent;
  fields(){
    _image = new VMPointer<Draw_Image>.field(this);
    _white = new VMPointer<Draw_Image>.field(this);
    _black = new VMPointer<Draw_Image>.field(this);
    _opaque = new VMPointer<Draw_Image>.field(this);
    _transparent = new VMPointer<Draw_Image>.field(this);
  }
}
var Draw_Display_size = 20;
var Draw_Display_map = [0xf8,];
class Draw_Font extends VMStruct{
  VMPointer<VMString> _name;
  VMWord _height;
  VMWord _ascent;
  VMPointer<Draw_Display> _display;
  fields(){
    _name = new VMPointer<VMString>.field(this);
    _height = field(VMWord);
    _ascent = field(VMWord);
    _display = new VMPointer<Draw_Display>.field(this);
  }
}
var Draw_Font_size = 16;
var Draw_Font_map = [0x90,];
class Draw_Screen extends VMStruct{
  VMWord _id;
  VMPointer<Draw_Image> _image;
  VMPointer<Draw_Image> _fill;
  VMPointer<Draw_Display> _display;
  fields(){
    _id = field(VMWord);
    _image = new VMPointer<Draw_Image>.field(this);
    _fill = new VMPointer<Draw_Image>.field(this);
    _display = new VMPointer<Draw_Display>.field(this);
  }
}
var Draw_Screen_size = 16;
var Draw_Screen_map = [0x70,];
class Draw_Pointer extends VMStruct{
  VMWord _buttons;
  Draw_Point _xy;
  VMWord _msec;
  fields(){
    _buttons = field(VMWord);
    _xy = field(Draw_Point);
    _msec = field(VMWord);
  }
}
var Draw_Pointer_size = 16;
var Draw_Pointer_map = [0];
class Draw_Context extends VMStruct{
  VMPointer<Draw_Display> _display;
  VMPointer<Draw_Screen> _screen;
  VMPointer<VMChannel> _wm;
  fields(){
    _display = new VMPointer<Draw_Display>.field(this);
    _screen = new VMPointer<Draw_Screen>.field(this);
    _wm = new VMPointer<VMChannel>.field(this);
  }
}
class Draw_Context_wm extends VMStruct{
  VMPointer<VMString> _t0;
  VMPointer<VMChannel> _t1;
  fields(){
    _t0 = new VMPointer<VMString>.field(this);
    _t1 = new VMPointer<VMChannel>.field(this);
  }
}
var Draw_Context_wm_size = 8;
var Draw_Context_wm_map = [0xc0,];
var Draw_Context_size = 12;
var Draw_Context_map = [0xe0,];
class Draw_Wmcontext extends VMStruct{
  VMPointer<VMChannel> _kbd;
  VMPointer<VMChannel> _ptr;
  VMPointer<VMChannel> _ctl;
  VMPointer<VMChannel> _wctl;
  VMPointer<VMChannel> _images;
  VMPointer<Sys_FD> _connfd;
  VMPointer<Draw_Context> _ctxt;
  fields(){
    _kbd = new VMPointer<VMChannel>.field(this);
    _ptr = new VMPointer<VMChannel>.field(this);
    _ctl = new VMPointer<VMChannel>.field(this);
    _wctl = new VMPointer<VMChannel>.field(this);
    _images = new VMPointer<VMChannel>.field(this);
    _connfd = new VMPointer<Sys_FD>.field(this);
    _ctxt = new VMPointer<Draw_Context>.field(this);
  }
}
var Draw_Wmcontext_kbd_size = 4;
var Draw_Wmcontext_kbd_map = [0];
var Draw_Wmcontext_ptr_size = 4;
var Draw_Wmcontext_ptr_map = [0x80,];
var Draw_Wmcontext_ctl_size = 4;
var Draw_Wmcontext_ctl_map = [0x80,];
var Draw_Wmcontext_wctl_size = 4;
var Draw_Wmcontext_wctl_map = [0x80,];
var Draw_Wmcontext_images_size = 4;
var Draw_Wmcontext_images_map = [0x80,];
var Draw_Wmcontext_size = 28;
var Draw_Wmcontext_map = [0xfe,];
class Prefab_Style extends VMStruct{
  VMPointer<Draw_Font> _titlefont;
  VMPointer<Draw_Font> _textfont;
  VMPointer<Draw_Image> _elemcolor;
  VMPointer<Draw_Image> _edgecolor;
  VMPointer<Draw_Image> _titlecolor;
  VMPointer<Draw_Image> _textcolor;
  VMPointer<Draw_Image> _highlightcolor;
  fields(){
    _titlefont = new VMPointer<Draw_Font>.field(this);
    _textfont = new VMPointer<Draw_Font>.field(this);
    _elemcolor = new VMPointer<Draw_Image>.field(this);
    _edgecolor = new VMPointer<Draw_Image>.field(this);
    _titlecolor = new VMPointer<Draw_Image>.field(this);
    _textcolor = new VMPointer<Draw_Image>.field(this);
    _highlightcolor = new VMPointer<Draw_Image>.field(this);
  }
}
var Prefab_Style_size = 28;
var Prefab_Style_map = [0xfe,];
class Prefab_Environ extends VMStruct{
  VMPointer<Draw_Screen> _screen;
  VMPointer<Prefab_Style> _style;
  fields(){
    _screen = new VMPointer<Draw_Screen>.field(this);
    _style = new VMPointer<Prefab_Style>.field(this);
  }
}
var Prefab_Environ_size = 8;
var Prefab_Environ_map = [0xc0,];
class Prefab_Layout extends VMStruct{
  VMPointer<Draw_Font> _font;
  VMPointer<Draw_Image> _color;
  VMPointer<VMString> _text;
  VMPointer<Draw_Image> _icon;
  VMPointer<Draw_Image> _mask;
  VMPointer<VMString> _tag;
  fields(){
    _font = new VMPointer<Draw_Font>.field(this);
    _color = new VMPointer<Draw_Image>.field(this);
    _text = new VMPointer<VMString>.field(this);
    _icon = new VMPointer<Draw_Image>.field(this);
    _mask = new VMPointer<Draw_Image>.field(this);
    _tag = new VMPointer<VMString>.field(this);
  }
}
var Prefab_Layout_size = 24;
var Prefab_Layout_map = [0xfc,];
class Prefab_Element extends VMStruct{
  VMWord _kind;
  Draw_Rect _r;
  VMPointer<Prefab_Environ> _environ;
  VMPointer<VMString> _tag;
  VMPointer<VMList> _kids;
  VMPointer<VMString> _str;
  VMPointer<Draw_Image> _mask;
  VMPointer<Draw_Image> _image;
  VMPointer<Draw_Font> _font;
  fields(){
    _kind = field(VMWord);
    _r = field(Draw_Rect);
    _environ = new VMPointer<Prefab_Environ>.field(this);
    _tag = new VMPointer<VMString>.field(this);
    _kids = new VMPointer<VMList>.field(this);
    _str = new VMPointer<VMString>.field(this);
    _mask = new VMPointer<Draw_Image>.field(this);
    _image = new VMPointer<Draw_Image>.field(this);
    _font = new VMPointer<Draw_Font>.field(this);
  }
}
var Prefab_Element_size = 48;
var Prefab_Element_map = [0x7,0xf0,];
class Prefab_Compound extends VMStruct{
  VMPointer<Draw_Image> _image;
  VMPointer<Prefab_Environ> _environ;
  Draw_Rect _r;
  VMPointer<Prefab_Element> _title;
  VMPointer<Prefab_Element> _contents;
  fields(){
    _image = new VMPointer<Draw_Image>.field(this);
    _environ = new VMPointer<Prefab_Environ>.field(this);
    _r = field(Draw_Rect);
    _title = new VMPointer<Prefab_Element>.field(this);
    _contents = new VMPointer<Prefab_Element>.field(this);
  }
}
var Prefab_Compound_size = 32;
var Prefab_Compound_map = [0xc3,];
class Tk_Toplevel extends VMStruct{
  VMPointer<Draw_Display> _display;
  VMPointer<VMChannel> _wreq;
  VMPointer<Draw_Image> _image;
  VMPointer<Draw_Wmcontext> _ctxt;
  Draw_Rect _screenr;
  fields(){
    _display = new VMPointer<Draw_Display>.field(this);
    _wreq = new VMPointer<VMChannel>.field(this);
    _image = new VMPointer<Draw_Image>.field(this);
    _ctxt = new VMPointer<Draw_Wmcontext>.field(this);
    _screenr = field(Draw_Rect);
  }
}
var Tk_Toplevel_wreq_size = 4;
var Tk_Toplevel_wreq_map = [0x80,];
var Tk_Toplevel_size = 32;
var Tk_Toplevel_map = [0xf0,];
class Keyring_IPint extends VMStruct{
  VMWord _x;
  fields(){
    _x = field(VMWord);
  }
}
var Keyring_IPint_size = 4;
var Keyring_IPint_map = [0];
class Keyring_SigAlg extends VMStruct{
  VMPointer<VMString> _name;
  fields(){
    _name = new VMPointer<VMString>.field(this);
  }
}
var Keyring_SigAlg_size = 4;
var Keyring_SigAlg_map = [0x80,];
class Keyring_PK extends VMStruct{
  VMPointer<Keyring_SigAlg> _sa;
  VMPointer<VMString> _owner;
  fields(){
    _sa = new VMPointer<Keyring_SigAlg>.field(this);
    _owner = new VMPointer<VMString>.field(this);
  }
}
var Keyring_PK_size = 8;
var Keyring_PK_map = [0xc0,];
class Keyring_SK extends VMStruct{
  VMPointer<Keyring_SigAlg> _sa;
  VMPointer<VMString> _owner;
  fields(){
    _sa = new VMPointer<Keyring_SigAlg>.field(this);
    _owner = new VMPointer<VMString>.field(this);
  }
}
var Keyring_SK_size = 8;
var Keyring_SK_map = [0xc0,];
class Keyring_Certificate extends VMStruct{
  VMPointer<Keyring_SigAlg> _sa;
  VMPointer<VMString> _ha;
  VMPointer<VMString> _signer;
  VMWord _exp;
  fields(){
    _sa = new VMPointer<Keyring_SigAlg>.field(this);
    _ha = new VMPointer<VMString>.field(this);
    _signer = new VMPointer<VMString>.field(this);
    _exp = field(VMWord);
  }
}
var Keyring_Certificate_size = 16;
var Keyring_Certificate_map = [0xe0,];
class Keyring_DigestState extends VMStruct{
  VMWord _x;
  fields(){
    _x = field(VMWord);
  }
}
var Keyring_DigestState_size = 4;
var Keyring_DigestState_map = [0];
class Keyring_AESstate extends VMStruct{
  VMWord _x;
  fields(){
    _x = field(VMWord);
  }
}
var Keyring_AESstate_size = 4;
var Keyring_AESstate_map = [0];
class Keyring_DESstate extends VMStruct{
  VMWord _x;
  fields(){
    _x = field(VMWord);
  }
}
var Keyring_DESstate_size = 4;
var Keyring_DESstate_map = [0];
class Keyring_IDEAstate extends VMStruct{
  VMWord _x;
  fields(){
    _x = field(VMWord);
  }
}
var Keyring_IDEAstate_size = 4;
var Keyring_IDEAstate_map = [0];
class Keyring_RC4state extends VMStruct{
  VMWord _x;
  fields(){
    _x = field(VMWord);
  }
}
var Keyring_RC4state_size = 4;
var Keyring_RC4state_map = [0];
class Keyring_BFstate extends VMStruct{
  VMWord _x;
  fields(){
    _x = field(VMWord);
  }
}
var Keyring_BFstate_size = 4;
var Keyring_BFstate_map = [0];
class Keyring_Authinfo extends VMStruct{
  VMPointer<Keyring_SK> _mysk;
  VMPointer<Keyring_PK> _mypk;
  VMPointer<Keyring_Certificate> _cert;
  VMPointer<Keyring_PK> _spk;
  VMPointer<Keyring_IPint> _alpha;
  VMPointer<Keyring_IPint> _p;
  fields(){
    _mysk = new VMPointer<Keyring_SK>.field(this);
    _mypk = new VMPointer<Keyring_PK>.field(this);
    _cert = new VMPointer<Keyring_Certificate>.field(this);
    _spk = new VMPointer<Keyring_PK>.field(this);
    _alpha = new VMPointer<Keyring_IPint>.field(this);
    _p = new VMPointer<Keyring_IPint>.field(this);
  }
}
var Keyring_Authinfo_size = 24;
var Keyring_Authinfo_map = [0xfc,];
class Keyring_RSApk extends VMStruct{
  VMPointer<Keyring_IPint> _n;
  VMPointer<Keyring_IPint> _ek;
  fields(){
    _n = new VMPointer<Keyring_IPint>.field(this);
    _ek = new VMPointer<Keyring_IPint>.field(this);
  }
}
var Keyring_RSApk_size = 8;
var Keyring_RSApk_map = [0xc0,];
class Keyring_RSAsk extends VMStruct{
  VMPointer<Keyring_RSApk> _pk;
  VMPointer<Keyring_IPint> _dk;
  VMPointer<Keyring_IPint> _p;
  VMPointer<Keyring_IPint> _q;
  VMPointer<Keyring_IPint> _kp;
  VMPointer<Keyring_IPint> _kq;
  VMPointer<Keyring_IPint> _c2;
  fields(){
    _pk = new VMPointer<Keyring_RSApk>.field(this);
    _dk = new VMPointer<Keyring_IPint>.field(this);
    _p = new VMPointer<Keyring_IPint>.field(this);
    _q = new VMPointer<Keyring_IPint>.field(this);
    _kp = new VMPointer<Keyring_IPint>.field(this);
    _kq = new VMPointer<Keyring_IPint>.field(this);
    _c2 = new VMPointer<Keyring_IPint>.field(this);
  }
}
var Keyring_RSAsk_size = 28;
var Keyring_RSAsk_map = [0xfe,];
class Keyring_RSAsig extends VMStruct{
  VMPointer<Keyring_IPint> _n;
  fields(){
    _n = new VMPointer<Keyring_IPint>.field(this);
  }
}
var Keyring_RSAsig_size = 4;
var Keyring_RSAsig_map = [0x80,];
class Keyring_DSApk extends VMStruct{
  VMPointer<Keyring_IPint> _p;
  VMPointer<Keyring_IPint> _q;
  VMPointer<Keyring_IPint> _alpha;
  VMPointer<Keyring_IPint> _key;
  fields(){
    _p = new VMPointer<Keyring_IPint>.field(this);
    _q = new VMPointer<Keyring_IPint>.field(this);
    _alpha = new VMPointer<Keyring_IPint>.field(this);
    _key = new VMPointer<Keyring_IPint>.field(this);
  }
}
var Keyring_DSApk_size = 16;
var Keyring_DSApk_map = [0xf0,];
class Keyring_DSAsk extends VMStruct{
  VMPointer<Keyring_DSApk> _pk;
  VMPointer<Keyring_IPint> _secret;
  fields(){
    _pk = new VMPointer<Keyring_DSApk>.field(this);
    _secret = new VMPointer<Keyring_IPint>.field(this);
  }
}
var Keyring_DSAsk_size = 8;
var Keyring_DSAsk_map = [0xc0,];
class Keyring_DSAsig extends VMStruct{
  VMPointer<Keyring_IPint> _r;
  VMPointer<Keyring_IPint> _s;
  fields(){
    _r = new VMPointer<Keyring_IPint>.field(this);
    _s = new VMPointer<Keyring_IPint>.field(this);
  }
}
var Keyring_DSAsig_size = 8;
var Keyring_DSAsig_map = [0xc0,];
class Keyring_EGpk extends VMStruct{
  VMPointer<Keyring_IPint> _p;
  VMPointer<Keyring_IPint> _alpha;
  VMPointer<Keyring_IPint> _key;
  fields(){
    _p = new VMPointer<Keyring_IPint>.field(this);
    _alpha = new VMPointer<Keyring_IPint>.field(this);
    _key = new VMPointer<Keyring_IPint>.field(this);
  }
}
var Keyring_EGpk_size = 12;
var Keyring_EGpk_map = [0xe0,];
class Keyring_EGsk extends VMStruct{
  VMPointer<Keyring_EGpk> _pk;
  VMPointer<Keyring_IPint> _secret;
  fields(){
    _pk = new VMPointer<Keyring_EGpk>.field(this);
    _secret = new VMPointer<Keyring_IPint>.field(this);
  }
}
var Keyring_EGsk_size = 8;
var Keyring_EGsk_map = [0xc0,];
class Keyring_EGsig extends VMStruct{
  VMPointer<Keyring_IPint> _r;
  VMPointer<Keyring_IPint> _s;
  fields(){
    _r = new VMPointer<Keyring_IPint>.field(this);
    _s = new VMPointer<Keyring_IPint>.field(this);
  }
}
var Keyring_EGsig_size = 8;
var Keyring_EGsig_map = [0xc0,];
class Loader_Inst extends VMStruct{
  VMByte _op;
  VMByte _addr;
  Array<VMByte> __pad2;
  VMWord _src;
  VMWord _mid;
  VMWord _dst;
  fields(){
    _op = field(VMByte);
    _addr = field(VMByte);
    __pad2 = new Array<VMByte>(this,2);
    _src = field(VMWord);
    _mid = field(VMWord);
    _dst = field(VMWord);
  }
}
var Loader_Inst_size = 16;
var Loader_Inst_map = [0];
class Loader_Typedesc extends VMStruct{
  VMWord _size;
  VMPointer<VMArray> _map;
  fields(){
    _size = field(VMWord);
    _map = new VMPointer<VMArray>.field(this);
  }
}
var Loader_Typedesc_size = 8;
var Loader_Typedesc_map = [0x40,];
class Loader_Link extends VMStruct{
  VMPointer<VMString> _name;
  VMWord _sig;
  VMWord _pc;
  VMWord _tdesc;
  fields(){
    _name = new VMPointer<VMString>.field(this);
    _sig = field(VMWord);
    _pc = field(VMWord);
    _tdesc = field(VMWord);
  }
}
var Loader_Link_size = 16;
var Loader_Link_map = [0x80,];
class Loader_Niladt extends VMStruct{
  Array<VMChar> _dummy;
  Array<VMByte> __pad1;
  fields(){
    _dummy = new Array<VMChar>(this,1);
    __pad1 = new Array<VMByte>(this,3);
  }
}
var Loader_Niladt_size = 4;
var Loader_Niladt_map = [0];
class Freetype_Matrix extends VMStruct{
  VMWord _a;
  VMWord _b;
  VMWord _c;
  VMWord _d;
  fields(){
    _a = field(VMWord);
    _b = field(VMWord);
    _c = field(VMWord);
    _d = field(VMWord);
  }
}
var Freetype_Matrix_size = 16;
var Freetype_Matrix_map = [0];
class Freetype_Vector extends VMStruct{
  VMWord _dx;
  VMWord _dy;
  fields(){
    _dx = field(VMWord);
    _dy = field(VMWord);
  }
}
var Freetype_Vector_size = 8;
var Freetype_Vector_map = [0];
class Freetype_Face extends VMStruct{
  VMWord _nfaces;
  VMWord _index;
  VMWord _style;
  VMWord _height;
  VMWord _ascent;
  VMPointer<VMString> _familyname;
  VMPointer<VMString> _stylename;
  fields(){
    _nfaces = field(VMWord);
    _index = field(VMWord);
    _style = field(VMWord);
    _height = field(VMWord);
    _ascent = field(VMWord);
    _familyname = new VMPointer<VMString>.field(this);
    _stylename = new VMPointer<VMString>.field(this);
  }
}
var Freetype_Face_size = 28;
var Freetype_Face_map = [0x6,];
class Freetype_Glyph extends VMStruct{
  VMWord _top;
  VMWord _left;
  VMWord _height;
  VMWord _width;
  Draw_Point _advance;
  VMPointer<VMArray> _bitmap;
  fields(){
    _top = field(VMWord);
    _left = field(VMWord);
    _height = field(VMWord);
    _width = field(VMWord);
    _advance = field(Draw_Point);
    _bitmap = new VMPointer<VMArray>.field(this);
  }
}
var Freetype_Glyph_size = 28;
var Freetype_Glyph_map = [0x2,];
class F_Sys_announce_ret extends VMStruct{
  VMWord _t0;
  Sys_Connection _t1;
  fields(){
    _t0 = field(VMWord);
    _t1 = field(Sys_Connection);
  }
}class F_Sys_announce extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<F_Sys_announce_ret> _ret;
  Array<VMByte> _temps;
  VMPointer<VMString> _addr;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<F_Sys_announce_ret>.field(this);
    _temps = new Array<VMByte>(this,12);
    _addr = new VMPointer<VMString>.field(this);
  }
}
class F_Sys_aprint extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<VMArray>> _ret;
  Array<VMByte> _temps;
  VMPointer<VMString> _s;
  VMWord _vargs;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<VMArray>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _s = new VMPointer<VMString>.field(this);
    _vargs = field(VMWord);
  }
}
class F_Sys_bind extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  VMPointer<VMString> _s;
  VMPointer<VMString> _on;
  VMWord _flags;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _s = new VMPointer<VMString>.field(this);
    _on = new VMPointer<VMString>.field(this);
    _flags = field(VMWord);
  }
}
class F_Sys_byte2char_ret extends VMStruct{
  VMWord _t0;
  VMWord _t1;
  VMWord _t2;
  fields(){
    _t0 = field(VMWord);
    _t1 = field(VMWord);
    _t2 = field(VMWord);
  }
}class F_Sys_byte2char extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<F_Sys_byte2char_ret> _ret;
  Array<VMByte> _temps;
  VMPointer<VMArray> _buf;
  VMWord _n;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<F_Sys_byte2char_ret>.field(this);
    _temps = new Array<VMByte>(this,12);
    _buf = new VMPointer<VMArray>.field(this);
    _n = field(VMWord);
  }
}
class F_Sys_char2byte extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  VMWord _c;
  VMPointer<VMArray> _buf;
  VMWord _n;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _c = field(VMWord);
    _buf = new VMPointer<VMArray>.field(this);
    _n = field(VMWord);
  }
}
class F_Sys_chdir extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  VMPointer<VMString> _path;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _path = new VMPointer<VMString>.field(this);
  }
}
class F_Sys_create extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Sys_FD>> _ret;
  Array<VMByte> _temps;
  VMPointer<VMString> _s;
  VMWord _mode;
  VMWord _perm;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Sys_FD>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _s = new VMPointer<VMString>.field(this);
    _mode = field(VMWord);
    _perm = field(VMWord);
  }
}
class F_Sys_dial_ret extends VMStruct{
  VMWord _t0;
  Sys_Connection _t1;
  fields(){
    _t0 = field(VMWord);
    _t1 = field(Sys_Connection);
  }
}class F_Sys_dial extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<F_Sys_dial_ret> _ret;
  Array<VMByte> _temps;
  VMPointer<VMString> _addr;
  VMPointer<VMString> _local;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<F_Sys_dial_ret>.field(this);
    _temps = new Array<VMByte>(this,12);
    _addr = new VMPointer<VMString>.field(this);
    _local = new VMPointer<VMString>.field(this);
  }
}
class F_Sys_dirread_ret extends VMStruct{
  VMWord _t0;
  VMPointer<VMArray> _t1;
  fields(){
    _t0 = field(VMWord);
    _t1 = new VMPointer<VMArray>.field(this);
  }
}class F_Sys_dirread extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<F_Sys_dirread_ret> _ret;
  Array<VMByte> _temps;
  VMPointer<Sys_FD> _fd;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<F_Sys_dirread_ret>.field(this);
    _temps = new Array<VMByte>(this,12);
    _fd = new VMPointer<Sys_FD>.field(this);
  }
}
class F_Sys_dup extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  VMWord _old;
  VMWord _new;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _old = field(VMWord);
    _new = field(VMWord);
  }
}
class F_Sys_export extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  VMPointer<Sys_FD> _c;
  VMPointer<VMString> _dir;
  VMWord _flag;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _c = new VMPointer<Sys_FD>.field(this);
    _dir = new VMPointer<VMString>.field(this);
    _flag = field(VMWord);
  }
}
class F_Sys_fauth extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Sys_FD>> _ret;
  Array<VMByte> _temps;
  VMPointer<Sys_FD> _fd;
  VMPointer<VMString> _aname;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Sys_FD>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _fd = new VMPointer<Sys_FD>.field(this);
    _aname = new VMPointer<VMString>.field(this);
  }
}
class F_Sys_fd2path extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<VMString>> _ret;
  Array<VMByte> _temps;
  VMPointer<Sys_FD> _fd;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<VMString>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _fd = new VMPointer<Sys_FD>.field(this);
  }
}
class F_Sys_fildes extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Sys_FD>> _ret;
  Array<VMByte> _temps;
  VMWord _fd;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Sys_FD>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _fd = field(VMWord);
  }
}
class F_Sys_file2chan extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Sys_FileIO>> _ret;
  Array<VMByte> _temps;
  VMPointer<VMString> _dir;
  VMPointer<VMString> _file;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Sys_FileIO>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _dir = new VMPointer<VMString>.field(this);
    _file = new VMPointer<VMString>.field(this);
  }
}
class F_Sys_fprint extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  VMPointer<Sys_FD> _fd;
  VMPointer<VMString> _s;
  VMWord _vargs;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _fd = new VMPointer<Sys_FD>.field(this);
    _s = new VMPointer<VMString>.field(this);
    _vargs = field(VMWord);
  }
}
class F_Sys_fstat_ret extends VMStruct{
  VMWord _t0;
  Array<VMByte> __pad4;
  Sys_Dir _t1;
  fields(){
    _t0 = field(VMWord);
    __pad4 = new Array<VMByte>(this,4);
    _t1 = field(Sys_Dir);
  }
}class F_Sys_fstat extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<F_Sys_fstat_ret> _ret;
  Array<VMByte> _temps;
  VMPointer<Sys_FD> _fd;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<F_Sys_fstat_ret>.field(this);
    _temps = new Array<VMByte>(this,12);
    _fd = new VMPointer<Sys_FD>.field(this);
  }
}
class F_Sys_fversion_ret extends VMStruct{
  VMWord _t0;
  VMPointer<VMString> _t1;
  fields(){
    _t0 = field(VMWord);
    _t1 = new VMPointer<VMString>.field(this);
  }
}class F_Sys_fversion extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<F_Sys_fversion_ret> _ret;
  Array<VMByte> _temps;
  VMPointer<Sys_FD> _fd;
  VMWord _msize;
  VMPointer<VMString> _version;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<F_Sys_fversion_ret>.field(this);
    _temps = new Array<VMByte>(this,12);
    _fd = new VMPointer<Sys_FD>.field(this);
    _msize = field(VMWord);
    _version = new VMPointer<VMString>.field(this);
  }
}
class F_Sys_fwstat extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  VMPointer<Sys_FD> _fd;
  Array<VMByte> __pad36;
  Sys_Dir _d;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _fd = new VMPointer<Sys_FD>.field(this);
    __pad36 = new Array<VMByte>(this,4);
    _d = field(Sys_Dir);
  }
}
class F_Sys_iounit extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  VMPointer<Sys_FD> _fd;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _fd = new VMPointer<Sys_FD>.field(this);
  }
}
class F_Sys_listen_ret extends VMStruct{
  VMWord _t0;
  Sys_Connection _t1;
  fields(){
    _t0 = field(VMWord);
    _t1 = field(Sys_Connection);
  }
}class F_Sys_listen extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<F_Sys_listen_ret> _ret;
  Array<VMByte> _temps;
  Sys_Connection _c;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<F_Sys_listen_ret>.field(this);
    _temps = new Array<VMByte>(this,12);
    _c = field(Sys_Connection);
  }
}
class F_Sys_millisec extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
  }
}
class F_Sys_mount extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  VMPointer<Sys_FD> _fd;
  VMPointer<Sys_FD> _afd;
  VMPointer<VMString> _on;
  VMWord _flags;
  VMPointer<VMString> _spec;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _fd = new VMPointer<Sys_FD>.field(this);
    _afd = new VMPointer<Sys_FD>.field(this);
    _on = new VMPointer<VMString>.field(this);
    _flags = field(VMWord);
    _spec = new VMPointer<VMString>.field(this);
  }
}
class F_Sys_open extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Sys_FD>> _ret;
  Array<VMByte> _temps;
  VMPointer<VMString> _s;
  VMWord _mode;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Sys_FD>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _s = new VMPointer<VMString>.field(this);
    _mode = field(VMWord);
  }
}
class F_Sys_pctl extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  VMWord _flags;
  VMPointer<VMList> _movefd;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _flags = field(VMWord);
    _movefd = new VMPointer<VMList>.field(this);
  }
}
class F_Sys_pipe extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  VMPointer<VMArray> _fds;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _fds = new VMPointer<VMArray>.field(this);
  }
}
class F_Sys_pread extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  VMPointer<Sys_FD> _fd;
  VMPointer<VMArray> _buf;
  VMWord _n;
  Array<VMByte> __pad44;
  VMLong _off;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _fd = new VMPointer<Sys_FD>.field(this);
    _buf = new VMPointer<VMArray>.field(this);
    _n = field(VMWord);
    __pad44 = new Array<VMByte>(this,4);
    _off = field(VMLong);
  }
}
class F_Sys_print extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  VMPointer<VMString> _s;
  VMWord _vargs;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _s = new VMPointer<VMString>.field(this);
    _vargs = field(VMWord);
  }
}
class F_Sys_pwrite extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  VMPointer<Sys_FD> _fd;
  VMPointer<VMArray> _buf;
  VMWord _n;
  Array<VMByte> __pad44;
  VMLong _off;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _fd = new VMPointer<Sys_FD>.field(this);
    _buf = new VMPointer<VMArray>.field(this);
    _n = field(VMWord);
    __pad44 = new Array<VMByte>(this,4);
    _off = field(VMLong);
  }
}
class F_Sys_read extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  VMPointer<Sys_FD> _fd;
  VMPointer<VMArray> _buf;
  VMWord _n;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _fd = new VMPointer<Sys_FD>.field(this);
    _buf = new VMPointer<VMArray>.field(this);
    _n = field(VMWord);
  }
}
class F_Sys_readn extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  VMPointer<Sys_FD> _fd;
  VMPointer<VMArray> _buf;
  VMWord _n;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _fd = new VMPointer<Sys_FD>.field(this);
    _buf = new VMPointer<VMArray>.field(this);
    _n = field(VMWord);
  }
}
class F_Sys_remove extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  VMPointer<VMString> _s;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _s = new VMPointer<VMString>.field(this);
  }
}
class F_Sys_seek extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMLong> _ret;
  Array<VMByte> _temps;
  VMPointer<Sys_FD> _fd;
  Array<VMByte> __pad36;
  VMLong _off;
  VMWord _start;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMLong>.field(this);
    _temps = new Array<VMByte>(this,12);
    _fd = new VMPointer<Sys_FD>.field(this);
    __pad36 = new Array<VMByte>(this,4);
    _off = field(VMLong);
    _start = field(VMWord);
  }
}
class F_Sys_sleep extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  VMWord _period;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _period = field(VMWord);
  }
}
class F_Sys_sprint extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<VMString>> _ret;
  Array<VMByte> _temps;
  VMPointer<VMString> _s;
  VMWord _vargs;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<VMString>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _s = new VMPointer<VMString>.field(this);
    _vargs = field(VMWord);
  }
}
class F_Sys_stat_ret extends VMStruct{
  VMWord _t0;
  Array<VMByte> __pad4;
  Sys_Dir _t1;
  fields(){
    _t0 = field(VMWord);
    __pad4 = new Array<VMByte>(this,4);
    _t1 = field(Sys_Dir);
  }
}class F_Sys_stat extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<F_Sys_stat_ret> _ret;
  Array<VMByte> _temps;
  VMPointer<VMString> _s;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<F_Sys_stat_ret>.field(this);
    _temps = new Array<VMByte>(this,12);
    _s = new VMPointer<VMString>.field(this);
  }
}
class F_Sys_stream extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  VMPointer<Sys_FD> _src;
  VMPointer<Sys_FD> _dst;
  VMWord _bufsiz;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _src = new VMPointer<Sys_FD>.field(this);
    _dst = new VMPointer<Sys_FD>.field(this);
    _bufsiz = field(VMWord);
  }
}
class F_Sys_tokenize_ret extends VMStruct{
  VMWord _t0;
  VMPointer<VMList> _t1;
  fields(){
    _t0 = field(VMWord);
    _t1 = new VMPointer<VMList>.field(this);
  }
}class F_Sys_tokenize extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<F_Sys_tokenize_ret> _ret;
  Array<VMByte> _temps;
  VMPointer<VMString> _s;
  VMPointer<VMString> _delim;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<F_Sys_tokenize_ret>.field(this);
    _temps = new Array<VMByte>(this,12);
    _s = new VMPointer<VMString>.field(this);
    _delim = new VMPointer<VMString>.field(this);
  }
}
class F_Sys_unmount extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  VMPointer<VMString> _s1;
  VMPointer<VMString> _s2;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _s1 = new VMPointer<VMString>.field(this);
    _s2 = new VMPointer<VMString>.field(this);
  }
}
class F_Sys_utfbytes extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  VMPointer<VMArray> _buf;
  VMWord _n;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _buf = new VMPointer<VMArray>.field(this);
    _n = field(VMWord);
  }
}
class F_Sys_werrstr extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  VMPointer<VMString> _s;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _s = new VMPointer<VMString>.field(this);
  }
}
class F_Sys_write extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  VMPointer<Sys_FD> _fd;
  VMPointer<VMArray> _buf;
  VMWord _n;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _fd = new VMPointer<Sys_FD>.field(this);
    _buf = new VMPointer<VMArray>.field(this);
    _n = field(VMWord);
  }
}
class F_Sys_wstat extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  VMPointer<VMString> _s;
  Array<VMByte> __pad36;
  Sys_Dir _d;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _s = new VMPointer<VMString>.field(this);
    __pad36 = new Array<VMByte>(this,4);
    _d = field(Sys_Dir);
  }
}
var Sys_PATH = r'$Sys';
var Sys_Maxint = 2147483647;
var Sys_QTDIR = 128;
var Sys_QTAPPEND = 64;
var Sys_QTEXCL = 32;
var Sys_QTAUTH = 8;
var Sys_QTTMP = 4;
var Sys_QTFILE = 0;
var Sys_ATOMICIO = 8192;
var Sys_SEEKSTART = 0;
var Sys_SEEKRELA = 1;
var Sys_SEEKEND = 2;
var Sys_NAMEMAX = 256;
var Sys_ERRMAX = 128;
var Sys_WAITLEN = 192;
var Sys_OREAD = 0;
var Sys_OWRITE = 1;
var Sys_ORDWR = 2;
var Sys_OTRUNC = 16;
var Sys_ORCLOSE = 64;
var Sys_OEXCL = 4096;
var Sys_DMDIR = -2147483648;
var Sys_DMAPPEND = 1073741824;
var Sys_DMEXCL = 536870912;
var Sys_DMAUTH = 134217728;
var Sys_DMTMP = 67108864;
var Sys_MREPL = 0;
var Sys_MBEFORE = 1;
var Sys_MAFTER = 2;
var Sys_MCREATE = 4;
var Sys_MCACHE = 16;
var Sys_NEWFD = 1;
var Sys_FORKFD = 2;
var Sys_NEWNS = 4;
var Sys_FORKNS = 8;
var Sys_NEWPGRP = 16;
var Sys_NODEVS = 32;
var Sys_NEWENV = 64;
var Sys_FORKENV = 128;
var Sys_EXPWAIT = 0;
var Sys_EXPASYNC = 1;
var Sys_UTFmax = 3;
var Sys_UTFerror = 128;
class F_Rect_Xrect extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  Draw_Rect _r;
  Draw_Rect _s;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _r = field(Draw_Rect);
    _s = field(Draw_Rect);
  }
}
class F_Point_add extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<Draw_Point> _ret;
  Array<VMByte> _temps;
  Draw_Point _p;
  Draw_Point _q;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<Draw_Point>.field(this);
    _temps = new Array<VMByte>(this,12);
    _p = field(Draw_Point);
    _q = field(Draw_Point);
  }
}
class F_Rect_addpt extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<Draw_Rect> _ret;
  Array<VMByte> _temps;
  Draw_Rect _r;
  Draw_Point _p;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<Draw_Rect>.field(this);
    _temps = new Array<VMByte>(this,12);
    _r = field(Draw_Rect);
    _p = field(Draw_Point);
  }
}
class F_Display_allocate extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Draw_Display>> _ret;
  Array<VMByte> _temps;
  VMPointer<VMString> _dev;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Draw_Display>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _dev = new VMPointer<VMString>.field(this);
  }
}
class F_Screen_allocate extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Draw_Screen>> _ret;
  Array<VMByte> _temps;
  VMPointer<Draw_Image> _image;
  VMPointer<Draw_Image> _fill;
  VMWord _public;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Draw_Screen>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _image = new VMPointer<Draw_Image>.field(this);
    _fill = new VMPointer<Draw_Image>.field(this);
    _public = field(VMWord);
  }
}
class F_Image_arc extends VMStruct{
  Array<VMWord> _regs;
  VMWord _noret;
  Array<VMByte> _temps;
  VMPointer<Draw_Image> _dst;
  Draw_Point _c;
  VMWord _a;
  VMWord _b;
  VMWord _thick;
  VMPointer<Draw_Image> _src;
  Draw_Point _sp;
  VMWord _alpha;
  VMWord _phi;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _noret = field(VMWord);
    _temps = new Array<VMByte>(this,12);
    _dst = new VMPointer<Draw_Image>.field(this);
    _c = field(Draw_Point);
    _a = field(VMWord);
    _b = field(VMWord);
    _thick = field(VMWord);
    _src = new VMPointer<Draw_Image>.field(this);
    _sp = field(Draw_Point);
    _alpha = field(VMWord);
    _phi = field(VMWord);
  }
}
class F_Image_arcop extends VMStruct{
  Array<VMWord> _regs;
  VMWord _noret;
  Array<VMByte> _temps;
  VMPointer<Draw_Image> _dst;
  Draw_Point _c;
  VMWord _a;
  VMWord _b;
  VMWord _thick;
  VMPointer<Draw_Image> _src;
  Draw_Point _sp;
  VMWord _alpha;
  VMWord _phi;
  VMWord _op;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _noret = field(VMWord);
    _temps = new Array<VMByte>(this,12);
    _dst = new VMPointer<Draw_Image>.field(this);
    _c = field(Draw_Point);
    _a = field(VMWord);
    _b = field(VMWord);
    _thick = field(VMWord);
    _src = new VMPointer<Draw_Image>.field(this);
    _sp = field(Draw_Point);
    _alpha = field(VMWord);
    _phi = field(VMWord);
    _op = field(VMWord);
  }
}
class F_Image_arrow extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  VMWord _a;
  VMWord _b;
  VMWord _c;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _a = field(VMWord);
    _b = field(VMWord);
    _c = field(VMWord);
  }
}
class F_Font_bbox extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<Draw_Rect> _ret;
  Array<VMByte> _temps;
  VMPointer<Draw_Font> _f;
  VMPointer<VMString> _str;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<Draw_Rect>.field(this);
    _temps = new Array<VMByte>(this,12);
    _f = new VMPointer<Draw_Font>.field(this);
    _str = new VMPointer<VMString>.field(this);
  }
}
class F_Image_bezier extends VMStruct{
  Array<VMWord> _regs;
  VMWord _noret;
  Array<VMByte> _temps;
  VMPointer<Draw_Image> _dst;
  Draw_Point _a;
  Draw_Point _b;
  Draw_Point _c;
  Draw_Point _d;
  VMWord _end0;
  VMWord _end1;
  VMWord _radius;
  VMPointer<Draw_Image> _src;
  Draw_Point _sp;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _noret = field(VMWord);
    _temps = new Array<VMByte>(this,12);
    _dst = new VMPointer<Draw_Image>.field(this);
    _a = field(Draw_Point);
    _b = field(Draw_Point);
    _c = field(Draw_Point);
    _d = field(Draw_Point);
    _end0 = field(VMWord);
    _end1 = field(VMWord);
    _radius = field(VMWord);
    _src = new VMPointer<Draw_Image>.field(this);
    _sp = field(Draw_Point);
  }
}
class F_Image_bezierop extends VMStruct{
  Array<VMWord> _regs;
  VMWord _noret;
  Array<VMByte> _temps;
  VMPointer<Draw_Image> _dst;
  Draw_Point _a;
  Draw_Point _b;
  Draw_Point _c;
  Draw_Point _d;
  VMWord _end0;
  VMWord _end1;
  VMWord _radius;
  VMPointer<Draw_Image> _src;
  Draw_Point _sp;
  VMWord _op;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _noret = field(VMWord);
    _temps = new Array<VMByte>(this,12);
    _dst = new VMPointer<Draw_Image>.field(this);
    _a = field(Draw_Point);
    _b = field(Draw_Point);
    _c = field(Draw_Point);
    _d = field(Draw_Point);
    _end0 = field(VMWord);
    _end1 = field(VMWord);
    _radius = field(VMWord);
    _src = new VMPointer<Draw_Image>.field(this);
    _sp = field(Draw_Point);
    _op = field(VMWord);
  }
}
class F_Image_bezspline extends VMStruct{
  Array<VMWord> _regs;
  VMWord _noret;
  Array<VMByte> _temps;
  VMPointer<Draw_Image> _dst;
  VMPointer<VMArray> _p;
  VMWord _end0;
  VMWord _end1;
  VMWord _radius;
  VMPointer<Draw_Image> _src;
  Draw_Point _sp;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _noret = field(VMWord);
    _temps = new Array<VMByte>(this,12);
    _dst = new VMPointer<Draw_Image>.field(this);
    _p = new VMPointer<VMArray>.field(this);
    _end0 = field(VMWord);
    _end1 = field(VMWord);
    _radius = field(VMWord);
    _src = new VMPointer<Draw_Image>.field(this);
    _sp = field(Draw_Point);
  }
}
class F_Image_bezsplineop extends VMStruct{
  Array<VMWord> _regs;
  VMWord _noret;
  Array<VMByte> _temps;
  VMPointer<Draw_Image> _dst;
  VMPointer<VMArray> _p;
  VMWord _end0;
  VMWord _end1;
  VMWord _radius;
  VMPointer<Draw_Image> _src;
  Draw_Point _sp;
  VMWord _op;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _noret = field(VMWord);
    _temps = new Array<VMByte>(this,12);
    _dst = new VMPointer<Draw_Image>.field(this);
    _p = new VMPointer<VMArray>.field(this);
    _end0 = field(VMWord);
    _end1 = field(VMWord);
    _radius = field(VMWord);
    _src = new VMPointer<Draw_Image>.field(this);
    _sp = field(Draw_Point);
    _op = field(VMWord);
  }
}
class F_Image_border extends VMStruct{
  Array<VMWord> _regs;
  VMWord _noret;
  Array<VMByte> _temps;
  VMPointer<Draw_Image> _dst;
  Draw_Rect _r;
  VMWord _i;
  VMPointer<Draw_Image> _src;
  Draw_Point _sp;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _noret = field(VMWord);
    _temps = new Array<VMByte>(this,12);
    _dst = new VMPointer<Draw_Image>.field(this);
    _r = field(Draw_Rect);
    _i = field(VMWord);
    _src = new VMPointer<Draw_Image>.field(this);
    _sp = field(Draw_Point);
  }
}
class F_Image_bottom extends VMStruct{
  Array<VMWord> _regs;
  VMWord _noret;
  Array<VMByte> _temps;
  VMPointer<Draw_Image> _win;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _noret = field(VMWord);
    _temps = new Array<VMByte>(this,12);
    _win = new VMPointer<Draw_Image>.field(this);
  }
}
class F_Screen_bottom extends VMStruct{
  Array<VMWord> _regs;
  VMWord _noret;
  Array<VMByte> _temps;
  VMPointer<Draw_Screen> _screen;
  VMPointer<VMArray> _wins;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _noret = field(VMWord);
    _temps = new Array<VMByte>(this,12);
    _screen = new VMPointer<Draw_Screen>.field(this);
    _wins = new VMPointer<VMArray>.field(this);
  }
}
class F_Font_build extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Draw_Font>> _ret;
  Array<VMByte> _temps;
  VMPointer<Draw_Display> _d;
  VMPointer<VMString> _name;
  VMPointer<VMString> _desc;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Draw_Font>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _d = new VMPointer<Draw_Display>.field(this);
    _name = new VMPointer<VMString>.field(this);
    _desc = new VMPointer<VMString>.field(this);
  }
}
class F_Draw_bytesperline extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  Draw_Rect _r;
  VMWord _d;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _r = field(Draw_Rect);
    _d = field(VMWord);
  }
}
class F_Rect_canon extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<Draw_Rect> _ret;
  Array<VMByte> _temps;
  Draw_Rect _r;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<Draw_Rect>.field(this);
    _temps = new Array<VMByte>(this,12);
    _r = field(Draw_Rect);
  }
}
class F_Rect_clip_ret extends VMStruct{
  Draw_Rect _t0;
  VMWord _t1;
  fields(){
    _t0 = field(Draw_Rect);
    _t1 = field(VMWord);
  }
}class F_Rect_clip extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<F_Rect_clip_ret> _ret;
  Array<VMByte> _temps;
  Draw_Rect _r;
  Draw_Rect _s;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<F_Rect_clip_ret>.field(this);
    _temps = new Array<VMByte>(this,12);
    _r = field(Draw_Rect);
    _s = field(Draw_Rect);
  }
}
class F_Display_cmap2rgb_ret extends VMStruct{
  VMWord _t0;
  VMWord _t1;
  VMWord _t2;
  fields(){
    _t0 = field(VMWord);
    _t1 = field(VMWord);
    _t2 = field(VMWord);
  }
}class F_Display_cmap2rgb extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<F_Display_cmap2rgb_ret> _ret;
  Array<VMByte> _temps;
  VMPointer<Draw_Display> _d;
  VMWord _c;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<F_Display_cmap2rgb_ret>.field(this);
    _temps = new Array<VMByte>(this,12);
    _d = new VMPointer<Draw_Display>.field(this);
    _c = field(VMWord);
  }
}
class F_Display_cmap2rgba extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  VMPointer<Draw_Display> _d;
  VMWord _c;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _d = new VMPointer<Draw_Display>.field(this);
    _c = field(VMWord);
  }
}
class F_Display_color extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Draw_Image>> _ret;
  Array<VMByte> _temps;
  VMPointer<Draw_Display> _d;
  VMWord _color;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Draw_Image>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _d = new VMPointer<Draw_Display>.field(this);
    _color = field(VMWord);
  }
}
class F_Display_colormix extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Draw_Image>> _ret;
  Array<VMByte> _temps;
  VMPointer<Draw_Display> _d;
  VMWord _c1;
  VMWord _c2;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Draw_Image>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _d = new VMPointer<Draw_Display>.field(this);
    _c1 = field(VMWord);
    _c2 = field(VMWord);
  }
}
class F_Rect_combine extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<Draw_Rect> _ret;
  Array<VMByte> _temps;
  Draw_Rect _r;
  Draw_Rect _s;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<Draw_Rect>.field(this);
    _temps = new Array<VMByte>(this,12);
    _r = field(Draw_Rect);
    _s = field(Draw_Rect);
  }
}
class F_Rect_contains extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  Draw_Rect _r;
  Draw_Point _p;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _r = field(Draw_Rect);
    _p = field(Draw_Point);
  }
}
class F_Chans_depth extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  Draw_Chans _c;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _c = field(Draw_Chans);
  }
}
class F_Point_div extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<Draw_Point> _ret;
  Array<VMByte> _temps;
  Draw_Point _p;
  VMWord _i;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<Draw_Point>.field(this);
    _temps = new Array<VMByte>(this,12);
    _p = field(Draw_Point);
    _i = field(VMWord);
  }
}
class F_Image_draw extends VMStruct{
  Array<VMWord> _regs;
  VMWord _noret;
  Array<VMByte> _temps;
  VMPointer<Draw_Image> _dst;
  Draw_Rect _r;
  VMPointer<Draw_Image> _src;
  VMPointer<Draw_Image> _matte;
  Draw_Point _p;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _noret = field(VMWord);
    _temps = new Array<VMByte>(this,12);
    _dst = new VMPointer<Draw_Image>.field(this);
    _r = field(Draw_Rect);
    _src = new VMPointer<Draw_Image>.field(this);
    _matte = new VMPointer<Draw_Image>.field(this);
    _p = field(Draw_Point);
  }
}
class F_Image_drawop extends VMStruct{
  Array<VMWord> _regs;
  VMWord _noret;
  Array<VMByte> _temps;
  VMPointer<Draw_Image> _dst;
  Draw_Rect _r;
  VMPointer<Draw_Image> _src;
  VMPointer<Draw_Image> _matte;
  Draw_Point _p;
  VMWord _op;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _noret = field(VMWord);
    _temps = new Array<VMByte>(this,12);
    _dst = new VMPointer<Draw_Image>.field(this);
    _r = field(Draw_Rect);
    _src = new VMPointer<Draw_Image>.field(this);
    _matte = new VMPointer<Draw_Image>.field(this);
    _p = field(Draw_Point);
    _op = field(VMWord);
  }
}
class F_Rect_dx extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  Draw_Rect _r;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _r = field(Draw_Rect);
  }
}
class F_Rect_dy extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  Draw_Rect _r;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _r = field(Draw_Rect);
  }
}
class F_Image_ellipse extends VMStruct{
  Array<VMWord> _regs;
  VMWord _noret;
  Array<VMByte> _temps;
  VMPointer<Draw_Image> _dst;
  Draw_Point _c;
  VMWord _a;
  VMWord _b;
  VMWord _thick;
  VMPointer<Draw_Image> _src;
  Draw_Point _sp;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _noret = field(VMWord);
    _temps = new Array<VMByte>(this,12);
    _dst = new VMPointer<Draw_Image>.field(this);
    _c = field(Draw_Point);
    _a = field(VMWord);
    _b = field(VMWord);
    _thick = field(VMWord);
    _src = new VMPointer<Draw_Image>.field(this);
    _sp = field(Draw_Point);
  }
}
class F_Image_ellipseop extends VMStruct{
  Array<VMWord> _regs;
  VMWord _noret;
  Array<VMByte> _temps;
  VMPointer<Draw_Image> _dst;
  Draw_Point _c;
  VMWord _a;
  VMWord _b;
  VMWord _thick;
  VMPointer<Draw_Image> _src;
  Draw_Point _sp;
  VMWord _op;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _noret = field(VMWord);
    _temps = new Array<VMByte>(this,12);
    _dst = new VMPointer<Draw_Image>.field(this);
    _c = field(Draw_Point);
    _a = field(VMWord);
    _b = field(VMWord);
    _thick = field(VMWord);
    _src = new VMPointer<Draw_Image>.field(this);
    _sp = field(Draw_Point);
    _op = field(VMWord);
  }
}
class F_Chans_eq extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  Draw_Chans _c;
  Draw_Chans _d;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _c = field(Draw_Chans);
    _d = field(Draw_Chans);
  }
}
class F_Point_eq extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  Draw_Point _p;
  Draw_Point _q;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _p = field(Draw_Point);
    _q = field(Draw_Point);
  }
}
class F_Rect_eq extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  Draw_Rect _r;
  Draw_Rect _s;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _r = field(Draw_Rect);
    _s = field(Draw_Rect);
  }
}
class F_Image_fillarc extends VMStruct{
  Array<VMWord> _regs;
  VMWord _noret;
  Array<VMByte> _temps;
  VMPointer<Draw_Image> _dst;
  Draw_Point _c;
  VMWord _a;
  VMWord _b;
  VMPointer<Draw_Image> _src;
  Draw_Point _sp;
  VMWord _alpha;
  VMWord _phi;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _noret = field(VMWord);
    _temps = new Array<VMByte>(this,12);
    _dst = new VMPointer<Draw_Image>.field(this);
    _c = field(Draw_Point);
    _a = field(VMWord);
    _b = field(VMWord);
    _src = new VMPointer<Draw_Image>.field(this);
    _sp = field(Draw_Point);
    _alpha = field(VMWord);
    _phi = field(VMWord);
  }
}
class F_Image_fillarcop extends VMStruct{
  Array<VMWord> _regs;
  VMWord _noret;
  Array<VMByte> _temps;
  VMPointer<Draw_Image> _dst;
  Draw_Point _c;
  VMWord _a;
  VMWord _b;
  VMPointer<Draw_Image> _src;
  Draw_Point _sp;
  VMWord _alpha;
  VMWord _phi;
  VMWord _op;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _noret = field(VMWord);
    _temps = new Array<VMByte>(this,12);
    _dst = new VMPointer<Draw_Image>.field(this);
    _c = field(Draw_Point);
    _a = field(VMWord);
    _b = field(VMWord);
    _src = new VMPointer<Draw_Image>.field(this);
    _sp = field(Draw_Point);
    _alpha = field(VMWord);
    _phi = field(VMWord);
    _op = field(VMWord);
  }
}
class F_Image_fillbezier extends VMStruct{
  Array<VMWord> _regs;
  VMWord _noret;
  Array<VMByte> _temps;
  VMPointer<Draw_Image> _dst;
  Draw_Point _a;
  Draw_Point _b;
  Draw_Point _c;
  Draw_Point _d;
  VMWord _wind;
  VMPointer<Draw_Image> _src;
  Draw_Point _sp;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _noret = field(VMWord);
    _temps = new Array<VMByte>(this,12);
    _dst = new VMPointer<Draw_Image>.field(this);
    _a = field(Draw_Point);
    _b = field(Draw_Point);
    _c = field(Draw_Point);
    _d = field(Draw_Point);
    _wind = field(VMWord);
    _src = new VMPointer<Draw_Image>.field(this);
    _sp = field(Draw_Point);
  }
}
class F_Image_fillbezierop extends VMStruct{
  Array<VMWord> _regs;
  VMWord _noret;
  Array<VMByte> _temps;
  VMPointer<Draw_Image> _dst;
  Draw_Point _a;
  Draw_Point _b;
  Draw_Point _c;
  Draw_Point _d;
  VMWord _wind;
  VMPointer<Draw_Image> _src;
  Draw_Point _sp;
  VMWord _op;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _noret = field(VMWord);
    _temps = new Array<VMByte>(this,12);
    _dst = new VMPointer<Draw_Image>.field(this);
    _a = field(Draw_Point);
    _b = field(Draw_Point);
    _c = field(Draw_Point);
    _d = field(Draw_Point);
    _wind = field(VMWord);
    _src = new VMPointer<Draw_Image>.field(this);
    _sp = field(Draw_Point);
    _op = field(VMWord);
  }
}
class F_Image_fillbezspline extends VMStruct{
  Array<VMWord> _regs;
  VMWord _noret;
  Array<VMByte> _temps;
  VMPointer<Draw_Image> _dst;
  VMPointer<VMArray> _p;
  VMWord _wind;
  VMPointer<Draw_Image> _src;
  Draw_Point _sp;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _noret = field(VMWord);
    _temps = new Array<VMByte>(this,12);
    _dst = new VMPointer<Draw_Image>.field(this);
    _p = new VMPointer<VMArray>.field(this);
    _wind = field(VMWord);
    _src = new VMPointer<Draw_Image>.field(this);
    _sp = field(Draw_Point);
  }
}
class F_Image_fillbezsplineop extends VMStruct{
  Array<VMWord> _regs;
  VMWord _noret;
  Array<VMByte> _temps;
  VMPointer<Draw_Image> _dst;
  VMPointer<VMArray> _p;
  VMWord _wind;
  VMPointer<Draw_Image> _src;
  Draw_Point _sp;
  VMWord _op;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _noret = field(VMWord);
    _temps = new Array<VMByte>(this,12);
    _dst = new VMPointer<Draw_Image>.field(this);
    _p = new VMPointer<VMArray>.field(this);
    _wind = field(VMWord);
    _src = new VMPointer<Draw_Image>.field(this);
    _sp = field(Draw_Point);
    _op = field(VMWord);
  }
}
class F_Image_fillellipse extends VMStruct{
  Array<VMWord> _regs;
  VMWord _noret;
  Array<VMByte> _temps;
  VMPointer<Draw_Image> _dst;
  Draw_Point _c;
  VMWord _a;
  VMWord _b;
  VMPointer<Draw_Image> _src;
  Draw_Point _sp;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _noret = field(VMWord);
    _temps = new Array<VMByte>(this,12);
    _dst = new VMPointer<Draw_Image>.field(this);
    _c = field(Draw_Point);
    _a = field(VMWord);
    _b = field(VMWord);
    _src = new VMPointer<Draw_Image>.field(this);
    _sp = field(Draw_Point);
  }
}
class F_Image_fillellipseop extends VMStruct{
  Array<VMWord> _regs;
  VMWord _noret;
  Array<VMByte> _temps;
  VMPointer<Draw_Image> _dst;
  Draw_Point _c;
  VMWord _a;
  VMWord _b;
  VMPointer<Draw_Image> _src;
  Draw_Point _sp;
  VMWord _op;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _noret = field(VMWord);
    _temps = new Array<VMByte>(this,12);
    _dst = new VMPointer<Draw_Image>.field(this);
    _c = field(Draw_Point);
    _a = field(VMWord);
    _b = field(VMWord);
    _src = new VMPointer<Draw_Image>.field(this);
    _sp = field(Draw_Point);
    _op = field(VMWord);
  }
}
class F_Image_fillpoly extends VMStruct{
  Array<VMWord> _regs;
  VMWord _noret;
  Array<VMByte> _temps;
  VMPointer<Draw_Image> _dst;
  VMPointer<VMArray> _p;
  VMWord _wind;
  VMPointer<Draw_Image> _src;
  Draw_Point _sp;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _noret = field(VMWord);
    _temps = new Array<VMByte>(this,12);
    _dst = new VMPointer<Draw_Image>.field(this);
    _p = new VMPointer<VMArray>.field(this);
    _wind = field(VMWord);
    _src = new VMPointer<Draw_Image>.field(this);
    _sp = field(Draw_Point);
  }
}
class F_Image_fillpolyop extends VMStruct{
  Array<VMWord> _regs;
  VMWord _noret;
  Array<VMByte> _temps;
  VMPointer<Draw_Image> _dst;
  VMPointer<VMArray> _p;
  VMWord _wind;
  VMPointer<Draw_Image> _src;
  Draw_Point _sp;
  VMWord _op;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _noret = field(VMWord);
    _temps = new Array<VMByte>(this,12);
    _dst = new VMPointer<Draw_Image>.field(this);
    _p = new VMPointer<VMArray>.field(this);
    _wind = field(VMWord);
    _src = new VMPointer<Draw_Image>.field(this);
    _sp = field(Draw_Point);
    _op = field(VMWord);
  }
}
class F_Image_flush extends VMStruct{
  Array<VMWord> _regs;
  VMWord _noret;
  Array<VMByte> _temps;
  VMPointer<Draw_Image> _win;
  VMWord _func;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _noret = field(VMWord);
    _temps = new Array<VMByte>(this,12);
    _win = new VMPointer<Draw_Image>.field(this);
    _func = field(VMWord);
  }
}
class F_Image_gendraw extends VMStruct{
  Array<VMWord> _regs;
  VMWord _noret;
  Array<VMByte> _temps;
  VMPointer<Draw_Image> _dst;
  Draw_Rect _r;
  VMPointer<Draw_Image> _src;
  Draw_Point _p0;
  VMPointer<Draw_Image> _matte;
  Draw_Point _p1;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _noret = field(VMWord);
    _temps = new Array<VMByte>(this,12);
    _dst = new VMPointer<Draw_Image>.field(this);
    _r = field(Draw_Rect);
    _src = new VMPointer<Draw_Image>.field(this);
    _p0 = field(Draw_Point);
    _matte = new VMPointer<Draw_Image>.field(this);
    _p1 = field(Draw_Point);
  }
}
class F_Image_gendrawop extends VMStruct{
  Array<VMWord> _regs;
  VMWord _noret;
  Array<VMByte> _temps;
  VMPointer<Draw_Image> _dst;
  Draw_Rect _r;
  VMPointer<Draw_Image> _src;
  Draw_Point _p0;
  VMPointer<Draw_Image> _matte;
  Draw_Point _p1;
  VMWord _op;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _noret = field(VMWord);
    _temps = new Array<VMByte>(this,12);
    _dst = new VMPointer<Draw_Image>.field(this);
    _r = field(Draw_Rect);
    _src = new VMPointer<Draw_Image>.field(this);
    _p0 = field(Draw_Point);
    _matte = new VMPointer<Draw_Image>.field(this);
    _p1 = field(Draw_Point);
    _op = field(VMWord);
  }
}
class F_Display_getwindow_ret extends VMStruct{
  VMPointer<Draw_Screen> _t0;
  VMPointer<Draw_Image> _t1;
  fields(){
    _t0 = new VMPointer<Draw_Screen>.field(this);
    _t1 = new VMPointer<Draw_Image>.field(this);
  }
}class F_Display_getwindow extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<F_Display_getwindow_ret> _ret;
  Array<VMByte> _temps;
  VMPointer<Draw_Display> _d;
  VMPointer<VMString> _winname;
  VMPointer<Draw_Screen> _screen;
  VMPointer<Draw_Image> _image;
  VMWord _backup;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<F_Display_getwindow_ret>.field(this);
    _temps = new Array<VMByte>(this,12);
    _d = new VMPointer<Draw_Display>.field(this);
    _winname = new VMPointer<VMString>.field(this);
    _screen = new VMPointer<Draw_Screen>.field(this);
    _image = new VMPointer<Draw_Image>.field(this);
    _backup = field(VMWord);
  }
}
class F_Draw_icossin_ret extends VMStruct{
  VMWord _t0;
  VMWord _t1;
  fields(){
    _t0 = field(VMWord);
    _t1 = field(VMWord);
  }
}class F_Draw_icossin extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<F_Draw_icossin_ret> _ret;
  Array<VMByte> _temps;
  VMWord _deg;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<F_Draw_icossin_ret>.field(this);
    _temps = new Array<VMByte>(this,12);
    _deg = field(VMWord);
  }
}
class F_Draw_icossin2_ret extends VMStruct{
  VMWord _t0;
  VMWord _t1;
  fields(){
    _t0 = field(VMWord);
    _t1 = field(VMWord);
  }
}class F_Draw_icossin2 extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<F_Draw_icossin2_ret> _ret;
  Array<VMByte> _temps;
  Draw_Point _p;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<F_Draw_icossin2_ret>.field(this);
    _temps = new Array<VMByte>(this,12);
    _p = field(Draw_Point);
  }
}
class F_Point_in extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  Draw_Point _p;
  Draw_Rect _r;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _p = field(Draw_Point);
    _r = field(Draw_Rect);
  }
}
class F_Rect_inrect extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  Draw_Rect _r;
  Draw_Rect _s;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _r = field(Draw_Rect);
    _s = field(Draw_Rect);
  }
}
class F_Rect_inset extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<Draw_Rect> _ret;
  Array<VMByte> _temps;
  Draw_Rect _r;
  VMWord _n;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<Draw_Rect>.field(this);
    _temps = new Array<VMByte>(this,12);
    _r = field(Draw_Rect);
    _n = field(VMWord);
  }
}
class F_Image_line extends VMStruct{
  Array<VMWord> _regs;
  VMWord _noret;
  Array<VMByte> _temps;
  VMPointer<Draw_Image> _dst;
  Draw_Point _p0;
  Draw_Point _p1;
  VMWord _end0;
  VMWord _end1;
  VMWord _radius;
  VMPointer<Draw_Image> _src;
  Draw_Point _sp;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _noret = field(VMWord);
    _temps = new Array<VMByte>(this,12);
    _dst = new VMPointer<Draw_Image>.field(this);
    _p0 = field(Draw_Point);
    _p1 = field(Draw_Point);
    _end0 = field(VMWord);
    _end1 = field(VMWord);
    _radius = field(VMWord);
    _src = new VMPointer<Draw_Image>.field(this);
    _sp = field(Draw_Point);
  }
}
class F_Image_lineop extends VMStruct{
  Array<VMWord> _regs;
  VMWord _noret;
  Array<VMByte> _temps;
  VMPointer<Draw_Image> _dst;
  Draw_Point _p0;
  Draw_Point _p1;
  VMWord _end0;
  VMWord _end1;
  VMWord _radius;
  VMPointer<Draw_Image> _src;
  Draw_Point _sp;
  VMWord _op;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _noret = field(VMWord);
    _temps = new Array<VMByte>(this,12);
    _dst = new VMPointer<Draw_Image>.field(this);
    _p0 = field(Draw_Point);
    _p1 = field(Draw_Point);
    _end0 = field(VMWord);
    _end1 = field(VMWord);
    _radius = field(VMWord);
    _src = new VMPointer<Draw_Image>.field(this);
    _sp = field(Draw_Point);
    _op = field(VMWord);
  }
}
class F_Chans_mk extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<Draw_Chans> _ret;
  Array<VMByte> _temps;
  VMPointer<VMString> _s;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<Draw_Chans>.field(this);
    _temps = new Array<VMByte>(this,12);
    _s = new VMPointer<VMString>.field(this);
  }
}
class F_Point_mul extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<Draw_Point> _ret;
  Array<VMByte> _temps;
  Draw_Point _p;
  VMWord _i;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<Draw_Point>.field(this);
    _temps = new Array<VMByte>(this,12);
    _p = field(Draw_Point);
    _i = field(VMWord);
  }
}
class F_Image_name extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  VMPointer<Draw_Image> _src;
  VMPointer<VMString> _name;
  VMWord _in;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _src = new VMPointer<Draw_Image>.field(this);
    _name = new VMPointer<VMString>.field(this);
    _in = field(VMWord);
  }
}
class F_Display_namedimage extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Draw_Image>> _ret;
  Array<VMByte> _temps;
  VMPointer<Draw_Display> _d;
  VMPointer<VMString> _name;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Draw_Image>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _d = new VMPointer<Draw_Display>.field(this);
    _name = new VMPointer<VMString>.field(this);
  }
}
class F_Display_newimage extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Draw_Image>> _ret;
  Array<VMByte> _temps;
  VMPointer<Draw_Display> _d;
  Draw_Rect _r;
  Draw_Chans _chans;
  VMWord _repl;
  VMWord _color;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Draw_Image>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _d = new VMPointer<Draw_Display>.field(this);
    _r = field(Draw_Rect);
    _chans = field(Draw_Chans);
    _repl = field(VMWord);
    _color = field(VMWord);
  }
}
class F_Screen_newwindow extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Draw_Image>> _ret;
  Array<VMByte> _temps;
  VMPointer<Draw_Screen> _screen;
  Draw_Rect _r;
  VMWord _backing;
  VMWord _color;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Draw_Image>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _screen = new VMPointer<Draw_Screen>.field(this);
    _r = field(Draw_Rect);
    _backing = field(VMWord);
    _color = field(VMWord);
  }
}
class F_Display_open extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Draw_Image>> _ret;
  Array<VMByte> _temps;
  VMPointer<Draw_Display> _d;
  VMPointer<VMString> _name;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Draw_Image>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _d = new VMPointer<Draw_Display>.field(this);
    _name = new VMPointer<VMString>.field(this);
  }
}
class F_Font_open extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Draw_Font>> _ret;
  Array<VMByte> _temps;
  VMPointer<Draw_Display> _d;
  VMPointer<VMString> _name;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Draw_Font>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _d = new VMPointer<Draw_Display>.field(this);
    _name = new VMPointer<VMString>.field(this);
  }
}
class F_Image_origin extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  VMPointer<Draw_Image> _win;
  Draw_Point _log;
  Draw_Point _scr;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _win = new VMPointer<Draw_Image>.field(this);
    _log = field(Draw_Point);
    _scr = field(Draw_Point);
  }
}
class F_Image_poly extends VMStruct{
  Array<VMWord> _regs;
  VMWord _noret;
  Array<VMByte> _temps;
  VMPointer<Draw_Image> _dst;
  VMPointer<VMArray> _p;
  VMWord _end0;
  VMWord _end1;
  VMWord _radius;
  VMPointer<Draw_Image> _src;
  Draw_Point _sp;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _noret = field(VMWord);
    _temps = new Array<VMByte>(this,12);
    _dst = new VMPointer<Draw_Image>.field(this);
    _p = new VMPointer<VMArray>.field(this);
    _end0 = field(VMWord);
    _end1 = field(VMWord);
    _radius = field(VMWord);
    _src = new VMPointer<Draw_Image>.field(this);
    _sp = field(Draw_Point);
  }
}
class F_Image_polyop extends VMStruct{
  Array<VMWord> _regs;
  VMWord _noret;
  Array<VMByte> _temps;
  VMPointer<Draw_Image> _dst;
  VMPointer<VMArray> _p;
  VMWord _end0;
  VMWord _end1;
  VMWord _radius;
  VMPointer<Draw_Image> _src;
  Draw_Point _sp;
  VMWord _op;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _noret = field(VMWord);
    _temps = new Array<VMByte>(this,12);
    _dst = new VMPointer<Draw_Image>.field(this);
    _p = new VMPointer<VMArray>.field(this);
    _end0 = field(VMWord);
    _end1 = field(VMWord);
    _radius = field(VMWord);
    _src = new VMPointer<Draw_Image>.field(this);
    _sp = field(Draw_Point);
    _op = field(VMWord);
  }
}
class F_Display_publicscreen extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Draw_Screen>> _ret;
  Array<VMByte> _temps;
  VMPointer<Draw_Display> _d;
  VMWord _id;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Draw_Screen>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _d = new VMPointer<Draw_Display>.field(this);
    _id = field(VMWord);
  }
}
class F_Display_readimage extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Draw_Image>> _ret;
  Array<VMByte> _temps;
  VMPointer<Draw_Display> _d;
  VMPointer<Sys_FD> _fd;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Draw_Image>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _d = new VMPointer<Draw_Display>.field(this);
    _fd = new VMPointer<Sys_FD>.field(this);
  }
}
class F_Image_readpixels extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  VMPointer<Draw_Image> _src;
  Draw_Rect _r;
  VMPointer<VMArray> _data;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _src = new VMPointer<Draw_Image>.field(this);
    _r = field(Draw_Rect);
    _data = new VMPointer<VMArray>.field(this);
  }
}
class F_Display_rgb extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Draw_Image>> _ret;
  Array<VMByte> _temps;
  VMPointer<Draw_Display> _d;
  VMWord _r;
  VMWord _g;
  VMWord _b;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Draw_Image>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _d = new VMPointer<Draw_Display>.field(this);
    _r = field(VMWord);
    _g = field(VMWord);
    _b = field(VMWord);
  }
}
class F_Display_rgb2cmap extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  VMPointer<Draw_Display> _d;
  VMWord _r;
  VMWord _g;
  VMWord _b;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _d = new VMPointer<Draw_Display>.field(this);
    _r = field(VMWord);
    _g = field(VMWord);
    _b = field(VMWord);
  }
}
class F_Draw_setalpha extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  VMWord _c;
  VMWord _a;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _c = field(VMWord);
    _a = field(VMWord);
  }
}
class F_Rect_size extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<Draw_Point> _ret;
  Array<VMByte> _temps;
  Draw_Rect _r;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<Draw_Point>.field(this);
    _temps = new Array<VMByte>(this,12);
    _r = field(Draw_Rect);
  }
}
class F_Display_startrefresh extends VMStruct{
  Array<VMWord> _regs;
  VMWord _noret;
  Array<VMByte> _temps;
  VMPointer<Draw_Display> _d;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _noret = field(VMWord);
    _temps = new Array<VMByte>(this,12);
    _d = new VMPointer<Draw_Display>.field(this);
  }
}
class F_Point_sub extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<Draw_Point> _ret;
  Array<VMByte> _temps;
  Draw_Point _p;
  Draw_Point _q;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<Draw_Point>.field(this);
    _temps = new Array<VMByte>(this,12);
    _p = field(Draw_Point);
    _q = field(Draw_Point);
  }
}
class F_Rect_subpt extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<Draw_Rect> _ret;
  Array<VMByte> _temps;
  Draw_Rect _r;
  Draw_Point _p;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<Draw_Rect>.field(this);
    _temps = new Array<VMByte>(this,12);
    _r = field(Draw_Rect);
    _p = field(Draw_Point);
  }
}
class F_Chans_text extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<VMString>> _ret;
  Array<VMByte> _temps;
  Draw_Chans _c;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<VMString>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _c = field(Draw_Chans);
  }
}
class F_Image_text extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<Draw_Point> _ret;
  Array<VMByte> _temps;
  VMPointer<Draw_Image> _dst;
  Draw_Point _p;
  VMPointer<Draw_Image> _src;
  Draw_Point _sp;
  VMPointer<Draw_Font> _font;
  VMPointer<VMString> _str;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<Draw_Point>.field(this);
    _temps = new Array<VMByte>(this,12);
    _dst = new VMPointer<Draw_Image>.field(this);
    _p = field(Draw_Point);
    _src = new VMPointer<Draw_Image>.field(this);
    _sp = field(Draw_Point);
    _font = new VMPointer<Draw_Font>.field(this);
    _str = new VMPointer<VMString>.field(this);
  }
}
class F_Image_textbg extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<Draw_Point> _ret;
  Array<VMByte> _temps;
  VMPointer<Draw_Image> _dst;
  Draw_Point _p;
  VMPointer<Draw_Image> _src;
  Draw_Point _sp;
  VMPointer<Draw_Font> _font;
  VMPointer<VMString> _str;
  VMPointer<Draw_Image> _bg;
  Draw_Point _bgp;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<Draw_Point>.field(this);
    _temps = new Array<VMByte>(this,12);
    _dst = new VMPointer<Draw_Image>.field(this);
    _p = field(Draw_Point);
    _src = new VMPointer<Draw_Image>.field(this);
    _sp = field(Draw_Point);
    _font = new VMPointer<Draw_Font>.field(this);
    _str = new VMPointer<VMString>.field(this);
    _bg = new VMPointer<Draw_Image>.field(this);
    _bgp = field(Draw_Point);
  }
}
class F_Image_textbgop extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<Draw_Point> _ret;
  Array<VMByte> _temps;
  VMPointer<Draw_Image> _dst;
  Draw_Point _p;
  VMPointer<Draw_Image> _src;
  Draw_Point _sp;
  VMPointer<Draw_Font> _font;
  VMPointer<VMString> _str;
  VMPointer<Draw_Image> _bg;
  Draw_Point _bgp;
  VMWord _op;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<Draw_Point>.field(this);
    _temps = new Array<VMByte>(this,12);
    _dst = new VMPointer<Draw_Image>.field(this);
    _p = field(Draw_Point);
    _src = new VMPointer<Draw_Image>.field(this);
    _sp = field(Draw_Point);
    _font = new VMPointer<Draw_Font>.field(this);
    _str = new VMPointer<VMString>.field(this);
    _bg = new VMPointer<Draw_Image>.field(this);
    _bgp = field(Draw_Point);
    _op = field(VMWord);
  }
}
class F_Image_textop extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<Draw_Point> _ret;
  Array<VMByte> _temps;
  VMPointer<Draw_Image> _dst;
  Draw_Point _p;
  VMPointer<Draw_Image> _src;
  Draw_Point _sp;
  VMPointer<Draw_Font> _font;
  VMPointer<VMString> _str;
  VMWord _op;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<Draw_Point>.field(this);
    _temps = new Array<VMByte>(this,12);
    _dst = new VMPointer<Draw_Image>.field(this);
    _p = field(Draw_Point);
    _src = new VMPointer<Draw_Image>.field(this);
    _sp = field(Draw_Point);
    _font = new VMPointer<Draw_Font>.field(this);
    _str = new VMPointer<VMString>.field(this);
    _op = field(VMWord);
  }
}
class F_Image_top extends VMStruct{
  Array<VMWord> _regs;
  VMWord _noret;
  Array<VMByte> _temps;
  VMPointer<Draw_Image> _win;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _noret = field(VMWord);
    _temps = new Array<VMByte>(this,12);
    _win = new VMPointer<Draw_Image>.field(this);
  }
}
class F_Screen_top extends VMStruct{
  Array<VMWord> _regs;
  VMWord _noret;
  Array<VMByte> _temps;
  VMPointer<Draw_Screen> _screen;
  VMPointer<VMArray> _wins;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _noret = field(VMWord);
    _temps = new Array<VMByte>(this,12);
    _screen = new VMPointer<Draw_Screen>.field(this);
    _wins = new VMPointer<VMArray>.field(this);
  }
}
class F_Font_width extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  VMPointer<Draw_Font> _f;
  VMPointer<VMString> _str;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _f = new VMPointer<Draw_Font>.field(this);
    _str = new VMPointer<VMString>.field(this);
  }
}
class F_Display_writeimage extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  VMPointer<Draw_Display> _d;
  VMPointer<Sys_FD> _fd;
  VMPointer<Draw_Image> _i;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _d = new VMPointer<Draw_Display>.field(this);
    _fd = new VMPointer<Sys_FD>.field(this);
    _i = new VMPointer<Draw_Image>.field(this);
  }
}
class F_Image_writepixels extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  VMPointer<Draw_Image> _dst;
  Draw_Rect _r;
  VMPointer<VMArray> _data;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _dst = new VMPointer<Draw_Image>.field(this);
    _r = field(Draw_Rect);
    _data = new VMPointer<VMArray>.field(this);
  }
}
var Draw_PATH = r'$Draw';
var Draw_Opaque = -1;
var Draw_Transparent = 0;
var Draw_Black = 255;
var Draw_White = -1;
var Draw_Red = -16776961;
var Draw_Green = 16711935;
var Draw_Blue = 65535;
var Draw_Cyan = 16777215;
var Draw_Magenta = -16711681;
var Draw_Yellow = -65281;
var Draw_Grey = -286331137;
var Draw_Paleyellow = -21761;
var Draw_Darkyellow = -286351617;
var Draw_Darkgreen = 1149781247;
var Draw_Palegreen = -1426085121;
var Draw_Medgreen = -1999861505;
var Draw_Darkblue = 22015;
var Draw_Palebluegreen = -1426063361;
var Draw_Paleblue = 48127;
var Draw_Bluegreen = 8947967;
var Draw_Greygreen = 1437248255;
var Draw_Palegreygreen = -1628508417;
var Draw_Yellowgreen = -1718006529;
var Draw_Medblue = 39423;
var Draw_Greyblue = 6142975;
var Draw_Palegreyblue = 1234427391;
var Draw_Purpleblue = -2004300545;
var Draw_Notacolor = -256;
var Draw_Nofill = -256;
var Draw_Endsquare = 0;
var Draw_Enddisc = 1;
var Draw_Endarrow = 2;
var Draw_Flushoff = 0;
var Draw_Flushon = 1;
var Draw_Flushnow = 2;
var Draw_Refbackup = 0;
var Draw_Refnone = 1;
var Draw_SinD = 8;
var Draw_DinS = 4;
var Draw_SoutD = 2;
var Draw_DoutS = 1;
var Draw_S = 10;
var Draw_SoverD = 11;
var Draw_SatopD = 9;
var Draw_SxorD = 3;
var Draw_D = 5;
var Draw_DoverS = 7;
var Draw_DatopS = 6;
var Draw_DxorS = 3;
var Draw_Clear = 0;
var Draw_CRed = 0;
var Draw_CGreen = 1;
var Draw_CBlue = 2;
var Draw_CGrey = 3;
var Draw_CAlpha = 4;
var Draw_CMap = 5;
var Draw_CIgnore = 6;
class F_Element_adjust extends VMStruct{
  Array<VMWord> _regs;
  VMWord _noret;
  Array<VMByte> _temps;
  VMPointer<Prefab_Element> _elem;
  VMWord _equal;
  VMWord _dir;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _noret = field(VMWord);
    _temps = new Array<VMByte>(this,12);
    _elem = new VMPointer<Prefab_Element>.field(this);
    _equal = field(VMWord);
    _dir = field(VMWord);
  }
}
class F_Element_append extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  VMPointer<Prefab_Element> _elist;
  VMPointer<Prefab_Element> _elem;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _elist = new VMPointer<Prefab_Element>.field(this);
    _elem = new VMPointer<Prefab_Element>.field(this);
  }
}
class F_Compound_box extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Prefab_Compound>> _ret;
  Array<VMByte> _temps;
  VMPointer<Prefab_Environ> _env;
  Draw_Point _p;
  VMPointer<Prefab_Element> _title;
  VMPointer<Prefab_Element> _elist;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Prefab_Compound>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _env = new VMPointer<Prefab_Environ>.field(this);
    _p = field(Draw_Point);
    _title = new VMPointer<Prefab_Element>.field(this);
    _elist = new VMPointer<Prefab_Element>.field(this);
  }
}
class F_Element_clip extends VMStruct{
  Array<VMWord> _regs;
  VMWord _noret;
  Array<VMByte> _temps;
  VMPointer<Prefab_Element> _elem;
  Draw_Rect _r;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _noret = field(VMWord);
    _temps = new Array<VMByte>(this,12);
    _elem = new VMPointer<Prefab_Element>.field(this);
    _r = field(Draw_Rect);
  }
}
class F_Compound_draw extends VMStruct{
  Array<VMWord> _regs;
  VMWord _noret;
  Array<VMByte> _temps;
  VMPointer<Prefab_Compound> _comp;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _noret = field(VMWord);
    _temps = new Array<VMByte>(this,12);
    _comp = new VMPointer<Prefab_Compound>.field(this);
  }
}
class F_Element_elist extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Prefab_Element>> _ret;
  Array<VMByte> _temps;
  VMPointer<Prefab_Environ> _env;
  VMPointer<Prefab_Element> _elem;
  VMWord _kind;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Prefab_Element>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _env = new VMPointer<Prefab_Environ>.field(this);
    _elem = new VMPointer<Prefab_Element>.field(this);
    _kind = field(VMWord);
  }
}
class F_Compound_highlight extends VMStruct{
  Array<VMWord> _regs;
  VMWord _noret;
  Array<VMByte> _temps;
  VMPointer<Prefab_Compound> _comp;
  VMPointer<Prefab_Element> _elem;
  VMWord _on;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _noret = field(VMWord);
    _temps = new Array<VMByte>(this,12);
    _comp = new VMPointer<Prefab_Compound>.field(this);
    _elem = new VMPointer<Prefab_Element>.field(this);
    _on = field(VMWord);
  }
}
class F_Element_icon extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Prefab_Element>> _ret;
  Array<VMByte> _temps;
  VMPointer<Prefab_Environ> _env;
  Draw_Rect _r;
  VMPointer<Draw_Image> _icon;
  VMPointer<Draw_Image> _mask;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Prefab_Element>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _env = new VMPointer<Prefab_Environ>.field(this);
    _r = field(Draw_Rect);
    _icon = new VMPointer<Draw_Image>.field(this);
    _mask = new VMPointer<Draw_Image>.field(this);
  }
}
class F_Compound_iconbox extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Prefab_Compound>> _ret;
  Array<VMByte> _temps;
  VMPointer<Prefab_Environ> _env;
  Draw_Point _p;
  VMPointer<VMString> _title;
  VMPointer<Draw_Image> _icon;
  VMPointer<Draw_Image> _mask;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Prefab_Compound>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _env = new VMPointer<Prefab_Environ>.field(this);
    _p = field(Draw_Point);
    _title = new VMPointer<VMString>.field(this);
    _icon = new VMPointer<Draw_Image>.field(this);
    _mask = new VMPointer<Draw_Image>.field(this);
  }
}
class F_Element_layout extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Prefab_Element>> _ret;
  Array<VMByte> _temps;
  VMPointer<Prefab_Environ> _env;
  VMPointer<VMList> _lay;
  Draw_Rect _r;
  VMWord _kind;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Prefab_Element>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _env = new VMPointer<Prefab_Environ>.field(this);
    _lay = new VMPointer<VMList>.field(this);
    _r = field(Draw_Rect);
    _kind = field(VMWord);
  }
}
class F_Compound_layoutbox extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Prefab_Compound>> _ret;
  Array<VMByte> _temps;
  VMPointer<Prefab_Environ> _env;
  Draw_Rect _r;
  VMPointer<VMString> _title;
  VMPointer<VMList> _lay;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Prefab_Compound>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _env = new VMPointer<Prefab_Environ>.field(this);
    _r = field(Draw_Rect);
    _title = new VMPointer<VMString>.field(this);
    _lay = new VMPointer<VMList>.field(this);
  }
}
class F_Compound_redraw extends VMStruct{
  Array<VMWord> _regs;
  VMWord _noret;
  Array<VMByte> _temps;
  VMPointer<Prefab_Compound> _comp;
  Draw_Rect _r;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _noret = field(VMWord);
    _temps = new Array<VMByte>(this,12);
    _comp = new VMPointer<Prefab_Compound>.field(this);
    _r = field(Draw_Rect);
  }
}
class F_Element_scroll extends VMStruct{
  Array<VMWord> _regs;
  VMWord _noret;
  Array<VMByte> _temps;
  VMPointer<Prefab_Element> _elem;
  Draw_Point _d;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _noret = field(VMWord);
    _temps = new Array<VMByte>(this,12);
    _elem = new VMPointer<Prefab_Element>.field(this);
    _d = field(Draw_Point);
  }
}
class F_Compound_scroll extends VMStruct{
  Array<VMWord> _regs;
  VMWord _noret;
  Array<VMByte> _temps;
  VMPointer<Prefab_Compound> _comp;
  VMPointer<Prefab_Element> _elem;
  Draw_Point _d;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _noret = field(VMWord);
    _temps = new Array<VMByte>(this,12);
    _comp = new VMPointer<Prefab_Compound>.field(this);
    _elem = new VMPointer<Prefab_Element>.field(this);
    _d = field(Draw_Point);
  }
}
class F_Compound_select_ret extends VMStruct{
  VMWord _t0;
  VMWord _t1;
  VMPointer<Prefab_Element> _t2;
  fields(){
    _t0 = field(VMWord);
    _t1 = field(VMWord);
    _t2 = new VMPointer<Prefab_Element>.field(this);
  }
}class F_Compound_select extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<F_Compound_select_ret> _ret;
  Array<VMByte> _temps;
  VMPointer<Prefab_Compound> _comp;
  VMPointer<Prefab_Element> _elem;
  VMWord _i;
  VMPointer<VMChannel> _c;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<F_Compound_select_ret>.field(this);
    _temps = new Array<VMByte>(this,12);
    _comp = new VMPointer<Prefab_Compound>.field(this);
    _elem = new VMPointer<Prefab_Element>.field(this);
    _i = field(VMWord);
    _c = new VMPointer<VMChannel>.field(this);
  }
}
class F_Element_separator extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Prefab_Element>> _ret;
  Array<VMByte> _temps;
  VMPointer<Prefab_Environ> _env;
  Draw_Rect _r;
  VMPointer<Draw_Image> _icon;
  VMPointer<Draw_Image> _mask;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Prefab_Element>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _env = new VMPointer<Prefab_Environ>.field(this);
    _r = field(Draw_Rect);
    _icon = new VMPointer<Draw_Image>.field(this);
    _mask = new VMPointer<Draw_Image>.field(this);
  }
}
class F_Element_show extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  VMPointer<Prefab_Element> _elist;
  VMPointer<Prefab_Element> _elem;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _elist = new VMPointer<Prefab_Element>.field(this);
    _elem = new VMPointer<Prefab_Element>.field(this);
  }
}
class F_Compound_show extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  VMPointer<Prefab_Compound> _comp;
  VMPointer<Prefab_Element> _elem;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _comp = new VMPointer<Prefab_Compound>.field(this);
    _elem = new VMPointer<Prefab_Element>.field(this);
  }
}
class F_Compound_tagselect_ret extends VMStruct{
  VMWord _t0;
  VMWord _t1;
  VMPointer<Prefab_Element> _t2;
  fields(){
    _t0 = field(VMWord);
    _t1 = field(VMWord);
    _t2 = new VMPointer<Prefab_Element>.field(this);
  }
}class F_Compound_tagselect extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<F_Compound_tagselect_ret> _ret;
  Array<VMByte> _temps;
  VMPointer<Prefab_Compound> _comp;
  VMPointer<Prefab_Element> _elem;
  VMWord _i;
  VMPointer<VMChannel> _c;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<F_Compound_tagselect_ret>.field(this);
    _temps = new Array<VMByte>(this,12);
    _comp = new VMPointer<Prefab_Compound>.field(this);
    _elem = new VMPointer<Prefab_Element>.field(this);
    _i = field(VMWord);
    _c = new VMPointer<VMChannel>.field(this);
  }
}
class F_Element_text extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Prefab_Element>> _ret;
  Array<VMByte> _temps;
  VMPointer<Prefab_Environ> _env;
  VMPointer<VMString> _text;
  Draw_Rect _r;
  VMWord _kind;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Prefab_Element>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _env = new VMPointer<Prefab_Environ>.field(this);
    _text = new VMPointer<VMString>.field(this);
    _r = field(Draw_Rect);
    _kind = field(VMWord);
  }
}
class F_Compound_textbox extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Prefab_Compound>> _ret;
  Array<VMByte> _temps;
  VMPointer<Prefab_Environ> _env;
  Draw_Rect _r;
  VMPointer<VMString> _title;
  VMPointer<VMString> _text;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Prefab_Compound>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _env = new VMPointer<Prefab_Environ>.field(this);
    _r = field(Draw_Rect);
    _title = new VMPointer<VMString>.field(this);
    _text = new VMPointer<VMString>.field(this);
  }
}
class F_Element_translate extends VMStruct{
  Array<VMWord> _regs;
  VMWord _noret;
  Array<VMByte> _temps;
  VMPointer<Prefab_Element> _elem;
  Draw_Point _d;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _noret = field(VMWord);
    _temps = new Array<VMByte>(this,12);
    _elem = new VMPointer<Prefab_Element>.field(this);
    _d = field(Draw_Point);
  }
}
var Prefab_PATH = r'$Prefab';
var Prefab_EIcon = 0;
var Prefab_EText = 1;
var Prefab_ETitle = 2;
var Prefab_EHorizontal = 3;
var Prefab_EVertical = 4;
var Prefab_ESeparator = 5;
var Prefab_Adjpack = 10;
var Prefab_Adjequal = 11;
var Prefab_Adjfill = 12;
var Prefab_Adjleft = 20;
var Prefab_Adjup = 20;
var Prefab_Adjcenter = 21;
var Prefab_Adjright = 22;
var Prefab_Adjdown = 22;
class F_Tk_cmd extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<VMString>> _ret;
  Array<VMByte> _temps;
  VMPointer<Tk_Toplevel> _t;
  VMPointer<VMString> _arg;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<VMString>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _t = new VMPointer<Tk_Toplevel>.field(this);
    _arg = new VMPointer<VMString>.field(this);
  }
}
class F_Tk_color extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  VMPointer<VMString> _col;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _col = new VMPointer<VMString>.field(this);
  }
}
class F_Tk_getimage_ret extends VMStruct{
  VMPointer<Draw_Image> _t0;
  VMPointer<Draw_Image> _t1;
  VMPointer<VMString> _t2;
  fields(){
    _t0 = new VMPointer<Draw_Image>.field(this);
    _t1 = new VMPointer<Draw_Image>.field(this);
    _t2 = new VMPointer<VMString>.field(this);
  }
}class F_Tk_getimage extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<F_Tk_getimage_ret> _ret;
  Array<VMByte> _temps;
  VMPointer<Tk_Toplevel> _t;
  VMPointer<VMString> _name;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<F_Tk_getimage_ret>.field(this);
    _temps = new Array<VMByte>(this,12);
    _t = new VMPointer<Tk_Toplevel>.field(this);
    _name = new VMPointer<VMString>.field(this);
  }
}
class F_Tk_keyboard extends VMStruct{
  Array<VMWord> _regs;
  VMWord _noret;
  Array<VMByte> _temps;
  VMPointer<Tk_Toplevel> _t;
  VMWord _key;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _noret = field(VMWord);
    _temps = new Array<VMByte>(this,12);
    _t = new VMPointer<Tk_Toplevel>.field(this);
    _key = field(VMWord);
  }
}
class F_Tk_namechan extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<VMString>> _ret;
  Array<VMByte> _temps;
  VMPointer<Tk_Toplevel> _t;
  VMPointer<VMChannel> _c;
  VMPointer<VMString> _n;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<VMString>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _t = new VMPointer<Tk_Toplevel>.field(this);
    _c = new VMPointer<VMChannel>.field(this);
    _n = new VMPointer<VMString>.field(this);
  }
}
class F_Tk_pointer extends VMStruct{
  Array<VMWord> _regs;
  VMWord _noret;
  Array<VMByte> _temps;
  VMPointer<Tk_Toplevel> _t;
  Draw_Pointer _p;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _noret = field(VMWord);
    _temps = new Array<VMByte>(this,12);
    _t = new VMPointer<Tk_Toplevel>.field(this);
    _p = field(Draw_Pointer);
  }
}
class F_Tk_putimage extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<VMString>> _ret;
  Array<VMByte> _temps;
  VMPointer<Tk_Toplevel> _t;
  VMPointer<VMString> _name;
  VMPointer<Draw_Image> _i;
  VMPointer<Draw_Image> _m;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<VMString>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _t = new VMPointer<Tk_Toplevel>.field(this);
    _name = new VMPointer<VMString>.field(this);
    _i = new VMPointer<Draw_Image>.field(this);
    _m = new VMPointer<Draw_Image>.field(this);
  }
}
class F_Tk_quote extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<VMString>> _ret;
  Array<VMByte> _temps;
  VMPointer<VMString> _s;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<VMString>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _s = new VMPointer<VMString>.field(this);
  }
}
class F_Tk_rect extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<Draw_Rect> _ret;
  Array<VMByte> _temps;
  VMPointer<Tk_Toplevel> _t;
  VMPointer<VMString> _name;
  VMWord _flags;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<Draw_Rect>.field(this);
    _temps = new Array<VMByte>(this,12);
    _t = new VMPointer<Tk_Toplevel>.field(this);
    _name = new VMPointer<VMString>.field(this);
    _flags = field(VMWord);
  }
}
class F_Tk_toplevel extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Tk_Toplevel>> _ret;
  Array<VMByte> _temps;
  VMPointer<Draw_Display> _d;
  VMPointer<VMString> _arg;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Tk_Toplevel>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _d = new VMPointer<Draw_Display>.field(this);
    _arg = new VMPointer<VMString>.field(this);
  }
}
var Tk_PATH = r'$Tk';
var Tk_Border = 1;
var Tk_Required = 2;
var Tk_Local = 4;
class F_Math_FPcontrol extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  VMWord _r;
  VMWord _mask;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _r = field(VMWord);
    _mask = field(VMWord);
  }
}
class F_Math_FPstatus extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  VMWord _r;
  VMWord _mask;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _r = field(VMWord);
    _mask = field(VMWord);
  }
}
class F_Math_acos extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMDouble> _ret;
  Array<VMByte> _temps;
  VMDouble _x;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMDouble>.field(this);
    _temps = new Array<VMByte>(this,12);
    _x = field(VMDouble);
  }
}
class F_Math_acosh extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMDouble> _ret;
  Array<VMByte> _temps;
  VMDouble _x;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMDouble>.field(this);
    _temps = new Array<VMByte>(this,12);
    _x = field(VMDouble);
  }
}
class F_Math_asin extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMDouble> _ret;
  Array<VMByte> _temps;
  VMDouble _x;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMDouble>.field(this);
    _temps = new Array<VMByte>(this,12);
    _x = field(VMDouble);
  }
}
class F_Math_asinh extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMDouble> _ret;
  Array<VMByte> _temps;
  VMDouble _x;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMDouble>.field(this);
    _temps = new Array<VMByte>(this,12);
    _x = field(VMDouble);
  }
}
class F_Math_atan extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMDouble> _ret;
  Array<VMByte> _temps;
  VMDouble _x;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMDouble>.field(this);
    _temps = new Array<VMByte>(this,12);
    _x = field(VMDouble);
  }
}
class F_Math_atan2 extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMDouble> _ret;
  Array<VMByte> _temps;
  VMDouble _y;
  VMDouble _x;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMDouble>.field(this);
    _temps = new Array<VMByte>(this,12);
    _y = field(VMDouble);
    _x = field(VMDouble);
  }
}
class F_Math_atanh extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMDouble> _ret;
  Array<VMByte> _temps;
  VMDouble _x;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMDouble>.field(this);
    _temps = new Array<VMByte>(this,12);
    _x = field(VMDouble);
  }
}
class F_Math_bits32real extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMDouble> _ret;
  Array<VMByte> _temps;
  VMWord _b;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMDouble>.field(this);
    _temps = new Array<VMByte>(this,12);
    _b = field(VMWord);
  }
}
class F_Math_bits64real extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMDouble> _ret;
  Array<VMByte> _temps;
  VMLong _b;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMDouble>.field(this);
    _temps = new Array<VMByte>(this,12);
    _b = field(VMLong);
  }
}
class F_Math_cbrt extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMDouble> _ret;
  Array<VMByte> _temps;
  VMDouble _x;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMDouble>.field(this);
    _temps = new Array<VMByte>(this,12);
    _x = field(VMDouble);
  }
}
class F_Math_ceil extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMDouble> _ret;
  Array<VMByte> _temps;
  VMDouble _x;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMDouble>.field(this);
    _temps = new Array<VMByte>(this,12);
    _x = field(VMDouble);
  }
}
class F_Math_copysign extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMDouble> _ret;
  Array<VMByte> _temps;
  VMDouble _x;
  VMDouble _s;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMDouble>.field(this);
    _temps = new Array<VMByte>(this,12);
    _x = field(VMDouble);
    _s = field(VMDouble);
  }
}
class F_Math_cos extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMDouble> _ret;
  Array<VMByte> _temps;
  VMDouble _x;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMDouble>.field(this);
    _temps = new Array<VMByte>(this,12);
    _x = field(VMDouble);
  }
}
class F_Math_cosh extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMDouble> _ret;
  Array<VMByte> _temps;
  VMDouble _x;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMDouble>.field(this);
    _temps = new Array<VMByte>(this,12);
    _x = field(VMDouble);
  }
}
class F_Math_dot extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMDouble> _ret;
  Array<VMByte> _temps;
  VMPointer<VMArray> _x;
  VMPointer<VMArray> _y;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMDouble>.field(this);
    _temps = new Array<VMByte>(this,12);
    _x = new VMPointer<VMArray>.field(this);
    _y = new VMPointer<VMArray>.field(this);
  }
}
class F_Math_erf extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMDouble> _ret;
  Array<VMByte> _temps;
  VMDouble _x;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMDouble>.field(this);
    _temps = new Array<VMByte>(this,12);
    _x = field(VMDouble);
  }
}
class F_Math_erfc extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMDouble> _ret;
  Array<VMByte> _temps;
  VMDouble _x;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMDouble>.field(this);
    _temps = new Array<VMByte>(this,12);
    _x = field(VMDouble);
  }
}
class F_Math_exp extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMDouble> _ret;
  Array<VMByte> _temps;
  VMDouble _x;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMDouble>.field(this);
    _temps = new Array<VMByte>(this,12);
    _x = field(VMDouble);
  }
}
class F_Math_expm1 extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMDouble> _ret;
  Array<VMByte> _temps;
  VMDouble _x;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMDouble>.field(this);
    _temps = new Array<VMByte>(this,12);
    _x = field(VMDouble);
  }
}
class F_Math_export_int extends VMStruct{
  Array<VMWord> _regs;
  VMWord _noret;
  Array<VMByte> _temps;
  VMPointer<VMArray> _b;
  VMPointer<VMArray> _x;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _noret = field(VMWord);
    _temps = new Array<VMByte>(this,12);
    _b = new VMPointer<VMArray>.field(this);
    _x = new VMPointer<VMArray>.field(this);
  }
}
class F_Math_export_real extends VMStruct{
  Array<VMWord> _regs;
  VMWord _noret;
  Array<VMByte> _temps;
  VMPointer<VMArray> _b;
  VMPointer<VMArray> _x;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _noret = field(VMWord);
    _temps = new Array<VMByte>(this,12);
    _b = new VMPointer<VMArray>.field(this);
    _x = new VMPointer<VMArray>.field(this);
  }
}
class F_Math_export_real32 extends VMStruct{
  Array<VMWord> _regs;
  VMWord _noret;
  Array<VMByte> _temps;
  VMPointer<VMArray> _b;
  VMPointer<VMArray> _x;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _noret = field(VMWord);
    _temps = new Array<VMByte>(this,12);
    _b = new VMPointer<VMArray>.field(this);
    _x = new VMPointer<VMArray>.field(this);
  }
}
class F_Math_fabs extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMDouble> _ret;
  Array<VMByte> _temps;
  VMDouble _x;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMDouble>.field(this);
    _temps = new Array<VMByte>(this,12);
    _x = field(VMDouble);
  }
}
class F_Math_fdim extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMDouble> _ret;
  Array<VMByte> _temps;
  VMDouble _x;
  VMDouble _y;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMDouble>.field(this);
    _temps = new Array<VMByte>(this,12);
    _x = field(VMDouble);
    _y = field(VMDouble);
  }
}
class F_Math_finite extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  VMDouble _x;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _x = field(VMDouble);
  }
}
class F_Math_floor extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMDouble> _ret;
  Array<VMByte> _temps;
  VMDouble _x;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMDouble>.field(this);
    _temps = new Array<VMByte>(this,12);
    _x = field(VMDouble);
  }
}
class F_Math_fmax extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMDouble> _ret;
  Array<VMByte> _temps;
  VMDouble _x;
  VMDouble _y;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMDouble>.field(this);
    _temps = new Array<VMByte>(this,12);
    _x = field(VMDouble);
    _y = field(VMDouble);
  }
}
class F_Math_fmin extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMDouble> _ret;
  Array<VMByte> _temps;
  VMDouble _x;
  VMDouble _y;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMDouble>.field(this);
    _temps = new Array<VMByte>(this,12);
    _x = field(VMDouble);
    _y = field(VMDouble);
  }
}
class F_Math_fmod extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMDouble> _ret;
  Array<VMByte> _temps;
  VMDouble _x;
  VMDouble _y;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMDouble>.field(this);
    _temps = new Array<VMByte>(this,12);
    _x = field(VMDouble);
    _y = field(VMDouble);
  }
}
class F_Math_gemm extends VMStruct{
  Array<VMWord> _regs;
  VMWord _noret;
  Array<VMByte> _temps;
  VMWord _transa;
  VMWord _transb;
  VMWord _m;
  VMWord _n;
  VMWord _k;
  Array<VMByte> __pad52;
  VMDouble _alpha;
  VMPointer<VMArray> _a;
  VMWord _lda;
  VMPointer<VMArray> _b;
  VMWord _ldb;
  VMDouble _beta;
  VMPointer<VMArray> _c;
  VMWord _ldc;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _noret = field(VMWord);
    _temps = new Array<VMByte>(this,12);
    _transa = field(VMWord);
    _transb = field(VMWord);
    _m = field(VMWord);
    _n = field(VMWord);
    _k = field(VMWord);
    __pad52 = new Array<VMByte>(this,4);
    _alpha = field(VMDouble);
    _a = new VMPointer<VMArray>.field(this);
    _lda = field(VMWord);
    _b = new VMPointer<VMArray>.field(this);
    _ldb = field(VMWord);
    _beta = field(VMDouble);
    _c = new VMPointer<VMArray>.field(this);
    _ldc = field(VMWord);
  }
}
class F_Math_getFPcontrol extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
  }
}
class F_Math_getFPstatus extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
  }
}
class F_Math_hypot extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMDouble> _ret;
  Array<VMByte> _temps;
  VMDouble _x;
  VMDouble _y;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMDouble>.field(this);
    _temps = new Array<VMByte>(this,12);
    _x = field(VMDouble);
    _y = field(VMDouble);
  }
}
class F_Math_iamax extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  VMPointer<VMArray> _x;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _x = new VMPointer<VMArray>.field(this);
  }
}
class F_Math_ilogb extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  VMDouble _x;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _x = field(VMDouble);
  }
}
class F_Math_import_int extends VMStruct{
  Array<VMWord> _regs;
  VMWord _noret;
  Array<VMByte> _temps;
  VMPointer<VMArray> _b;
  VMPointer<VMArray> _x;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _noret = field(VMWord);
    _temps = new Array<VMByte>(this,12);
    _b = new VMPointer<VMArray>.field(this);
    _x = new VMPointer<VMArray>.field(this);
  }
}
class F_Math_import_real extends VMStruct{
  Array<VMWord> _regs;
  VMWord _noret;
  Array<VMByte> _temps;
  VMPointer<VMArray> _b;
  VMPointer<VMArray> _x;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _noret = field(VMWord);
    _temps = new Array<VMByte>(this,12);
    _b = new VMPointer<VMArray>.field(this);
    _x = new VMPointer<VMArray>.field(this);
  }
}
class F_Math_import_real32 extends VMStruct{
  Array<VMWord> _regs;
  VMWord _noret;
  Array<VMByte> _temps;
  VMPointer<VMArray> _b;
  VMPointer<VMArray> _x;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _noret = field(VMWord);
    _temps = new Array<VMByte>(this,12);
    _b = new VMPointer<VMArray>.field(this);
    _x = new VMPointer<VMArray>.field(this);
  }
}
class F_Math_isnan extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  VMDouble _x;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _x = field(VMDouble);
  }
}
class F_Math_j0 extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMDouble> _ret;
  Array<VMByte> _temps;
  VMDouble _x;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMDouble>.field(this);
    _temps = new Array<VMByte>(this,12);
    _x = field(VMDouble);
  }
}
class F_Math_j1 extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMDouble> _ret;
  Array<VMByte> _temps;
  VMDouble _x;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMDouble>.field(this);
    _temps = new Array<VMByte>(this,12);
    _x = field(VMDouble);
  }
}
class F_Math_jn extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMDouble> _ret;
  Array<VMByte> _temps;
  VMWord _n;
  Array<VMByte> __pad36;
  VMDouble _x;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMDouble>.field(this);
    _temps = new Array<VMByte>(this,12);
    _n = field(VMWord);
    __pad36 = new Array<VMByte>(this,4);
    _x = field(VMDouble);
  }
}
class F_Math_lgamma_ret extends VMStruct{
  VMWord _t0;
  Array<VMByte> __pad4;
  VMDouble _t1;
  fields(){
    _t0 = field(VMWord);
    __pad4 = new Array<VMByte>(this,4);
    _t1 = field(VMDouble);
  }
}class F_Math_lgamma extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<F_Math_lgamma_ret> _ret;
  Array<VMByte> _temps;
  VMDouble _x;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<F_Math_lgamma_ret>.field(this);
    _temps = new Array<VMByte>(this,12);
    _x = field(VMDouble);
  }
}
class F_Math_log extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMDouble> _ret;
  Array<VMByte> _temps;
  VMDouble _x;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMDouble>.field(this);
    _temps = new Array<VMByte>(this,12);
    _x = field(VMDouble);
  }
}
class F_Math_log10 extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMDouble> _ret;
  Array<VMByte> _temps;
  VMDouble _x;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMDouble>.field(this);
    _temps = new Array<VMByte>(this,12);
    _x = field(VMDouble);
  }
}
class F_Math_log1p extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMDouble> _ret;
  Array<VMByte> _temps;
  VMDouble _x;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMDouble>.field(this);
    _temps = new Array<VMByte>(this,12);
    _x = field(VMDouble);
  }
}
class F_Math_modf_ret extends VMStruct{
  VMWord _t0;
  Array<VMByte> __pad4;
  VMDouble _t1;
  fields(){
    _t0 = field(VMWord);
    __pad4 = new Array<VMByte>(this,4);
    _t1 = field(VMDouble);
  }
}class F_Math_modf extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<F_Math_modf_ret> _ret;
  Array<VMByte> _temps;
  VMDouble _x;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<F_Math_modf_ret>.field(this);
    _temps = new Array<VMByte>(this,12);
    _x = field(VMDouble);
  }
}
class F_Math_nextafter extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMDouble> _ret;
  Array<VMByte> _temps;
  VMDouble _x;
  VMDouble _y;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMDouble>.field(this);
    _temps = new Array<VMByte>(this,12);
    _x = field(VMDouble);
    _y = field(VMDouble);
  }
}
class F_Math_norm1 extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMDouble> _ret;
  Array<VMByte> _temps;
  VMPointer<VMArray> _x;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMDouble>.field(this);
    _temps = new Array<VMByte>(this,12);
    _x = new VMPointer<VMArray>.field(this);
  }
}
class F_Math_norm2 extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMDouble> _ret;
  Array<VMByte> _temps;
  VMPointer<VMArray> _x;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMDouble>.field(this);
    _temps = new Array<VMByte>(this,12);
    _x = new VMPointer<VMArray>.field(this);
  }
}
class F_Math_pow extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMDouble> _ret;
  Array<VMByte> _temps;
  VMDouble _x;
  VMDouble _y;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMDouble>.field(this);
    _temps = new Array<VMByte>(this,12);
    _x = field(VMDouble);
    _y = field(VMDouble);
  }
}
class F_Math_pow10 extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMDouble> _ret;
  Array<VMByte> _temps;
  VMWord _p;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMDouble>.field(this);
    _temps = new Array<VMByte>(this,12);
    _p = field(VMWord);
  }
}
class F_Math_realbits32 extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  VMDouble _x;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _x = field(VMDouble);
  }
}
class F_Math_realbits64 extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMLong> _ret;
  Array<VMByte> _temps;
  VMDouble _x;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMLong>.field(this);
    _temps = new Array<VMByte>(this,12);
    _x = field(VMDouble);
  }
}
class F_Math_remainder extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMDouble> _ret;
  Array<VMByte> _temps;
  VMDouble _x;
  VMDouble _p;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMDouble>.field(this);
    _temps = new Array<VMByte>(this,12);
    _x = field(VMDouble);
    _p = field(VMDouble);
  }
}
class F_Math_rint extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMDouble> _ret;
  Array<VMByte> _temps;
  VMDouble _x;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMDouble>.field(this);
    _temps = new Array<VMByte>(this,12);
    _x = field(VMDouble);
  }
}
class F_Math_scalbn extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMDouble> _ret;
  Array<VMByte> _temps;
  VMDouble _x;
  VMWord _n;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMDouble>.field(this);
    _temps = new Array<VMByte>(this,12);
    _x = field(VMDouble);
    _n = field(VMWord);
  }
}
class F_Math_sin extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMDouble> _ret;
  Array<VMByte> _temps;
  VMDouble _x;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMDouble>.field(this);
    _temps = new Array<VMByte>(this,12);
    _x = field(VMDouble);
  }
}
class F_Math_sinh extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMDouble> _ret;
  Array<VMByte> _temps;
  VMDouble _x;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMDouble>.field(this);
    _temps = new Array<VMByte>(this,12);
    _x = field(VMDouble);
  }
}
class F_Math_sort extends VMStruct{
  Array<VMWord> _regs;
  VMWord _noret;
  Array<VMByte> _temps;
  VMPointer<VMArray> _x;
  VMPointer<VMArray> _pi;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _noret = field(VMWord);
    _temps = new Array<VMByte>(this,12);
    _x = new VMPointer<VMArray>.field(this);
    _pi = new VMPointer<VMArray>.field(this);
  }
}
class F_Math_sqrt extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMDouble> _ret;
  Array<VMByte> _temps;
  VMDouble _x;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMDouble>.field(this);
    _temps = new Array<VMByte>(this,12);
    _x = field(VMDouble);
  }
}
class F_Math_tan extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMDouble> _ret;
  Array<VMByte> _temps;
  VMDouble _x;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMDouble>.field(this);
    _temps = new Array<VMByte>(this,12);
    _x = field(VMDouble);
  }
}
class F_Math_tanh extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMDouble> _ret;
  Array<VMByte> _temps;
  VMDouble _x;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMDouble>.field(this);
    _temps = new Array<VMByte>(this,12);
    _x = field(VMDouble);
  }
}
class F_Math_y0 extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMDouble> _ret;
  Array<VMByte> _temps;
  VMDouble _x;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMDouble>.field(this);
    _temps = new Array<VMByte>(this,12);
    _x = field(VMDouble);
  }
}
class F_Math_y1 extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMDouble> _ret;
  Array<VMByte> _temps;
  VMDouble _x;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMDouble>.field(this);
    _temps = new Array<VMByte>(this,12);
    _x = field(VMDouble);
  }
}
class F_Math_yn extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMDouble> _ret;
  Array<VMByte> _temps;
  VMWord _n;
  Array<VMByte> __pad36;
  VMDouble _x;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMDouble>.field(this);
    _temps = new Array<VMByte>(this,12);
    _n = field(VMWord);
    __pad36 = new Array<VMByte>(this,4);
    _x = field(VMDouble);
  }
}
var Math_PATH = r'$Math';
var Math_Infinity = double.INFINITY;
var Math_NaN = double.NAN;
var Math_MachEps = 2.220446049250313e-16;
var Math_Pi = 3.141592653589793;
var Math_Degree = .017453292519943295;
var Math_INVAL = 1;
var Math_ZDIV = 2;
var Math_OVFL = 4;
var Math_UNFL = 8;
var Math_INEX = 16;
var Math_RND_NR = 0;
var Math_RND_NINF = 256;
var Math_RND_PINF = 512;
var Math_RND_Z = 768;
var Math_RND_MASK = 768;
class F_IPint_add extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Keyring_IPint>> _ret;
  Array<VMByte> _temps;
  VMPointer<Keyring_IPint> _i1;
  VMPointer<Keyring_IPint> _i2;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Keyring_IPint>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _i1 = new VMPointer<Keyring_IPint>.field(this);
    _i2 = new VMPointer<Keyring_IPint>.field(this);
  }
}
class F_Keyring_aescbc extends VMStruct{
  Array<VMWord> _regs;
  VMWord _noret;
  Array<VMByte> _temps;
  VMPointer<Keyring_AESstate> _state;
  VMPointer<VMArray> _buf;
  VMWord _n;
  VMWord _direction;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _noret = field(VMWord);
    _temps = new Array<VMByte>(this,12);
    _state = new VMPointer<Keyring_AESstate>.field(this);
    _buf = new VMPointer<VMArray>.field(this);
    _n = field(VMWord);
    _direction = field(VMWord);
  }
}
class F_Keyring_aessetup extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Keyring_AESstate>> _ret;
  Array<VMByte> _temps;
  VMPointer<VMArray> _key;
  VMPointer<VMArray> _ivec;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Keyring_AESstate>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _key = new VMPointer<VMArray>.field(this);
    _ivec = new VMPointer<VMArray>.field(this);
  }
}
class F_IPint_and extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Keyring_IPint>> _ret;
  Array<VMByte> _temps;
  VMPointer<Keyring_IPint> _i1;
  VMPointer<Keyring_IPint> _i2;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Keyring_IPint>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _i1 = new VMPointer<Keyring_IPint>.field(this);
    _i2 = new VMPointer<Keyring_IPint>.field(this);
  }
}
class F_Keyring_auth_ret extends VMStruct{
  VMPointer<VMString> _t0;
  VMPointer<VMArray> _t1;
  fields(){
    _t0 = new VMPointer<VMString>.field(this);
    _t1 = new VMPointer<VMArray>.field(this);
  }
}class F_Keyring_auth extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<F_Keyring_auth_ret> _ret;
  Array<VMByte> _temps;
  VMPointer<Sys_FD> _fd;
  VMPointer<Keyring_Authinfo> _info;
  VMWord _setid;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<F_Keyring_auth_ret>.field(this);
    _temps = new Array<VMByte>(this,12);
    _fd = new VMPointer<Sys_FD>.field(this);
    _info = new VMPointer<Keyring_Authinfo>.field(this);
    _setid = field(VMWord);
  }
}
class F_IPint_b64toip extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Keyring_IPint>> _ret;
  Array<VMByte> _temps;
  VMPointer<VMString> _str;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Keyring_IPint>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _str = new VMPointer<VMString>.field(this);
  }
}
class F_IPint_bebytestoip extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Keyring_IPint>> _ret;
  Array<VMByte> _temps;
  VMPointer<VMArray> _mag;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Keyring_IPint>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _mag = new VMPointer<VMArray>.field(this);
  }
}
class F_IPint_bits extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  VMPointer<Keyring_IPint> _i;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _i = new VMPointer<Keyring_IPint>.field(this);
  }
}
class F_Keyring_blowfishcbc extends VMStruct{
  Array<VMWord> _regs;
  VMWord _noret;
  Array<VMByte> _temps;
  VMPointer<Keyring_BFstate> _state;
  VMPointer<VMArray> _buf;
  VMWord _n;
  VMWord _direction;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _noret = field(VMWord);
    _temps = new Array<VMByte>(this,12);
    _state = new VMPointer<Keyring_BFstate>.field(this);
    _buf = new VMPointer<VMArray>.field(this);
    _n = field(VMWord);
    _direction = field(VMWord);
  }
}
class F_Keyring_blowfishsetup extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Keyring_BFstate>> _ret;
  Array<VMByte> _temps;
  VMPointer<VMArray> _key;
  VMPointer<VMArray> _ivec;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Keyring_BFstate>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _key = new VMPointer<VMArray>.field(this);
    _ivec = new VMPointer<VMArray>.field(this);
  }
}
class F_IPint_bytestoip extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Keyring_IPint>> _ret;
  Array<VMByte> _temps;
  VMPointer<VMArray> _buf;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Keyring_IPint>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _buf = new VMPointer<VMArray>.field(this);
  }
}
class F_Keyring_certtoattr extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<VMString>> _ret;
  Array<VMByte> _temps;
  VMPointer<Keyring_Certificate> _c;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<VMString>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _c = new VMPointer<Keyring_Certificate>.field(this);
  }
}
class F_Keyring_certtostr extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<VMString>> _ret;
  Array<VMByte> _temps;
  VMPointer<Keyring_Certificate> _c;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<VMString>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _c = new VMPointer<Keyring_Certificate>.field(this);
  }
}
class F_IPint_cmp extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  VMPointer<Keyring_IPint> _i1;
  VMPointer<Keyring_IPint> _i2;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _i1 = new VMPointer<Keyring_IPint>.field(this);
    _i2 = new VMPointer<Keyring_IPint>.field(this);
  }
}
class F_IPint_copy extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Keyring_IPint>> _ret;
  Array<VMByte> _temps;
  VMPointer<Keyring_IPint> _i;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Keyring_IPint>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _i = new VMPointer<Keyring_IPint>.field(this);
  }
}
class F_DigestState_copy extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Keyring_DigestState>> _ret;
  Array<VMByte> _temps;
  VMPointer<Keyring_DigestState> _d;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Keyring_DigestState>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _d = new VMPointer<Keyring_DigestState>.field(this);
  }
}
class F_RSAsk_decrypt extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Keyring_IPint>> _ret;
  Array<VMByte> _temps;
  VMPointer<Keyring_RSAsk> _k;
  VMPointer<Keyring_IPint> _m;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Keyring_IPint>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _k = new VMPointer<Keyring_RSAsk>.field(this);
    _m = new VMPointer<Keyring_IPint>.field(this);
  }
}
class F_Keyring_descbc extends VMStruct{
  Array<VMWord> _regs;
  VMWord _noret;
  Array<VMByte> _temps;
  VMPointer<Keyring_DESstate> _state;
  VMPointer<VMArray> _buf;
  VMWord _n;
  VMWord _direction;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _noret = field(VMWord);
    _temps = new Array<VMByte>(this,12);
    _state = new VMPointer<Keyring_DESstate>.field(this);
    _buf = new VMPointer<VMArray>.field(this);
    _n = field(VMWord);
    _direction = field(VMWord);
  }
}
class F_Keyring_desecb extends VMStruct{
  Array<VMWord> _regs;
  VMWord _noret;
  Array<VMByte> _temps;
  VMPointer<Keyring_DESstate> _state;
  VMPointer<VMArray> _buf;
  VMWord _n;
  VMWord _direction;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _noret = field(VMWord);
    _temps = new Array<VMByte>(this,12);
    _state = new VMPointer<Keyring_DESstate>.field(this);
    _buf = new VMPointer<VMArray>.field(this);
    _n = field(VMWord);
    _direction = field(VMWord);
  }
}
class F_Keyring_dessetup extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Keyring_DESstate>> _ret;
  Array<VMByte> _temps;
  VMPointer<VMArray> _key;
  VMPointer<VMArray> _ivec;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Keyring_DESstate>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _key = new VMPointer<VMArray>.field(this);
    _ivec = new VMPointer<VMArray>.field(this);
  }
}
class F_Keyring_dhparams_ret extends VMStruct{
  VMPointer<Keyring_IPint> _t0;
  VMPointer<Keyring_IPint> _t1;
  fields(){
    _t0 = new VMPointer<Keyring_IPint>.field(this);
    _t1 = new VMPointer<Keyring_IPint>.field(this);
  }
}class F_Keyring_dhparams extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<F_Keyring_dhparams_ret> _ret;
  Array<VMByte> _temps;
  VMWord _nbits;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<F_Keyring_dhparams_ret>.field(this);
    _temps = new Array<VMByte>(this,12);
    _nbits = field(VMWord);
  }
}
class F_IPint_div_ret extends VMStruct{
  VMPointer<Keyring_IPint> _t0;
  VMPointer<Keyring_IPint> _t1;
  fields(){
    _t0 = new VMPointer<Keyring_IPint>.field(this);
    _t1 = new VMPointer<Keyring_IPint>.field(this);
  }
}class F_IPint_div extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<F_IPint_div_ret> _ret;
  Array<VMByte> _temps;
  VMPointer<Keyring_IPint> _i1;
  VMPointer<Keyring_IPint> _i2;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<F_IPint_div_ret>.field(this);
    _temps = new Array<VMByte>(this,12);
    _i1 = new VMPointer<Keyring_IPint>.field(this);
    _i2 = new VMPointer<Keyring_IPint>.field(this);
  }
}
class F_RSApk_encrypt extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Keyring_IPint>> _ret;
  Array<VMByte> _temps;
  VMPointer<Keyring_RSApk> _k;
  VMPointer<Keyring_IPint> _m;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Keyring_IPint>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _k = new VMPointer<Keyring_RSApk>.field(this);
    _m = new VMPointer<Keyring_IPint>.field(this);
  }
}
class F_IPint_eq extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  VMPointer<Keyring_IPint> _i1;
  VMPointer<Keyring_IPint> _i2;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _i1 = new VMPointer<Keyring_IPint>.field(this);
    _i2 = new VMPointer<Keyring_IPint>.field(this);
  }
}
class F_IPint_expmod extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Keyring_IPint>> _ret;
  Array<VMByte> _temps;
  VMPointer<Keyring_IPint> _base;
  VMPointer<Keyring_IPint> _exp;
  VMPointer<Keyring_IPint> _mod;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Keyring_IPint>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _base = new VMPointer<Keyring_IPint>.field(this);
    _exp = new VMPointer<Keyring_IPint>.field(this);
    _mod = new VMPointer<Keyring_IPint>.field(this);
  }
}
class F_RSAsk_fill extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Keyring_RSAsk>> _ret;
  Array<VMByte> _temps;
  VMPointer<Keyring_IPint> _n;
  VMPointer<Keyring_IPint> _e;
  VMPointer<Keyring_IPint> _d;
  VMPointer<Keyring_IPint> _p;
  VMPointer<Keyring_IPint> _q;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Keyring_RSAsk>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _n = new VMPointer<Keyring_IPint>.field(this);
    _e = new VMPointer<Keyring_IPint>.field(this);
    _d = new VMPointer<Keyring_IPint>.field(this);
    _p = new VMPointer<Keyring_IPint>.field(this);
    _q = new VMPointer<Keyring_IPint>.field(this);
  }
}
class F_RSAsk_gen extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Keyring_RSAsk>> _ret;
  Array<VMByte> _temps;
  VMWord _nlen;
  VMWord _elen;
  VMWord _nrep;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Keyring_RSAsk>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _nlen = field(VMWord);
    _elen = field(VMWord);
    _nrep = field(VMWord);
  }
}
class F_DSAsk_gen extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Keyring_DSAsk>> _ret;
  Array<VMByte> _temps;
  VMPointer<Keyring_DSApk> _oldpk;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Keyring_DSAsk>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _oldpk = new VMPointer<Keyring_DSApk>.field(this);
  }
}
class F_EGsk_gen extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Keyring_EGsk>> _ret;
  Array<VMByte> _temps;
  VMWord _nlen;
  VMWord _nrep;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Keyring_EGsk>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _nlen = field(VMWord);
    _nrep = field(VMWord);
  }
}
class F_Keyring_genSK extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Keyring_SK>> _ret;
  Array<VMByte> _temps;
  VMPointer<VMString> _algname;
  VMPointer<VMString> _owner;
  VMWord _length;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Keyring_SK>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _algname = new VMPointer<VMString>.field(this);
    _owner = new VMPointer<VMString>.field(this);
    _length = field(VMWord);
  }
}
class F_Keyring_genSKfromPK extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Keyring_SK>> _ret;
  Array<VMByte> _temps;
  VMPointer<Keyring_PK> _pk;
  VMPointer<VMString> _owner;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Keyring_SK>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _pk = new VMPointer<Keyring_PK>.field(this);
    _owner = new VMPointer<VMString>.field(this);
  }
}
class F_Keyring_getbytearray_ret extends VMStruct{
  VMPointer<VMArray> _t0;
  VMPointer<VMString> _t1;
  fields(){
    _t0 = new VMPointer<VMArray>.field(this);
    _t1 = new VMPointer<VMString>.field(this);
  }
}class F_Keyring_getbytearray extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<F_Keyring_getbytearray_ret> _ret;
  Array<VMByte> _temps;
  VMPointer<Sys_FD> _fd;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<F_Keyring_getbytearray_ret>.field(this);
    _temps = new Array<VMByte>(this,12);
    _fd = new VMPointer<Sys_FD>.field(this);
  }
}
class F_Keyring_getmsg extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<VMArray>> _ret;
  Array<VMByte> _temps;
  VMPointer<Sys_FD> _fd;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<VMArray>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _fd = new VMPointer<Sys_FD>.field(this);
  }
}
class F_Keyring_getstring_ret extends VMStruct{
  VMPointer<VMString> _t0;
  VMPointer<VMString> _t1;
  fields(){
    _t0 = new VMPointer<VMString>.field(this);
    _t1 = new VMPointer<VMString>.field(this);
  }
}class F_Keyring_getstring extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<F_Keyring_getstring_ret> _ret;
  Array<VMByte> _temps;
  VMPointer<Sys_FD> _fd;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<F_Keyring_getstring_ret>.field(this);
    _temps = new Array<VMByte>(this,12);
    _fd = new VMPointer<Sys_FD>.field(this);
  }
}
class F_Keyring_hmac_md5 extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Keyring_DigestState>> _ret;
  Array<VMByte> _temps;
  VMPointer<VMArray> _data;
  VMWord _n;
  VMPointer<VMArray> _key;
  VMPointer<VMArray> _digest;
  VMPointer<Keyring_DigestState> _state;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Keyring_DigestState>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _data = new VMPointer<VMArray>.field(this);
    _n = field(VMWord);
    _key = new VMPointer<VMArray>.field(this);
    _digest = new VMPointer<VMArray>.field(this);
    _state = new VMPointer<Keyring_DigestState>.field(this);
  }
}
class F_Keyring_hmac_sha1 extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Keyring_DigestState>> _ret;
  Array<VMByte> _temps;
  VMPointer<VMArray> _data;
  VMWord _n;
  VMPointer<VMArray> _key;
  VMPointer<VMArray> _digest;
  VMPointer<Keyring_DigestState> _state;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Keyring_DigestState>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _data = new VMPointer<VMArray>.field(this);
    _n = field(VMWord);
    _key = new VMPointer<VMArray>.field(this);
    _digest = new VMPointer<VMArray>.field(this);
    _state = new VMPointer<Keyring_DigestState>.field(this);
  }
}
class F_Keyring_ideacbc extends VMStruct{
  Array<VMWord> _regs;
  VMWord _noret;
  Array<VMByte> _temps;
  VMPointer<Keyring_IDEAstate> _state;
  VMPointer<VMArray> _buf;
  VMWord _n;
  VMWord _direction;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _noret = field(VMWord);
    _temps = new Array<VMByte>(this,12);
    _state = new VMPointer<Keyring_IDEAstate>.field(this);
    _buf = new VMPointer<VMArray>.field(this);
    _n = field(VMWord);
    _direction = field(VMWord);
  }
}
class F_Keyring_ideaecb extends VMStruct{
  Array<VMWord> _regs;
  VMWord _noret;
  Array<VMByte> _temps;
  VMPointer<Keyring_IDEAstate> _state;
  VMPointer<VMArray> _buf;
  VMWord _n;
  VMWord _direction;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _noret = field(VMWord);
    _temps = new Array<VMByte>(this,12);
    _state = new VMPointer<Keyring_IDEAstate>.field(this);
    _buf = new VMPointer<VMArray>.field(this);
    _n = field(VMWord);
    _direction = field(VMWord);
  }
}
class F_Keyring_ideasetup extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Keyring_IDEAstate>> _ret;
  Array<VMByte> _temps;
  VMPointer<VMArray> _key;
  VMPointer<VMArray> _ivec;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Keyring_IDEAstate>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _key = new VMPointer<VMArray>.field(this);
    _ivec = new VMPointer<VMArray>.field(this);
  }
}
class F_IPint_inttoip extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Keyring_IPint>> _ret;
  Array<VMByte> _temps;
  VMWord _i;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Keyring_IPint>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _i = field(VMWord);
  }
}
class F_IPint_invert extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Keyring_IPint>> _ret;
  Array<VMByte> _temps;
  VMPointer<Keyring_IPint> _base;
  VMPointer<Keyring_IPint> _mod;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Keyring_IPint>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _base = new VMPointer<Keyring_IPint>.field(this);
    _mod = new VMPointer<Keyring_IPint>.field(this);
  }
}
class F_IPint_iptob64 extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<VMString>> _ret;
  Array<VMByte> _temps;
  VMPointer<Keyring_IPint> _i;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<VMString>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _i = new VMPointer<Keyring_IPint>.field(this);
  }
}
class F_IPint_iptob64z extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<VMString>> _ret;
  Array<VMByte> _temps;
  VMPointer<Keyring_IPint> _i;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<VMString>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _i = new VMPointer<Keyring_IPint>.field(this);
  }
}
class F_IPint_iptobebytes extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<VMArray>> _ret;
  Array<VMByte> _temps;
  VMPointer<Keyring_IPint> _i;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<VMArray>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _i = new VMPointer<Keyring_IPint>.field(this);
  }
}
class F_IPint_iptobytes extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<VMArray>> _ret;
  Array<VMByte> _temps;
  VMPointer<Keyring_IPint> _i;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<VMArray>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _i = new VMPointer<Keyring_IPint>.field(this);
  }
}
class F_IPint_iptoint extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  VMPointer<Keyring_IPint> _i;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _i = new VMPointer<Keyring_IPint>.field(this);
  }
}
class F_IPint_iptostr extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<VMString>> _ret;
  Array<VMByte> _temps;
  VMPointer<Keyring_IPint> _i;
  VMWord _base;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<VMString>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _i = new VMPointer<Keyring_IPint>.field(this);
    _base = field(VMWord);
  }
}
class F_Keyring_md4 extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Keyring_DigestState>> _ret;
  Array<VMByte> _temps;
  VMPointer<VMArray> _buf;
  VMWord _n;
  VMPointer<VMArray> _digest;
  VMPointer<Keyring_DigestState> _state;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Keyring_DigestState>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _buf = new VMPointer<VMArray>.field(this);
    _n = field(VMWord);
    _digest = new VMPointer<VMArray>.field(this);
    _state = new VMPointer<Keyring_DigestState>.field(this);
  }
}
class F_Keyring_md5 extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Keyring_DigestState>> _ret;
  Array<VMByte> _temps;
  VMPointer<VMArray> _buf;
  VMWord _n;
  VMPointer<VMArray> _digest;
  VMPointer<Keyring_DigestState> _state;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Keyring_DigestState>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _buf = new VMPointer<VMArray>.field(this);
    _n = field(VMWord);
    _digest = new VMPointer<VMArray>.field(this);
    _state = new VMPointer<Keyring_DigestState>.field(this);
  }
}
class F_IPint_mod extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Keyring_IPint>> _ret;
  Array<VMByte> _temps;
  VMPointer<Keyring_IPint> _i1;
  VMPointer<Keyring_IPint> _i2;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Keyring_IPint>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _i1 = new VMPointer<Keyring_IPint>.field(this);
    _i2 = new VMPointer<Keyring_IPint>.field(this);
  }
}
class F_IPint_mul extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Keyring_IPint>> _ret;
  Array<VMByte> _temps;
  VMPointer<Keyring_IPint> _i1;
  VMPointer<Keyring_IPint> _i2;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Keyring_IPint>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _i1 = new VMPointer<Keyring_IPint>.field(this);
    _i2 = new VMPointer<Keyring_IPint>.field(this);
  }
}
class F_IPint_neg extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Keyring_IPint>> _ret;
  Array<VMByte> _temps;
  VMPointer<Keyring_IPint> _i;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Keyring_IPint>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _i = new VMPointer<Keyring_IPint>.field(this);
  }
}
class F_IPint_not extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Keyring_IPint>> _ret;
  Array<VMByte> _temps;
  VMPointer<Keyring_IPint> _i1;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Keyring_IPint>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _i1 = new VMPointer<Keyring_IPint>.field(this);
  }
}
class F_IPint_ori extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Keyring_IPint>> _ret;
  Array<VMByte> _temps;
  VMPointer<Keyring_IPint> _i1;
  VMPointer<Keyring_IPint> _i2;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Keyring_IPint>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _i1 = new VMPointer<Keyring_IPint>.field(this);
    _i2 = new VMPointer<Keyring_IPint>.field(this);
  }
}
class F_Keyring_pktoattr extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<VMString>> _ret;
  Array<VMByte> _temps;
  VMPointer<Keyring_PK> _pk;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<VMString>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _pk = new VMPointer<Keyring_PK>.field(this);
  }
}
class F_Keyring_pktostr extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<VMString>> _ret;
  Array<VMByte> _temps;
  VMPointer<Keyring_PK> _pk;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<VMString>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _pk = new VMPointer<Keyring_PK>.field(this);
  }
}
class F_Keyring_putbytearray extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  VMPointer<Sys_FD> _fd;
  VMPointer<VMArray> _a;
  VMWord _n;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _fd = new VMPointer<Sys_FD>.field(this);
    _a = new VMPointer<VMArray>.field(this);
    _n = field(VMWord);
  }
}
class F_Keyring_puterror extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  VMPointer<Sys_FD> _fd;
  VMPointer<VMString> _s;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _fd = new VMPointer<Sys_FD>.field(this);
    _s = new VMPointer<VMString>.field(this);
  }
}
class F_Keyring_putstring extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  VMPointer<Sys_FD> _fd;
  VMPointer<VMString> _s;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _fd = new VMPointer<Sys_FD>.field(this);
    _s = new VMPointer<VMString>.field(this);
  }
}
class F_IPint_random extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Keyring_IPint>> _ret;
  Array<VMByte> _temps;
  VMWord _minbits;
  VMWord _maxbits;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Keyring_IPint>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _minbits = field(VMWord);
    _maxbits = field(VMWord);
  }
}
class F_Keyring_rc4 extends VMStruct{
  Array<VMWord> _regs;
  VMWord _noret;
  Array<VMByte> _temps;
  VMPointer<Keyring_RC4state> _state;
  VMPointer<VMArray> _buf;
  VMWord _n;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _noret = field(VMWord);
    _temps = new Array<VMByte>(this,12);
    _state = new VMPointer<Keyring_RC4state>.field(this);
    _buf = new VMPointer<VMArray>.field(this);
    _n = field(VMWord);
  }
}
class F_Keyring_rc4back extends VMStruct{
  Array<VMWord> _regs;
  VMWord _noret;
  Array<VMByte> _temps;
  VMPointer<Keyring_RC4state> _state;
  VMWord _n;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _noret = field(VMWord);
    _temps = new Array<VMByte>(this,12);
    _state = new VMPointer<Keyring_RC4state>.field(this);
    _n = field(VMWord);
  }
}
class F_Keyring_rc4setup extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Keyring_RC4state>> _ret;
  Array<VMByte> _temps;
  VMPointer<VMArray> _seed;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Keyring_RC4state>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _seed = new VMPointer<VMArray>.field(this);
  }
}
class F_Keyring_rc4skip extends VMStruct{
  Array<VMWord> _regs;
  VMWord _noret;
  Array<VMByte> _temps;
  VMPointer<Keyring_RC4state> _state;
  VMWord _n;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _noret = field(VMWord);
    _temps = new Array<VMByte>(this,12);
    _state = new VMPointer<Keyring_RC4state>.field(this);
    _n = field(VMWord);
  }
}
class F_Keyring_readauthinfo extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Keyring_Authinfo>> _ret;
  Array<VMByte> _temps;
  VMPointer<VMString> _filename;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Keyring_Authinfo>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _filename = new VMPointer<VMString>.field(this);
  }
}
class F_Keyring_senderrmsg extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  VMPointer<Sys_FD> _fd;
  VMPointer<VMString> _s;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _fd = new VMPointer<Sys_FD>.field(this);
    _s = new VMPointer<VMString>.field(this);
  }
}
class F_Keyring_sendmsg extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  VMPointer<Sys_FD> _fd;
  VMPointer<VMArray> _buf;
  VMWord _n;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _fd = new VMPointer<Sys_FD>.field(this);
    _buf = new VMPointer<VMArray>.field(this);
    _n = field(VMWord);
  }
}
class F_Keyring_sha1 extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Keyring_DigestState>> _ret;
  Array<VMByte> _temps;
  VMPointer<VMArray> _buf;
  VMWord _n;
  VMPointer<VMArray> _digest;
  VMPointer<Keyring_DigestState> _state;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Keyring_DigestState>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _buf = new VMPointer<VMArray>.field(this);
    _n = field(VMWord);
    _digest = new VMPointer<VMArray>.field(this);
    _state = new VMPointer<Keyring_DigestState>.field(this);
  }
}
class F_IPint_shl extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Keyring_IPint>> _ret;
  Array<VMByte> _temps;
  VMPointer<Keyring_IPint> _i;
  VMWord _n;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Keyring_IPint>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _i = new VMPointer<Keyring_IPint>.field(this);
    _n = field(VMWord);
  }
}
class F_IPint_shr extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Keyring_IPint>> _ret;
  Array<VMByte> _temps;
  VMPointer<Keyring_IPint> _i;
  VMWord _n;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Keyring_IPint>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _i = new VMPointer<Keyring_IPint>.field(this);
    _n = field(VMWord);
  }
}
class F_Keyring_sign extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Keyring_Certificate>> _ret;
  Array<VMByte> _temps;
  VMPointer<Keyring_SK> _sk;
  VMWord _exp;
  VMPointer<Keyring_DigestState> _state;
  VMPointer<VMString> _ha;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Keyring_Certificate>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _sk = new VMPointer<Keyring_SK>.field(this);
    _exp = field(VMWord);
    _state = new VMPointer<Keyring_DigestState>.field(this);
    _ha = new VMPointer<VMString>.field(this);
  }
}
class F_RSAsk_sign extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Keyring_RSAsig>> _ret;
  Array<VMByte> _temps;
  VMPointer<Keyring_RSAsk> _k;
  VMPointer<Keyring_IPint> _m;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Keyring_RSAsig>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _k = new VMPointer<Keyring_RSAsk>.field(this);
    _m = new VMPointer<Keyring_IPint>.field(this);
  }
}
class F_DSAsk_sign extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Keyring_DSAsig>> _ret;
  Array<VMByte> _temps;
  VMPointer<Keyring_DSAsk> _k;
  VMPointer<Keyring_IPint> _m;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Keyring_DSAsig>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _k = new VMPointer<Keyring_DSAsk>.field(this);
    _m = new VMPointer<Keyring_IPint>.field(this);
  }
}
class F_EGsk_sign extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Keyring_EGsig>> _ret;
  Array<VMByte> _temps;
  VMPointer<Keyring_EGsk> _k;
  VMPointer<Keyring_IPint> _m;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Keyring_EGsig>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _k = new VMPointer<Keyring_EGsk>.field(this);
    _m = new VMPointer<Keyring_IPint>.field(this);
  }
}
class F_Keyring_signm extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Keyring_Certificate>> _ret;
  Array<VMByte> _temps;
  VMPointer<Keyring_SK> _sk;
  VMPointer<Keyring_IPint> _m;
  VMPointer<VMString> _ha;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Keyring_Certificate>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _sk = new VMPointer<Keyring_SK>.field(this);
    _m = new VMPointer<Keyring_IPint>.field(this);
    _ha = new VMPointer<VMString>.field(this);
  }
}
class F_Keyring_sktoattr extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<VMString>> _ret;
  Array<VMByte> _temps;
  VMPointer<Keyring_SK> _sk;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<VMString>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _sk = new VMPointer<Keyring_SK>.field(this);
  }
}
class F_Keyring_sktopk extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Keyring_PK>> _ret;
  Array<VMByte> _temps;
  VMPointer<Keyring_SK> _sk;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Keyring_PK>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _sk = new VMPointer<Keyring_SK>.field(this);
  }
}
class F_Keyring_sktostr extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<VMString>> _ret;
  Array<VMByte> _temps;
  VMPointer<Keyring_SK> _sk;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<VMString>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _sk = new VMPointer<Keyring_SK>.field(this);
  }
}
class F_Keyring_strtocert extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Keyring_Certificate>> _ret;
  Array<VMByte> _temps;
  VMPointer<VMString> _s;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Keyring_Certificate>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _s = new VMPointer<VMString>.field(this);
  }
}
class F_IPint_strtoip extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Keyring_IPint>> _ret;
  Array<VMByte> _temps;
  VMPointer<VMString> _str;
  VMWord _base;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Keyring_IPint>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _str = new VMPointer<VMString>.field(this);
    _base = field(VMWord);
  }
}
class F_Keyring_strtopk extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Keyring_PK>> _ret;
  Array<VMByte> _temps;
  VMPointer<VMString> _s;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Keyring_PK>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _s = new VMPointer<VMString>.field(this);
  }
}
class F_Keyring_strtosk extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Keyring_SK>> _ret;
  Array<VMByte> _temps;
  VMPointer<VMString> _s;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Keyring_SK>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _s = new VMPointer<VMString>.field(this);
  }
}
class F_IPint_sub extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Keyring_IPint>> _ret;
  Array<VMByte> _temps;
  VMPointer<Keyring_IPint> _i1;
  VMPointer<Keyring_IPint> _i2;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Keyring_IPint>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _i1 = new VMPointer<Keyring_IPint>.field(this);
    _i2 = new VMPointer<Keyring_IPint>.field(this);
  }
}
class F_Keyring_verify extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  VMPointer<Keyring_PK> _pk;
  VMPointer<Keyring_Certificate> _cert;
  VMPointer<Keyring_DigestState> _state;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _pk = new VMPointer<Keyring_PK>.field(this);
    _cert = new VMPointer<Keyring_Certificate>.field(this);
    _state = new VMPointer<Keyring_DigestState>.field(this);
  }
}
class F_RSApk_verify extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  VMPointer<Keyring_RSApk> _k;
  VMPointer<Keyring_RSAsig> _sig;
  VMPointer<Keyring_IPint> _m;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _k = new VMPointer<Keyring_RSApk>.field(this);
    _sig = new VMPointer<Keyring_RSAsig>.field(this);
    _m = new VMPointer<Keyring_IPint>.field(this);
  }
}
class F_DSApk_verify extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  VMPointer<Keyring_DSApk> _k;
  VMPointer<Keyring_DSAsig> _sig;
  VMPointer<Keyring_IPint> _m;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _k = new VMPointer<Keyring_DSApk>.field(this);
    _sig = new VMPointer<Keyring_DSAsig>.field(this);
    _m = new VMPointer<Keyring_IPint>.field(this);
  }
}
class F_EGpk_verify extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  VMPointer<Keyring_EGpk> _k;
  VMPointer<Keyring_EGsig> _sig;
  VMPointer<Keyring_IPint> _m;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _k = new VMPointer<Keyring_EGpk>.field(this);
    _sig = new VMPointer<Keyring_EGsig>.field(this);
    _m = new VMPointer<Keyring_IPint>.field(this);
  }
}
class F_Keyring_verifym extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  VMPointer<Keyring_PK> _pk;
  VMPointer<Keyring_Certificate> _cert;
  VMPointer<Keyring_IPint> _m;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _pk = new VMPointer<Keyring_PK>.field(this);
    _cert = new VMPointer<Keyring_Certificate>.field(this);
    _m = new VMPointer<Keyring_IPint>.field(this);
  }
}
class F_Keyring_writeauthinfo extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  VMPointer<VMString> _filename;
  VMPointer<Keyring_Authinfo> _info;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _filename = new VMPointer<VMString>.field(this);
    _info = new VMPointer<Keyring_Authinfo>.field(this);
  }
}
class F_IPint_xor extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Keyring_IPint>> _ret;
  Array<VMByte> _temps;
  VMPointer<Keyring_IPint> _i1;
  VMPointer<Keyring_IPint> _i2;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Keyring_IPint>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _i1 = new VMPointer<Keyring_IPint>.field(this);
    _i2 = new VMPointer<Keyring_IPint>.field(this);
  }
}
var Keyring_PATH = r'$Keyring';
var Keyring_SHA1dlen = 20;
var Keyring_MD5dlen = 16;
var Keyring_MD4dlen = 16;
var Keyring_Encrypt = 0;
var Keyring_Decrypt = 1;
var Keyring_AESbsize = 16;
var Keyring_DESbsize = 8;
var Keyring_IDEAbsize = 8;
var Keyring_BFbsize = 8;
class F_Loader_compile extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  VMPointer<VMModLink> _mp;
  VMWord _flag;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _mp = new VMPointer<VMModLink>.field(this);
    _flag = field(VMWord);
  }
}
class F_Loader_dnew extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Loader_Niladt>> _ret;
  Array<VMByte> _temps;
  VMWord _size;
  VMPointer<VMArray> _map;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Loader_Niladt>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _size = field(VMWord);
    _map = new VMPointer<VMArray>.field(this);
  }
}
class F_Loader_ext extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  VMPointer<VMModLink> _mp;
  VMWord _idx;
  VMWord _pc;
  VMWord _tdesc;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _mp = new VMPointer<VMModLink>.field(this);
    _idx = field(VMWord);
    _pc = field(VMWord);
    _tdesc = field(VMWord);
  }
}
class F_Loader_ifetch extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<VMArray>> _ret;
  Array<VMByte> _temps;
  VMPointer<VMModLink> _mp;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<VMArray>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _mp = new VMPointer<VMModLink>.field(this);
  }
}
class F_Loader_link extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<VMArray>> _ret;
  Array<VMByte> _temps;
  VMPointer<VMModLink> _mp;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<VMArray>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _mp = new VMPointer<VMModLink>.field(this);
  }
}
class F_Loader_newmod extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<VMModLink>> _ret;
  Array<VMByte> _temps;
  VMPointer<VMString> _name;
  VMWord _ss;
  VMWord _nlink;
  VMPointer<VMArray> _inst;
  VMPointer<Loader_Niladt> _data;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<VMModLink>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _name = new VMPointer<VMString>.field(this);
    _ss = field(VMWord);
    _nlink = field(VMWord);
    _inst = new VMPointer<VMArray>.field(this);
    _data = new VMPointer<Loader_Niladt>.field(this);
  }
}
class F_Loader_tdesc extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<VMArray>> _ret;
  Array<VMByte> _temps;
  VMPointer<VMModLink> _mp;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<VMArray>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _mp = new VMPointer<VMModLink>.field(this);
  }
}
class F_Loader_tnew extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  VMPointer<VMModLink> _mp;
  VMWord _size;
  VMPointer<VMArray> _map;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _mp = new VMPointer<VMModLink>.field(this);
    _size = field(VMWord);
    _map = new VMPointer<VMArray>.field(this);
  }
}
var Loader_PATH = r'$Loader';
class F_Face_haschar extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMWord> _ret;
  Array<VMByte> _temps;
  VMPointer<Freetype_Face> _face;
  VMWord _c;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMWord>.field(this);
    _temps = new Array<VMByte>(this,12);
    _face = new VMPointer<Freetype_Face>.field(this);
    _c = field(VMWord);
  }
}
class F_Face_loadglyph extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Freetype_Glyph>> _ret;
  Array<VMByte> _temps;
  VMPointer<Freetype_Face> _face;
  VMWord _c;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Freetype_Glyph>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _face = new VMPointer<Freetype_Face>.field(this);
    _c = field(VMWord);
  }
}
class F_Freetype_newface extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Freetype_Face>> _ret;
  Array<VMByte> _temps;
  VMPointer<VMString> _path;
  VMWord _index;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Freetype_Face>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _path = new VMPointer<VMString>.field(this);
    _index = field(VMWord);
  }
}
class F_Freetype_newmemface extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<Freetype_Face>> _ret;
  Array<VMByte> _temps;
  VMPointer<VMArray> _data;
  VMWord _index;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<Freetype_Face>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _data = new VMPointer<VMArray>.field(this);
    _index = field(VMWord);
  }
}
class F_Face_setcharsize extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<VMString>> _ret;
  Array<VMByte> _temps;
  VMPointer<Freetype_Face> _face;
  VMWord _pts;
  VMWord _hdpi;
  VMWord _vdpi;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<VMString>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _face = new VMPointer<Freetype_Face>.field(this);
    _pts = field(VMWord);
    _hdpi = field(VMWord);
    _vdpi = field(VMWord);
  }
}
class F_Face_settransform extends VMStruct{
  Array<VMWord> _regs;
  VMPointer<VMPointer<VMString>> _ret;
  Array<VMByte> _temps;
  VMPointer<Freetype_Face> _face;
  VMPointer<Freetype_Matrix> _m;
  VMPointer<Freetype_Vector> _v;
  fields(){
    _regs = new Array<VMWord>(this,NREG-1);
    _ret = new VMPointer<VMPointer<VMString>>.field(this);
    _temps = new Array<VMByte>(this,12);
    _face = new VMPointer<Freetype_Face>.field(this);
    _m = new VMPointer<Freetype_Matrix>.field(this);
    _v = new VMPointer<Freetype_Vector>.field(this);
  }
}
var Freetype_PATH = r'$Freetype';
var Freetype_STYLE_ITALIC = 1;
var Freetype_STYLE_BOLD = 2;