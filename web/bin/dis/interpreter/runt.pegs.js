{

    function nonIgnore(x){
        return !(x.constructor == $Ignore);
    }

    function wrap(bs) {
        return function(x){
            return bs[0] + x + bs[1]; 
        };
    }

    var round  = wrap("()");
    var square = wrap("[]");
    var curly  = wrap("{}");
    var angled = wrap("<>");
    var single = wrap("''");
    var double = wrap('""');


    function $File(d, m){
        this.decs   = d;
        this.module = m;
    }
    function $Define(n, v){
        this.name   = n;
        this.value  = v;
    }
    function $Field(n,t){
        this.name   = n;
        this.type   = t;
    }
    function $Con(n, v){
        this.name   = n;
        this.value  = v;
    }
    function $Struct(n, f){
        this.name   = n;
        this.fields = f;
    }
    function $Type(n){
        this.name   = n;
    }
    function $Ignore(){}

    function $Array(t, l)   {
        this.type   = t;
        this.length = l;
    }
    function $Pointer(t, d) { 
        this.type = (d == 1) ? t : new $Pointer(t, d-1);
    }


    function debug(n){
        var $ = debug;

        switch(n.constructor){

        case Number:         return n;
        case String:         return n;
        case Array:          return map($, n);

        case $File:          return $(n.decs);
        case $Define:        return 'var ' + n.name + ' = ' + n.value + ";";

        case $Struct:        return n.name + curly($(n.fields));
        case $Field:         return n.name + ":" + $(n.type); 
        case $Type:          return $(n.name);
        case $Pointer:       return "*" + $(n.type);
        case $Array:         return $(n.type) + square(n.length);

        case $Ignore:        return '';

        }

    }

    //node, context
    function toDart(n, c){
        var $ = toDart;

        switch(n.constructor){
        case Array:          return map($, n);
        case $File:          return $(n.decs);
        case $Define:        return 'var ' + n.name + ' = ' + n.value + ";";
        case $Struct:        return dartStruct(n);
        case $Ignore:        return '';
        }
    }



    function dartStruct(s){
        function init(n){
            var $ = init;

            switch(n.constructor){
            case $Field:    return '_' + n.name + " = " + $(n.type); 
            case $Array:    return 'new ' + dartType(n) + '(this,' + n.length + ')'
            case $Pointer:  return 'new ' + dartType(n) + '.field(this)';
            case $Type:     return 'field' + round(dartType(n));
            }
        }

        function dartType(n){
            var $ = dartType;

            switch(n.constructor){
            case $Array   : return 'Array' + angled($(n.type));
            case $Pointer : return 'VMPointer' + angled($(n.type));
            case $Struct  : return retStructName();
            }
            return primitives[n.name] ? primitives[n.name] : n.name
        }

        function retStructName(){
            return s.name + '_ret';
        }

        function retStruct(){
            function isRetStruct(t){ 
                return (t instanceof $Pointer) && (t.type instanceof $Struct);
            }
            function type(f){
                return f.type;
            }
            var ret = map(type, s.fields).filter(isRetStruct);
            if(ret.length == 0) 
                return "";

            var struct = ret[0].type;
            struct.name = retStructName();
            return toDart(struct);
        }

        function fieldDec(f){ 
            return dartType(f.type) + ' _' + f.name + ';'; 
        }
        var listitem = wrap(['\t\t' , ';\n']);


        var header = "class " + s.name + ' extends VMStruct';
        var decs   = '\n\t' + map(fieldDec, s.fields).join('\n\t');
        var inits  =  map(listitem, map(init, s.fields)).join('');
        var fieldfun = '\n\t' + 'fields()' + curly('\n' + inits + '\t') + '\n';

        

        return retStruct() + header + curly(decs + fieldfun);
    }



    primitives = {
        uchar  : 'VMByte',
        BYTE   : 'VMByte',
        char   : 'VMChar',
        WORD   : 'VMWord',
        LONG   : 'VMLong',
        REAL   : 'VMDouble',
        Channel: 'VMChannel',
        List   : 'VMList',
        String : 'VMString',
        Array  : 'VMArray',
        Modlink: 'VMModLink',
        
    }


    /////////////////////////////////////////////////////////////////////
    // collections

    function map(fun, col){
        switch(col.constructor){
        case String:
            return map(fun, col.split(''))
        case Array:
            return col.map(fun)
        case Object:
            var m = {};
            for(var key in col)
                m[key] = fun(col[key], key, col)
            return m
        }
    }

    function fold(fun, col){
        switch(col.constructor){
        case Array:
            return col.reduce(fun)
        case Object:
            return fold(fun, values(col))
        }
    }

    function keys(obj){
        return Object.keys(obj)
    }
    function values(obj){
        return keys(obj).map(function(k){ return obj[k] })
    }
    function makeArray(n, fun){
        var a = []
        fun = fun || $1
        for(var i = 0; i < n; i++)
            a.push(fun(i))
        return a
    }
}

/////////////////////////////////////////////////////////////////////
start
    = f:file { return toDart(f, {}).join('\n'); }

file
    = d:line+ { return new $File(d.filter(nonIgnore)); }

line
    = _? d:declaration _? { return d; }

declaration
    = struct
    / define 
    / i:ignore { return new $Ignore(i); }
    / _


ignore
    = 'typedef' ws 'struct' ws id ws id ';'
    / 'typedef' ws id [^\n]*
    / simple ws? id '(' [^\)]* ')' ';'
    / '#pragma' [^\n]*

/////////////////////////////////////////////////////////////////////
open  = _? '{' _?
/////////////////////////////////////////////////////////////////////
close = _? '}' _? ';'? _?

define
    = '#define' ws i:id ws b:def_body { return new $Define(i,b); }

def_body
    = def_list
    / def_string
    / def_ieee              
    / e:([^\n]*)            { return e.join(''); }

def_list
    = '{' e:([^\}]*) '}'    { return square(e.join(''));}

def_string
    = '"' + t:[^"] + '"'    { return 'r' + single(t.join('')); }

def_ieee
    = 'Infinity'            { return "double.INFINITY"; }
    / 'NaN'                 { return "double.NAN"; }

struct
    = 'struct' ws i:id open f:field* close   { return new $Struct(i,f); }
    / 'typedef' ws f:anon_struct i:id ';'    { return new $Struct(i,f); }

anon_struct
    = 'struct' open f:field* close ws?  { return f; }

field
    = _? t:type _? ';' _           { return t; }
//  / _? t:type _? a:array ';' _   { return new $Array(t, a); }

type
    = s:simple       ws i:id _? a:array { return new $Field(i, new $Array(s, a)); }
    / s:simple r:ref ws i:id _? a:array { return new $Field(i, new $Array(new $Pointer(s,r), a)); }
    / s:simple       ws i:id { return new $Field(i, s); }
    / s:simple r:ref ws i:id { return new $Field(i, new $Pointer(s, r)); }

simple
    = a:anon_struct { return new $Struct('', a); }
    / i:id          { return new $Type(i); }

ref
    = r:'*'+ { return r.length; }

array
    = '[' i:([^\]])+ ']' { return i.join(''); }

/////////////////////////////////////////////////////////////////////


type_in_list
    = t:type ',' _? { return t;}
    / t:type _?     { return t;}



/////////////////////////////////////////////////////////////////////
label
    = _? i:id _? ":" _? { return i;}

id
    = h:char t:[a-zA-Z0-9_]* { return [h].concat(t).join(""); }

/////////////////////////////////////////////////////////////////////
expr
    = '-' ? integer
    / string
    / list 

list 
    = '{' l:[^}]* '}' { return square(l.join("")); }

string
    = '"' t:text '"' { return t.join("");}

text
    = [^"]* 


integer 
= b:decimal "r" d:digits { return parseInt(d, b); }
/ decimal

decimal
= digits:[0-9]+ { return parseInt(digits.join(""), 10); }

digits
= d:[0-9a-fA-F]+ { return d.join(""); }


/////////////////////////////////////////////////////////////////////
char
= [_a-zA-Z]

_
= garbage

garbage
= ws garbage?
/ comment garbage?




ws
= [ \t\n\r]+

comment
= '//' [^\n]* { return ""; }