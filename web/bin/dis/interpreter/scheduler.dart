part of dis;

class Scheduler{
  Queue<Prog> run   = new Queue();
  Queue<Prog> progs = new Queue();
  
  Prog get runhd  => run.first;
  Prog get runtl  => run.last;
  Prog get head   => progs.first;
  Prog get tail   => progs.last;  
}
Scheduler isched = new Scheduler();


Prog get currun =>
    isched.runhd;

Prog newprog(Prog parent){
  var p = new Prog();
  isched.progs.add(p);
  addrun(p);
  return p;
}

addrun(Prog p){
  p.state = ProgState.Pready;
  isched.run.add(p);
}
