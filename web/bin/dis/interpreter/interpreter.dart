part of dis;

VMReg R;
VMImports ldt;

const bool DEBUG_ENABLED = false;
const int NREG = 5;


printm() { if(true) print(mainMemory);}
printh() { if(true) print(heapMemory);}
printp() { if(DEBUG_ENABLED){printm(); printh();}}
get regs => 'FP: ${R.FP}\tSP: ${R.SP}\tTS: ${R.TS}';


interpret(String ass){
  interpretInit(ass);
  vmachine();
}

vmachine(){
  var queue = isched.run;
  while(queue.isNotEmpty){
    Prog p = queue.first;
    debug('RUNNING THREAD #${p.pid}');
    xec(p);
    queue.removeFirst();
    if(p.state == ProgState.Pready)
      queue.addLast(p);
  }
}

xec(Prog p){
  int i = 0;
  R = p.R;
  debug(''.padRight(10) + regs);
  for(R.IC = PQUANTA; R.IC > 0; R.IC--){
    var a = interpretInstruction();
    if(DEBUG_ENABLED){
      print('=========================Instruction #${++i}=========================');
      print(a);
      printh();
      //print(heapMemory.stats);
      //print(mainMemory.stats);
    }
  }
}



interpretInstruction(){
  VMInst i = R._PC.dereference;
  String name = opcodes[i.op];
  debug(name.padRight(10) + regs);
  Specification spec = specs[name];
  loadTypes(R, spec);
  loadOperands(R, i);
  int oldPC = R.PC;
  R.PC = (R._PC + 1);
  spec.implementation(R);
  return [toHex(oldPC), name];
}


interpretInit(String ass){
  Prog p = newprog(null);
  R = p.R;
  R.PC = initCodeSection(ass);
  R.entry = new Pointer<VMInst>.typed(R.PC);

  ///awkward
  VMInit init = halloc(VMInit).dereference;
  init.f.t = 0;
  init.f.fp = 0;
  init.fp = init.f.address;
  R.s = init._fp.address;

  print(init.f.type.size);
  print(init.address);
  print(init.fp);
  print(R.s);


  newstack(p);
  VMFrame f = R._FP.dereferenceAs(VMFrame);

  R.MP = malloc(VMByte, 128);
//  R.FP = 1 << 28;
//  R.SP = R.FP + 64;
  R.t = -1;

}

initCodeSection(String assembly){
  var loader = new AssemblyLoader(assembly);
  return loader.load();
}


loadTypes(VMReg R, Specification spec){
  R.midType = spec.operandType('m');
  R.srcType = spec.operandType('s');
  R.dstType = spec.operandType('d');
}

loadOperands(VMReg R, VMInst i) =>
  keys(operands).forEach((k) =>
      loadOperand(R, i, k));

loadOperand(VMReg R, VMInst i, String name){
  Operand op = operands[name];
  int modeEncoding = bits(i.add, op.modeEncodingStart, op.modeEncodingEnd);
  String mode = op.modes[modeEncoding];
  Function loader = op.loaders[mode];
  loader(R, i);
}


