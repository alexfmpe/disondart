part of dis;

const String
exAlt        = "alt send/recv on same chan",
exBusy       = "channel busy",
exModule     = "module not loaded",
exCompile    = "compile failed",
exCrange     = "constant range ",
exCctovflw   = "constant table overflow",
exCphase     = "compiler phase error",
exType       = "type not constructed correctly",
exZdiv       = "zero divide",
exHeap       = "out of memory: heap",
exImage      = "out of memory: image",
exItype      = "inconsistent type",
exMathia     = "invalid math argument",
exBounds     = "array bounds error",
exNegsize    = "negative array size",
exNomem      = "out of memory: main",
exSpawn      = "spawn a builtin module",
exOp         = "illegal dis instruction",
exTcheck     = "type check",
exInval      = "invalid argument",
exNilref     = "dereference of nil",
exRange      = "value out of range",

exTest       = "test failed",
noErrors     = "no errors";




error([String exception = noErrors]){
  if(DEBUG_ENABLED){
    print(heapMemory.stats);
    print(mainMemory.stats);
  }
  print('>>Exiting: $exception');
}

