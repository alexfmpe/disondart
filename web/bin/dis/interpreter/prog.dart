part of dis;

class Enum<T>{
  final T name;
  const Enum(this.name);
  toString() => name;
}

class ProgState extends Enum<String>{
  static const ProgState
    Palt      = const ProgState('Palt'),
    Psend     = const ProgState('Psend'),
    Precv     = const ProgState('Precv'),
    Pdebug    = const ProgState('Pdebug'),
    Pready    = const ProgState('Pready'),
    Pbroken   = const ProgState('Pbroken'),
    Prelease  = const ProgState('Prelease'),
    Pexiting  = const ProgState('Pexiting');  
  const ProgState(String name) : super(name);
}

class Prog{
  static int pidnum = 0;
  bool debugThreads = true;
  
  ProgState _state;
  ProgState get state => _state;
  
  void set state(ProgState ps) {
    
    _state = ps;
    if(debugThreads)
      debug('THREAD $pid : $state');    
  }
  
  int pid = pidnum++;
  int quanta = PQUANTA;
  
  VMReg R = malloc(VMReg).dereference;
  Prog link;  
}
