part of dis;

class Operand{
  String name, accessor;
  int modeEncodingStart;
  int modeEncodingEnd;
  List<String> modes;
  Map<String, Function> loaders;
  Map<String, Function> assemblers;
  Operand(this.name,  this.accessor,
          this.modeEncodingStart, this.modeEncodingEnd,
          this.modes, this.assemblers, this.loaders);

  toString() => '$name';
}

Operand middle      = new Operand('m', 'mid', 6, 8, midModes, midAssemblers, midLoaders);
Operand source      = new Operand('s', 'src', 3, 6, srcModes, srcAssemblers, srcLoaders);
Operand destination = new Operand('d', 'dst', 0, 3, dstModes, dstAssemblers, dstLoaders);


Map<String, Operand> operands = {
 'm' : middle,
 's' : source,
 'd' : destination,
};


List<String> modes =
  ['none', 'immediate', 'indirect(FP)', 'indirect(MP)', 'doubleInd(FP)', 'doubleInd(MP)'];

List<String> midModes = permutate(modes, [0,1,2,3]);
List<String> srcModes = permutate(modes, [3,2,1,0,5,4]);
List<String> dstModes = permutate(modes, [3,2,1,0,5,4]);

Map<String, Function> midAssemblers =
{
 'none'      : (VMInst i, Match m){},
 'immediate' : (VMInst i, Match m) => i.reg = m[1],
 'indirect'  : (VMInst i, Match m) => i.reg = m[1],
};

Map<String, Function> srcAssemblers =
{
 'none'       : (VMInst i, Match m){},
 'immediate'  : (VMInst i, Match m) => i.s.imm = m[1],
 'indirect'   : (VMInst i, Match m) => i.s.ind = m[1],
 'doubleInd'  :
   (VMInst i,  Match m) {
    i.s.i.f = m[2];
    i.s.i.s = m[1];
 },
};

Map<String, Function> dstAssemblers =
{
 'none'       : (VMInst i, Match m){},
 'immediate'  : (VMInst i, Match m) => i.d.imm = m[1],
 'indirect'   : (VMInst i, Match m) => i.d.ind = m[1],
 'doubleInd'  :
   (VMInst i,  Match m) {
    i.d.i.f = m[2];
    i.d.i.s = m[1];
 },
};
///////////////////////////////////////////////////////////////////////////////
Map<String, Function> midLoaders =
{
  'none'          : (VMReg R, VMInst i){},
  'indirect(FP)'  : (VMReg R, VMInst i) => R.m = R.FP + i.reg,
  'indirect(MP)'  : (VMReg R, VMInst i) => R.m = R.MP + i.reg,
  'immediate'     : (VMReg R, VMInst i){
    R.t = i.reg;
    R.m = R._t.address;
  },
};

Map<String, Function> srcLoaders =
{
  'none'          : (VMReg R, VMInst i){},
  'immediate'     : (VMReg R, VMInst i) => R.s = i.s._imm.address,
  'indirect(FP)'  : (VMReg R, VMInst i) => R.s = R.FP + i.s.ind,
  'indirect(MP)'  : (VMReg R, VMInst i) => R.s = R.MP + i.s.ind,
  'doubleInd(FP)' : (VMReg R, VMInst i){
    VMWord first = view(VMWord, [R.FP + i.s.i.f]);
    R.s = first + i.s.i.s;
  },
  'doubleInd(MP)' : (VMReg R, VMInst i){
    VMWord first = view(VMWord, [R.MP + i.s.i.f]);
    R.s = first + i.s.i.s;
  }
};

Map<String, Function> dstLoaders =
{
  'none'          : (VMReg R, VMInst i){},
  'immediate'     : (VMReg R, VMInst i) => R.d = i.d._imm.address,
  'indirect(FP)'  : (VMReg R, VMInst i) => R.d = R.FP + i.d.ind,
  'indirect(MP)'  : (VMReg R, VMInst i) => R.d = R.MP + i.d.ind,
  'doubleInd(FP)' : (VMReg R, VMInst i){
    VMWord first = view(VMWord, [R.FP + i.d.i.f]);
    R.d = first + i.d.i.s;
  },
  'doubleInd(MP)' : (VMReg R, VMInst i){
    VMWord first = view(VMWord, [R.MP + i.d.i.f]);
    R.d = first + i.d.i.s;
  }
};
