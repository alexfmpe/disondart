part of dis;

class AssemblyLoader{
  int address;
  int size;
  String source;
  List<String> sourceLines;
  AssemblyLoader(String this.source){
    sourceLines = lineSplit(source);
    size = sizeof(VMInst) * sourceLines.length;
  }

  load(){
    var ptr = malloc(VMInst, size);
    keys(sourceLines).forEach((i) =>
        loadInstruction(sourceLines[i], ptr[i]));
    return ptr;
  }

  loadInstruction(String line, VMInst i){
    List<String>  pieces = line.split(new RegExp(r'[ ,]+')),
                  tokens = [pieces[0]];

    //re-join strings with spaces
    for(String s in pieces.skip(1)){
      String t = tokens.last;
      if(t.startsWith('"') && !t.endsWith('"'))
        tokens[tokens.length-1] = t + s;
      else
        tokens.add(s);
    }

    String name = tokens[0];
    int op = opcode(name);
    if(op == null){
      loadPseudo(name, tokens);
      return;
    }
    i.op = op;
    i.add = 0;
    keys(operands).forEach((k) =>
        loadOp(i, tokens, k));
  }

  loadOp(VMInst ins, List tokens, String operandName){
    int index = operandIndex(tokens[0], operandName);
    if(index == -1)
      return;
    String assembly = tokens[index + 1];
    Operand op = operands[operandName];

    operandFormats.forEach((format, mode){
      Match m = fullMatch(format, assembly);
      if(m != null){
        int addBits = op.modes.indexOf(mode) << op.modeEncodingStart;
        ins.add += addBits;
        String simpleMode = (new RegExp(r'\w+').firstMatch(mode)[0]);
        op.assemblers[simpleMode](ins, m);
      }
    });
  }

  fullMatch(String re, String s) =>
      (new RegExp(r'^' + re + r'$').firstMatch(s));

  operandIndex(String inst, String op) =>
      spec(inst).operandIndex(op);
}

String
  _num  = r'(-?(?:0x)?' + _hex + '+)',
  _dec  = r'\d',
  _hex  = r'[\da-fA-F]',
  _fp   = r'\(FP\)',
  _mp   = r'\(MP\)',
  _l    = r'\(',
  _r    = r'\)';

Map<String, String> operandFormats =
{
 r''                          : 'none',
 r'\$' + _num                 : 'immediate',
 _num + _fp                   : 'indirect(FP)',
 _num + _mp                   : 'indirect(MP)',
 _num + _l + _num + _fp + _r  : 'doubleInd(FP)',
 _num + _l + _num + _mp + _r  : 'doubleInd(MP)',
};

loadPseudo(String name, List tokens){
  pseudoInstructions[name](tokens.sublist(1));
}




List<int> tdmap(int size, String s){
  var nChars = (size / 32).ceil();
  s = unquote(s);
  var map = new List.generate(nChars, (i) => 0, growable: true);
  map.addAll(s.codeUnits);
  //print(s);
  //print(map);
  return map;
}

unquote(String s) =>
    s.substring(1, s.length-1);
