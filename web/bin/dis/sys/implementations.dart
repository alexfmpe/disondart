part of dis;

DateTime get currentTime => new DateTime.now();
Duration get ellapsedTime => currentTime.difference(startTime);
var startTime = currentTime;


abstract class Builtin<F extends VMStruct>{
  F f;
  execute();
  call(VMPointer fp){
    this.f = fp.dereferenceAs(F);
    execute();
  }
}

class Sys_sleep extends Builtin<F_Sys_sleep>{
  execute(){
    if(f.period <= 0)
        return;
      var dur = new Duration(milliseconds: f.period);
      sleep(dur);
  }
}

class Sys_millisec extends Builtin<F_Sys_millisec>{
  execute(){
    f._ret.dereference = ellapsedTime.inMilliseconds;
  }
}

class Sys_print extends Builtin<F_Sys_print>{
  execute(){
    VMString str = f._s.dereference;
    String s = formatOutput(str.text, new VarArgs(f));
    print(s);

  }
}



//temp hack on constructor to allow FakeArgs
class VarArgs{
  Pointer<VMByte> p;
  VarArgs([VMStruct f]){
    if(f != null)
      p = new Pointer<VMByte>.typed(f._vargs);
  }

  unwrap(VMObject o){
    if(o is VMNumber)
      return o.read();
    if(o is VMString)
      return o.text;
  }

  //yet another temporary hack
  advance(Type t){
    VMObject o;
    if(isPrimitive(t)){
      o = p.dereferenceAs(t);
      p += sizeof(t);
      return unwrap(o);
    }
    VMPointer ptr = p.dereferenceAs(VMPointer);
    VMString str = ptr.dereferenceAs(VMString);
    return unwrap(str);
  }
}




/*
Sys_utfbytes(VMPointer fp){
  F_Sys_utfbytes f = fp.dereference;
  VMPointer p = f.buf;
  if(p.isNil)
    error(exBounds);
  VMArray a = p.dereference;
  if(f.n > a.length)
    error(exBounds);

  String s = decodeUtf8(a.sublist(0, f.n));
  f.ret = encodeUtf8(s).length;

}
*/