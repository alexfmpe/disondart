part of dis;

abstract class VMObject{
  int byteSize;
  int address;
  int poolAddress;
  Pool p;
  ByteData bd;

  VMObject([dynamic arg = null]){}

  VMObject.Field(VMComposite c){
    initField(c);
  }

  init([dynamic arg = null]){
    if(arg is int)
      initAlone(arg);
    if(arg is VMComposite)
      initField(arg);
  }

  initField(VMComposite c){
    initAlone(c.address + c.nextOffset);
    c.byteSize = c.newSize(this.byteSize);
  }

  initAlone(int addr){
    p = selectPool(addr);
    bd = p.memory.bd;
    address = addr;
    poolAddress = addr - p.offset;
  }

  Pointer get pointer =>
      new Pointer(address, runtimeType, 1);

}

abstract class VMPrimitive<P> extends VMObject{
  call([value]) => (value == null) ? read() : write(value);
  read();
  write(value);
  toString() => '@0x${toHex(address)} \t |$byteSize| \t ${this.runtimeType} \t 0x${toHex(read())}';
}

@proxy
abstract class VMComposite extends VMObject{
  int byteSize = 0;
  int newSize(int fieldSize);
  int get nextOffset;

  fieldWithOffset(int offset, Type t, [List extraArgs = emptyList]) =>
      view(t, [this.address + offset]..addAll(extraArgs));

  field(Type t, [List extraArgs = emptyList]){
    VMObject v = fieldWithOffset(nextOffset, t, extraArgs);
    byteSize = newSize(v.byteSize);
    return v;
  }

  /*
  fieldP(Type t, [int depth = 1]) =>
      field(VMPointer, [t, depth]);
  */
  /*
  fieldA(Type t, int elements) =>
      new Array.Field(this, t, elements);
      //field(Array, [t, elements]);
  */
  fields();

  init([address]){
    super.init(address);
    fields();
  }

  padding([int align = 16]) =>
      byteSize = alignTo(byteSize, align);


  noSuchMethod(Invocation i){
    if(! i.isAccessor)
      return super.noSuchMethod(i);

    InstanceMirror im = reflect(this);
    var str = MirrorSystem.getName(i.memberName);

    if(i.isSetter)
      str = str.substring(0, str.length-1);

    Symbol smb = MirrorSystem.getSymbol('_$str', im.type.owner);
    VMPrimitive p = im.getField(smb).reflectee;

    if(i.isGetter)
      return p.read();
    if(i.isSetter)
      return p.write(i.positionalArguments[0]);
  }
}

abstract class VMStruct extends VMComposite{
  int newSize(int fieldSize) => byteSize + fieldSize;
  int get nextOffset => byteSize;
}

abstract class VMUnion extends VMComposite{
  int newSize(int fieldSize) => max(byteSize, fieldSize);
  int get nextOffset => 0;
}

abstract class VMNumber<N extends num> extends VMPrimitive<N> with VMComparable{
  get reader;
  get writer;
  read()    => reader(poolAddress);
  write(v)  => writer(poolAddress, eval(v));

  void debug() => print(read());

  operator +(n) => read() + eval(n);
  operator -(n) => read() - eval(n);
  operator *(n) => read() * eval(n);
  operator /(n);

  operator <(n)  => read() < eval(n);
  operator ==(n) => read() == eval(n);

  operator -() => -read();

  raise(n) => pow(read(), eval(n));
}

abstract class VMInteger extends VMNumber{
  //write(v)  => super.write(toInt(v));
  operator /(n) => read() ~/  eval(n);
  operator %(n) => read()  %  eval(n);
  operator &(n) => read()  &  eval(n);
  operator |(n) => read()  |  eval(n);
  operator ^(n) => read()  ^  eval(n);

  operator <<(n) => read() << eval(n);
  operator >>(n) => read() >> eval(n);
}

abstract class VMFloat extends VMNumber{
  write(v)  => super.write(toFloat(v));
  operator /(n) => read() / eval(n);
}

eval(n) =>
    (n is VMPrimitive)  ? n.read() :
    (n is PointerMixin) ? n.value  :
    (n is String)       ? toInt(n) :
    n;

