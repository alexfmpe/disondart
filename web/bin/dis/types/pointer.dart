part of dis;

abstract class PointerMixin<T>{
  int get value;
  set value(n);

  int  depth;
  Type type;
  int  get pointedSize => 
      sizeof(type);

  bool get isNil =>
      this == H;

  operator +(n) => 
      value + eval(n) * pointedSize;
  
  operator -(n) =>
      (n is PointerMixin) ? (this.value - n.value) ~/ pointedSize
                          : this + (-n);

  dereferenceAs(Type t) =>
      (new Pointer(value, t).dereference);

  dereferenceAt(x) =>
      (depth == 1)  ? view(type,      [eval(x)])
                    : view(VMPointer, [eval(x), depth-1]);

  T operator [](int offset) =>
    dereferenceAt(this + offset);
  

  operator []=(int offset, value) =>
      dereferenceAt(this + offset).write(value);

  T get dereference =>   
      this[0];
  
  
  set dereference(x) =>
      this[0] = x;

  Pointer cast(Type t, [int depth = 1]) =>
      new Pointer(value, t, depth);


  pointTo(v)  =>
      (v is int)      ? value = v           :
      (v is VMObject) ? pointTo(v.address)  :
      throw 'lolwut';
}


class Pointer<T extends VMObject> extends Object with PointerMixin<T>, VMComparable{
  int value;
  
  Pointer(v, type, [depth = 1]){
    pointTo(v);
    this.type  = type;
    this.depth = depth;
  }
  
  Pointer.typed(v){
    pointTo(v);
    this.type = T;
    this.depth = (v is PointerMixin) ? v.depth + 1 : 1;     
  }
  
  Pointer operator +(int n) => new Pointer(value + n*pointedSize, type, depth);
  operator <(n)  => this.value < eval(n);
  operator ==(n) => this.value == eval(n);
}

class VMPointer<T extends VMObject> extends VMUWord with PointerMixin<T>{
  VMPointer(){}
  
  VMPointer.field(VMComposite c){
      this.type = T;
      this.depth = (c is PointerMixin) ? c.depth + 1 : 1;
      initField(c);
    }
  
  init([address, type = VMByte, depth = 1]){
    //error("VMPointer init should not happen");
    
    super.init(address);
    this.type = type;
    this.depth = depth;
  }
  
  Pointer get pointer =>
      new Pointer(address, type, depth + 1);

  int get value => read();
  set value(v)  => write(v);
}
