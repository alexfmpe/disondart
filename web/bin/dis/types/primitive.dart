part of dis;

// probably useless; revisit when generics actually work properly
class VMVoid extends VMByte{}

/* Numbers */
class VMByte extends VMInteger{
  get byteSize  => 1;
  get reader    => bd.getUint8;
  get writer    => bd.setUint8;
}

class VMChar extends VMInteger{
  get byteSize  => 1;
  get reader    => bd.getInt8;
  get writer    => bd.setInt8;
}

class VMUShort extends VMInteger{
  get byteSize  => 2;
  get reader    => bd.getUint16;
  get writer    => bd.setUint16;
}

class VMShort extends VMInteger{
  get byteSize  => 2;
  get reader    => bd.getInt16;
  get writer    => bd.setInt16;
}

class VMWord  extends VMInteger{
  get byteSize  => 4;
  get reader    => bd.getInt32;
  get writer    => bd.setInt32;
}

class VMUWord  extends VMInteger{
  get byteSize  => 4;
  get reader    => bd.getUint32;
  get writer    => bd.setUint32;
}

class VMLong  extends VMInteger{
  get byteSize  => 8;
  get reader    => bd.getInt64;
  get writer    => bd.setInt64;
}

class VMULong  extends VMInteger{
  get byteSize  => 8;
  get reader    => bd.getUint64;
  get writer    => bd.setUint64;
}


class VMSingle extends VMFloat{
  get byteSize  => 4;
  get reader    => bd.getFloat32;
  get writer    => bd.setFloat32;
}
class VMDouble extends VMFloat{
  get byteSize  => 8;
  get reader    => bd.getFloat64;
  get writer    => bd.setFloat64;
}

class VMRune extends VMWord{}
