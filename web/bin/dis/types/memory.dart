part of dis;

/*Memory*/
/*
class VMMemory extends VMPrimitive{
  int byteSize;
  init(address, [region, byteSize]){
    this.byteSize = byteSize;
    super.init(address, region);
  }

  read() => toInt(region.sublist(address, address + byteSize));
  write(value) => region.replaceRange(address, address + byteSize, toBytes(value));
}
*/
//class VMTypedMemory extends VMPrimitive{}

abstract class ArrayStupidDartMixinDammitAll extends VMObject{}


class Array<E extends VMObject> extends ArrayStupidDartMixinDammitAll with ListMixin<E> {  
  int length;
    
  Array(VMComposite c, this.length){
    byteSize = sizeof(E) * length;
    initField(c);
  }
  
  Pointer get pointer => new Pointer<E>.typed(address);

  operator[] (index)        => pointer[eval(index)];
  operator[]=(index, value) => pointer[eval(index)] = value;
}
