part of dis;


VMObject view(Type t, [List args = emptyList]){
  ClassMirror cm = reflectClass(t);
  InstanceMirror im = cm.newInstance(new Symbol(''), []);
  VMObject o = im.reflectee;
  initType(o, args);
  return o;
}

initType(VMObject o, [List args = emptyList]){
  if(args != null && args.length > 0)
    Function.apply(o.init, args);
}

/*
field(Type t, [List extraArgs = emptyList]){
  VMObject v = fieldWithOffset(nextOffset, t, extraArgs);
  byteSize = newSize(v.byteSize);
  return v;
}
*/

class Field<O extends VMObject>{
  factory Field(VMComposite c, [List args]){
  }
}

class View<O extends VMObject>{
  factory View([List args]){
    var obj = view(O, [args]);
    return obj;
  }
}
