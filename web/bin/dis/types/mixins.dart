part of dis;

abstract class VMSequence{
  int get length;
}

abstract class VMComparable{
  operator ==(n);
  operator < (n);
  operator <=(n) => (this < n) || (this == n);
  operator >=(n) => !(this < n);
  operator > (n) => !(this <= n);
  compareTo(n) =>
      (this < n) ? -1 :
      (this > n) ?  1 :
      0;
}

abstract class VMTypeContainer{
  int get t;
  set t(int x);
  TypeDescriptor get type{
    return typeWithID(t);
  }

  set type(TypeDescriptor td){
      return typeID(td);
  }
}


abstract class PseudoInt{
  int toInt();
  PseudoInt createPseudo(int);

  static unwrap(x) =>
      (x is int)        ? x           :
      (x is PseudoInt)  ? x.toInt()   :
      throw conversionException;

  operator  -()  => operate(mul,-1);

  operator  +(p) => operate(add, p);
  operator  -(p) => operate(sub, p);
  operator  *(p) => operate(mul, p);

  operator  %(p) => operate(mod, p);
  operator  /(p) => operate(div, p);
  operator ~/(p) => operate(idiv,p);

  operate(Function f, that)   =>
      operation(f, this, that);

  operation(Function f, a, b) =>
      createPseudo(f(unwrap(a), unwrap(b)));

  add(a,b)  => a+b;
  sub(a,b)  => a-b;
  mul(a,b)  => a*b;

  mod(a,b)  => a%b;
  div(a,b)  => a/b;
  idiv(a,b) =>a~/b;

}

