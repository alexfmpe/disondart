part of dis;

class VMAdr extends VMUnion{
  VMWord _imm, _ind;
  VMPointer _ins;
  VMAdr_i i;
  fields(){
    _imm	= field(VMWord);
    _ind  = field(VMWord);
    _ins  = new VMPointer<VMInst>.field(this);
    i     = field(VMAdr_i);
  }
}
//new VMPointer<VMInst>.field(this);
//new Field<VMPointer<VMInst>>
//new FieldP<VMInst>
class VMAdr_i extends VMStruct{
  VMUShort _f,_s;
  fields(){
    _f = field(VMUShort);
    _s = field(VMUShort);
  }
}


class VMArray extends VMStruct with VMTypeContainer, ListMixin{
  VMWord _len, _t;
  VMPointer _root, _data;

  fields(){
    _len  = field(VMWord);
    _t    = field(VMWord);
    _root = new VMPointer<VMArray>.field(this);
    _data = new VMPointer<VMByte>.field(this);
  }

  bool get isRoot =>
      root.isNil;

  get length => len;
  operator[]  (int index)         => _data[index];
  operator[]= (int index, value)  => _data[index] = value;
}



/* according to include/interp.h, must be a multiple of 8 bytes */
class VMHeap extends VMStruct with VMTypeContainer{
  VMWord _color, _t;
  VMULong _ref, _href;
  fields(){
    _color  = field(VMWord);
    _ref    = field(VMULong);
    _t      = field(VMWord);
    //_href   = field(VMULong);
    padding(8);
  }
}

class VMChannel extends VMStruct{}

class VMFrame extends VMStruct with VMTypeContainer{
  VMPointer _lr, _fp, _mr;
  VMWord _t;
  fields(){
    _lr = new VMPointer<VMInst>.field(this);
    _fp = new VMPointer<VMByte>.field(this);
    _mr = new VMPointer<VMModlink>.field(this);
    _t  = field(VMWord);
  }
}

class VMImport extends VMStruct{
  VMWord _sig;
  VMPointer _name;
  fields(){
    _sig  = field(VMWord);
    _name = new VMPointer<VMString>.field(this);
  }
}

class VMImports extends VMStruct{
  VMWord _nEntries;
  Array<VMImport> imports;
  fields(){
    _nEntries = field(VMWord);
    imports   = new Array<VMImport>(this, nEntries);
  }
}


class VMInst extends VMStruct{
  VMByte _op, _add;
  VMUShort _reg;
  VMAdr s,d;

  fields(){
    _op	  = field(VMByte);
    _add	= field(VMByte);
    _reg	= field(VMUShort);
    s     = field(VMAdr);
    d     = field(VMAdr);
    padding();
  }
}


class VMList extends VMStruct with VMSequence, VMTypeContainer{
  VMPointer _tail;
  VMWord _t;
  Array<VMByte> data;
  fields(){
    _tail = new VMPointer<VMList>.field(this);
    _t    = field(VMWord);
    data  = new Array<VMByte>(this, 0);
  }
  get length => pointer.isNil ? 0 : 1 + this.tail.dereference.length;
}


class VMModlink extends VMStruct{
  VMWord _compiled, _nlinks;
  VMPointer<VMByte> _MP, _data;
  VMPointer<VMInst> _prog;
  VMPointer<VMModule> _m;

  fields(){
    _MP       = new VMPointer<VMByte>.field(this);
    _m        = new VMPointer<VMModule>.field(this);
    _compiled = field(VMWord);
    _prog     = new VMPointer<VMInst>.field(this);
    //_type     = fieldp(VM)
    _data     = new VMPointer<VMByte>.field(this);
    _nlinks   = field(VMWord);
    padding();
  }
}

class VMModule extends VMStruct{}
class VMModLink extends VMStruct{}



class VMReg extends VMStruct{
  Type midType, srcType, dstType;
  VMPointer<VMModlink> _M;
  VMPointer<VMInst> _PC;
  VMPointer<VMByte> _MP,_FP,_SP,_TS,_EX,_xpc;
  VMPointer<VMVoid> _s, _d, _m;

  VMWord _IC, _t, _st, _dt;
  Pointer entry;

  fields(){
    _PC = new VMPointer<VMInst>.field(this);
    _MP = new VMPointer<VMByte>.field(this);
    _FP = new VMPointer<VMByte>.field(this);
    _SP = new VMPointer<VMByte>.field(this);

    _TS = new VMPointer<VMByte>.field(this);
    _EX = new VMPointer<VMByte>.field(this);
    _M  = new VMPointer<VMModlink>.field(this);
    _IC = field(VMWord);

    _s  = new VMPointer<VMVoid>.field(this);
    _m  = new VMPointer<VMVoid>.field(this);
    _d  = new VMPointer<VMVoid>.field(this);
    _t   = field(VMWord);

    padding();
}

  getter(Type t, int adr){
    return
      isPrimitive(t) ? view(t, [adr]):
      isReference(t) ? view(VMPointer, [adr, t, 1]):
      throw 'lolwut';
  }

  setter(VMPrimitive p, x) =>
      p.write(eval(x));

  get mid => getter(midType, m);
  get src => getter(srcType, s);
  get dst => getter(dstType, d);

  set mid(v) => setter(mid, v);
  set src(v) => setter(src, v);
  set dst(v) => setter(dst, v);

  toString(){
    return '[m s d]\t|' + toHex([m,s,d]).join('|\t|') + '|';
  }
}




typedef VMString StringCreator(int length);
class VMString extends VMStruct with VMComparable, ListMixin<int>{
  VMString_data data;
  VMWord _len, _max;
  VMPointer _tmp;
  fields(){
    _len  = field(VMWord);
    _max  = field(VMWord);
    _tmp  = new VMPointer<VMChar>.field(this);
    data  = field(VMString_data, [length]);
  }

  int get length =>
      pointer.isNil ? 0 : len.abs();

  get isAscii => !isRunes;
  get isRunes => (len < 0);

  Array<VMByte> get Sascii  => data.ascii;
  Array<VMRune> get Srune   => data.runes;

  VMString asAscii() => (isAscii ? this : toAscii);
  VMString asRunes() => (isRunes ? this : toRunes);

  VMString toAscii() => copyTo(newAscii);
  VMString toRunes() => copyTo(newRunes);

  VMString copyTo(StringCreator sc){
    VMString ns = sc(length);
    for(int i = 0; i < length; i++)
      ns[i] = this[i];
    return ns;
  }

  Array<VMPrimitive> get _data =>
      isAscii ? Sascii : Srune;

  get text =>
      new String.fromCharCodes(_data.map(eval));

  set text(String s){
    Runes runes = s.runes;
    int m = min(runes.length, this.max);
    range(0,m).forEach((i) =>
        _data[i] = runes.elementAt(i));
    if(m > len)
      len = m;
  }

  operator[] (int index)      => _data[index];
  operator[]=(int index, val) => _data[index] = val;

  operator ==(VMString s) =>  stringcmp(this,s) == 0;
  operator < (VMString s) =>  stringcmp(this,s)  < 0;
}

class VMString_data extends VMUnion{
  Array<VMByte> ascii;
  Array<VMRune> runes;
  int length;
  init([int address, int length]){
    this.length = length;
    super.init(address);
  }
  fields(){
    ascii = new Array<VMByte>(this, length);
    runes = new Array<VMRune>(this, length);
  }
}



class VMStkext extends VMUnion{
  Array<VMByte> stack;
  VMStkext_reg reg;
  fields(){
    stack = new Array<VMByte>(this, 0);
    reg = field(VMStkext_reg);
  }
}

////must fix typecontainer nonsense
class VMStkext_reg extends VMStruct with VMTypeContainer{
  get t => TR;
  set t(x) => TR = x;

  VMWord _TR;
  VMPointer<VMByte> _SP, _TS, _EX;
  VMStkext_reg_tos tos;
  fields(){
    _TR  = field(VMWord);
    _SP  = new VMPointer.field(this);
    _TS  = new VMPointer.field(this);
    _EX  = new VMPointer.field(this);
    tos = field(VMStkext_reg_tos);
  }
}
class VMStkext_reg_tos extends VMUnion{
  Array<VMByte> fu;
  Array<VMFrame> fr;
  fields(){
    fu = new Array<VMByte>(this, 0);
    fr = new Array<VMFrame>(this, 0);
  }
}


class VMInit extends VMStruct{
  VMPointer<VMFrame> _fp;
  VMFrame f;
  fields(){
    _fp = new VMPointer<VMFrame>.field(this);
    f  = field(VMFrame);
  }
}


isPrimitive(Type t) =>
    view(t, [0]) is VMPrimitive;

isReference(Type t) =>
    view(t, [0]) is VMComposite;
