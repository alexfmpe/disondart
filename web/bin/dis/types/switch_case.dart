part of dis;

abstract class VMSwitch<C extends VMCase> extends VMStruct{
  VMWord _n, _def;
  Array<C> cases;

  fields(){
    _n    = field(VMWord);
    padding(sizeof(C));
    cases = new Array<C>(this, n);
    _def  = field(VMWord);
  }
}


class VMSwitchW extends VMSwitch<VMCaseW>{}
class VMSwitchL extends VMSwitch<VMCaseL>{}
class VMSwitchC extends VMSwitch<VMCaseC>{}


abstract class VMCase<T> extends VMStruct{
  T _low, _high;
  VMWord _pc;
  fields(){
    _low  = field(T);
    _high = field(T);
    _pc   = field(VMWord);
    padding(3*sizeof(T));
  }
}

class VMCaseW extends VMCase<VMWord>{}
class VMCaseL extends VMCase<VMLong>{}
class VMCaseC extends VMCase<VMPointer<VMString>>{}

