part of dis;

terminate()
  => error();

branch(VMWord w) =>
    R.PC = R.entry + w();

goto(VMWord s, VMWord d){
  var p = new Pointer<VMWord>.typed(d());
  branch(p[s()]);
}


raise(PointerMixin s){
  error(exTest);  //mad temp hack

  if(s.isNil)
    error(exNilref);
  //VMProg p = currentRun;
  //p.exval = s;
  VMHeap h = D2H(s);
  h.ref++;

  VMString str;
  if(h.type == Tstring)
    str = s.cast(VMString).dereference;
  else{
    s = s.cast(VMString, 2).dereference;
    str = s.dereference;
  }

  error(str.text);
}

casew(VMWord s, VMWord d)     =>  switchCase(s, d, VMSwitchW, seekComparable);
casel(VMLong s, VMWord d)     =>  switchCase(s, d, VMSwitchL, seekComparable);
casec(VMPointer s, VMWord d)  =>  switchCase(s, d, VMSwitchC, seekString);



switchCase(value, VMWord d, Type type, Function seeker){
  VMWord pc;
  VMSwitch sw = view(type, [d]);
  int index = binarySearch(sw.cases, value, seeker);
  if(index == -1)
    pc = sw.def;
  else{
    VMCase c = sw.cases[index];
    pc = c.pc;
  }
  branch(pc);
}

int seekComparable(VMCase c, VMComparable n) =>
    n <  c.low  ? -1 :
    n >= c.high ?  1 :
    0;

int seekString(VMCaseC c, VMPointer<VMString> p){
  Pointer<VMString>
    pl  = c.low,
    ph  = c.high;

  VMString  low  = pl.dereference,
            high = ph.dereference,
            s    = p.dereference;
  
  return  ph.isNil  ? s.compareTo(low) :
          s <  low  ? -1 :
          s >= high ?  1 :
          0;
}


