part of dis;

class Specification{
  String name;
  Function implementation;
  List<List> parameters;
  Specification(this.implementation, this.parameters);

  String operandName(int n) => parameters[n][0];
  int operandIndex(String s) => indexOf(parameters, (p) => p.first == s);



  Type operandType(x) =>
      (x == null)   ? null:
      (x is int)    ? ((x < 0) ? null : parameters[x][1]) :
      (x is String) ? operandType(operandIndex(x)):
      throw 'lolwut';
}

Specification spec(String inst) =>
    specs[inst];