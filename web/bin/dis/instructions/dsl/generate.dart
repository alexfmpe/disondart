part of dis;

getTypes(String opcode){
  RegExp exp = new RegExp(r'[A-Z]{3}');
  String s = exp.stringMatch(opcode);
  if(s != null)
    return complexTypes[s];

  exp = new RegExp(r'\[[^\]]+\]');
  s = exp.stringMatch(opcode);
  if(s == null)
    return [];

  return s.substring(1, s.length-1).split(',');
}

String getName(String opcode){
  RegExp exp = new RegExp(r'[a-z]+');
  return exp.stringMatch(opcode);
}

List<Parameter> getParameters(String source){
  if(source.trim() == "")
    return [];
  List<String> l = source.split(new RegExp(r'\s+'));
  return l.map(buildParameter).toList();
}

buildParameter(String source){
  List<String> l = source.split('|');
  if(l.length == 1)
    return new Parameter(l[0]);
  else
    return new Parameter(l[0], l[1]);
}

List<DSLInstruction> buildInstructions(String spec){
  List<String> blocks = spec.split("::");
  blocks = blocks.map((String s) => s.trim()).toList();

  String basename = getName(blocks[0]);
  List<String> types = getTypes(blocks[0]);
  String signature = blocks[1];
  String code = blocks[2];
  List<DSLInstruction> l = [];

  if(types.length == 0)
    l.add(new DSLInstruction(basename, signature, code, ''));
  else
    types.forEach((String t){
      l.add(new DSLInstruction(basename + t.toLowerCase(),  signature, code, t));
      });
  return l;
}


class Parameter{
  Parameter(this.name, [String s]){
    type = typeShortcut(s);
  }
  String name;
  Type type;
  int orderNumber;
  //String toSpec() => "'$name' : [$orderNumber, $type],";
  String toSpec() => "['$name', $type],";
  String toString() => toSpec();
}

class DSLInstruction{
  String name;
  String signature;
  String code;
  Type defaultType;
  Map<String, Parameter> parameters = {};

  DSLInstruction(this.name, signature, this.code, shortcut){
    List<Parameter> l = getParameters(signature);
    for(int i = 0; i < l.length; i++){
      Parameter p = l[i];
      p.orderNumber = i;
      if(p.type == null)
        p.type = typeShortcut(shortcut);

      parameters[p.name] = p;
    }
  }

  String get implementation{
    String signature = "_$name(VMReg R)";
    String body = "  $code;";
    String declarations = '';
    /*
    String declarations = spec(name).parameters.map((l){
      String opName = l[0];
      Type opType = l[1];
      return '  $opType $opName = buildType($opType, [R.$opName(), data]);';
    }).join('\n');
    return '$signature {\n$declarations\n$body\n}\n';
    */

    RegExp arguments = new RegExp(r'\b[sdm]\b');
    asAccessor(Match m) =>
        'R.' + operands[m[0]].accessor;

    RegExp registers = new RegExp(r'\bPC\b');
    asField(Match m) =>
        'R.' + m[0];

    RegExp arrow = new RegExp(r'->');
    asArrowAccess(Match m) =>
        '.dereference.';

    body = body.replaceAllMapped(arguments, asAccessor);
    body = body.replaceAllMapped(registers, asField);
    body = body.replaceAllMapped(arrow, asArrowAccess);
    return '$signature {\n$body\n}\n';
  }

  String get specification{
    String spec = '  "$name" : new Specification(_$name, [\n';
    parameters.values.forEach((v)=> spec += '    ' + v.toSpec() + '\n');
    return spec + '  ]),';
  }
}

class Generator{
  String name, start, end;
  Function extractor;
  StringBuffer sb = new StringBuffer('part of dis;\n\n');
  Generator(this.name, this.extractor, [this.start = '', this.end = '']){
    sb.write(start);
  }
  extract(DSLInstruction i) =>
      sb.writeln(extractor(i));
  toString() =>
      sb..write(end)..toString();
}

generateFile(String name, Object data) =>
    writeFile('instructions/generated/' + name, data.toString());

generate(List<String> DSL){
  File f;
  IOSink s;
  List<Generator> gens =
  [
    new Generator('implementations',
                  (DSLInstruction i) => i.implementation),
    new Generator('specifications',
                  (DSLInstruction i) =>
                      i.specification,
                      'Map<String, Specification> specs = {\n',
                      '};'),
  ];

  List l = DSL.expand((String s) => buildInstructions(s)).toList();

  gens.forEach((Generator g){
    l.forEach((DSLInstruction i){
      g.extract(i);
    });

    generateFile('${g.name}.dart', g);
  });

  String separator = '/////////////////////////////';
}

