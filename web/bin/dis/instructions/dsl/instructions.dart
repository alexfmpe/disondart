part of dis;

Function typeShortcut = doubleMap(typeShortcuts);
Map<String, Type> typeShortcuts = {
  'b' : VMByte,
  's' : VMShort,
  'w' : VMWord,
  'uw': VMUWord,
  'l' : VMLong,
  'ul': VMULong,
  'r' : VMSingle,
  'f' : VMDouble,

  'p' : VMPointer,
//  'm' : VMMemory,
//  'mp': VMTypedMemory,

  'C' : VMString,
  'A' : VMArray,
  'L' : VMList,
  'X' : VMChannel,
  'I' : VMInst,
  'F' : VMFrame,
};

Map<String, List<Type>> complexTypes =
{
  'SHR' : ['s', 'r'],
  'FLT' : ['f', 'r'],
  'UNS' : ['uw','ul'],
  'INT' : ['b', 'w', 'l'],
  'NUM' : ['b', 'w', 'l', 'f'],
  'ATM' : ['b', 'w', 'l', 'f', 'p'],
  'PRM' : ['b', 'w', 'l', 'f', 'p', 'm', 'mp'],
  'CMP' : ['b', 'w', 'l', 'f', 'C'],
  'SEQ' : ['C', 'L', 'A'],
  'MEM' : ['m', 'mp'],
};


List<String> ISA_DSL = lineSplit(
'''
  negf        :: s|f d|f      :: d = -s

  add[NUM]    :: s m d        :: d = m + s
  div[NUM]    :: s m d        :: d = m / s
  mul[NUM]    :: s m d        :: d = m * s  
  sub[NUM]    :: s m d        :: d = m - s
  mod[INT]    :: s m d        :: d = m % s

  and[INT]    :: s m d        :: d = m & s  
   or[INT]    :: s m d        :: d = m | s
  xor[INT]    :: s m d        :: d = m ^ s

  shl[INT]    :: s|w m d      :: d = m << s
  shr[INT]    :: s|w m d      :: d = m >> s

  lsrw        :: s|w m|uw d|w :: d = m >> s
  lsrl        :: s|w m|ul d|w :: d = m >> s

  exp[f,w,l]  :: s|w m d      :: d = m.raise(s)


  beq[CMP]    :: s m d|w      :: if(s == m) branch(d)
  bne[CMP]    :: s m d|w      :: if(s != m) branch(d)
  bge[CMP]    :: s m d|w      :: if(s >= m) branch(d)
  bgt[CMP]    :: s m d|w      :: if(s >  m) branch(d)
  ble[CMP]    :: s m d|w      :: if(s <= m) branch(d)
  blt[CMP]    :: s m d|w      :: if(s <  m) branch(d)

  cons[NUM]   :: s   d|L     :: consnum(s,d)
  consptr     :: s|p d|L     :: consptr(s,d)
  consm       :: s|p m|w d|L :: consmm(s,d,m)
  consmp      :: s|p m|w d|L :: consmp(s,d,m)
  
  head[NUM]   :: s|L d        :: headnum(s,d)
  headptr     :: s|L d|p      :: headptr(s,d)
  headm       :: s|L m|w d|p  :: headmm(s,d,m)
  headmp      :: s|L m|w d|p  :: headmp(s,d,m)

  tail        :: s|L d|L      :: tail(s,d)

  newa        :: s|w m|w d|A  :: newArray(s,m,d)
  newaz       :: s|w m|w d|A  :: newArrayZ(s,m,d)
  slicea      :: s|w m|w d|A  :: slice(s,m,d)
  slicela     :: s|A m|w d|A  :: sliceAssign(s,m,d)

  indb        :: m|w s|A d|w  :: m = indb(s,d)
  indw        :: m|w s|A d|w  :: m = indw(s,d)
  indl        :: m|w s|A d|w  :: m = indl(s,d)
  indf        :: m|w s|A d|w  :: m = indf(s,d)
  indx        :: m|w s|A d|w  :: m = indx(s,d)

  jmp         :: d|w          :: branch(d)
  goto        :: s|w d|w      :: goto(s,d)

  frame       :: s|w d|F      :: frame(s,d) 
  mframe      :: s|p m|w d|F  :: mframe(s,m,d)

  call        :: s|F d|w      :: call(s,d)
  mcall       :: s|F m|w d|p  :: mcall(s,m,d)

  spawn       :: s|F d|w      :: spawn(s,d)

  ret         ::              :: ret()
  exit        ::              :: terminate()
  raise       :: s|p          :: raise(s)

  

  lea         :: s|w d|w      :: d = s.address
  len[SEQ]    :: s d|w        :: d = s->length
  
  mov[NUM]    :: s d          :: d = s
  movp        :: s|p d|p      :: movePointer(d,s)
  movm        :: s|p m|w d|p  :: movm(d,s,m)
  movmp       :: s|p m|w d|p  :: movmp(d,s,m)  

  tcmp        :: s|p d|p      :: typeCompare(s,d)

  new         :: s|w d|p      :: newObject(s,d);
  newz        :: s|w d|p      :: newObjectZ(s,d);

  cvtf[l,w]   :: s|f d        :: d = s
  cvtl[f,w]   :: s|l d        :: d = s
  cvtw[l,f]   :: s|w d        :: d = s
  
  cvtbw       :: s|b d|w      :: d = s
  cvtwb       :: s|w d|b      :: d = s
  cvtsw       :: s|s d|w      :: d = s
  cvtws       :: s|w d|s      :: d = s
  cvtrf       :: s|r d|f      :: d = s
  cvtfr       :: s|f d|r      :: d = s

  cvtcf       :: s|C d|f      :: d = c2f(s)
  cvtcw       :: s|C d|w      :: d = c2i(s)
  cvtcl       :: s|C d|l      :: d = c2l(s)
  
  cvtfc       :: s|f d|C      :: f2c(s,d)
  cvtwc       :: s|w d|C      :: i2c(s,d)
  cvtlc       :: s|l d|C      :: l2c(s,d)
  
  cvtac       :: s|p d|p      :: cvtac(s,d)
  cvtca       :: s|p d|p      :: cvtca(s,d)

''');

