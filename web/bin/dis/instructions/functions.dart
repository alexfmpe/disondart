part of dis;

call(VMPointer s, VMWord d){
  VMFrame f = s.dereference;
  f.lr = R.PC;
  f.fp = R.FP;
  R.FP = s;
  branch(d);
}

//hardcoded to sys mod for now
mcall(VMPointer s, VMWord m, VMPointer d){
  VMFrame f = s.dereference;
  var r = findRuntab(m);

  f.lr = R.PC;
  f.fp = R.FP;
  R.FP = s;
  r.fn(s);
  R.SP = R.FP;
  R.FP = f.fp;

//  ret();
}

ret(){
  VMFrame f = R._FP.dereferenceAs(VMFrame);

  if(f.fp == 0){
    R.FP = f.address;
    R.IC = 0;

    currun.state = ProgState.Pexiting;
    return;
  }
  R.FP = f.fp;
  R.SP = f.address;
  R.PC = f.lr;

  //var t = typeWithID(f.t);
  //freePointers(t, f.pointer);
}


