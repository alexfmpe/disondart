part of dis;

VMString newString(int length, [int charSize = 1]){
  var size = length*charSize;
  VMHeap h = heap(Tstring, extra:size);
  VMString s = H2D(VMString, h);
  s.tmp = nil;
  s.len = length;
  s.max = length ~/ charSize;
  //change when gc blocks are properly implemented
  return s;
}

VMString newAscii(int nb) =>
  newString(nb, 1);

VMString newRunes(int nr) =>
  newString(nr.abs(), sizeof(VMRune))
    ..len *= -1;

sliceString(VMWord s, VMWord m, VMPointer d){
  Pointer p = slicer(s(), m(), d);
  overwritePointer(d, p);
}

PointerMixin slicer(int start, int stop, VMPointer ds){
  if(ds.isNil){
    if(start == 0 && stop == 0)
      return H;
    else
      error(exBounds);
  }

  int length = stop - start;
  VMString str = ds.dereference;
  if(length < 0 || stop > str.length)
    error(exBounds);
  if(length == 0)
    return H;

  VMString slice;
  if(str.isAscii){
    slice = newAscii(length);
    memmove(slice.Sascii.pointer, str.Sascii.pointer + start, length);
  }else{
    slice = newRunes(length);
    memmove(slice.Srune.pointer, str.Srune.pointer + start, length);
  }

  return slice.pointer;
}

indc(VMPointer s, VMWord m){
  if(s.isNil)
    error(exNilref);
  VMString str = s.dereference;
  if(m > str.length)
    error(exBounds);
  return str[m()];
}

int strpcmp(PointerMixin a, PointerMixin b){
  if(a.isNil && b.isNil)
    return 0;
  if(a.isNil)
    return -strpcmp(b,a);

  VMString strA = a.dereference;
  if(b.isNil)
    return strA.length;
  else
    return stringcmp(strA, b.dereference);
}

int stringcmp(VMString a, VMString b){
  Array<VMPrimitive>
    _a = a._data,
    _b = b._data;
  int i, n = min(a.length, b.length);
  for(i = 0; i < n; i++)
    if(_a[i] != _b[i])
      return _a[i] - _b[i];

  return a.length - b.length;
}

Pointer stringdup(VMString s) =>
    str2vmstr(s.text);

bool disposable(VMPointer p) =>
    D2H(p).ref <= 1;

addString(VMPointer a, VMPointer b, bool append){
  if(a.isNil){
    if(b.isNil)
      return H;
    else
      return stringdup(b.dereference);
  }
  if(!disposable(a))
    append = false;
  if(b.isNil){
    if(append)
      return a;
    else
      return stringdup(a.dereference);
  }

  VMString  s1 = a.dereference,
            s2 = b.dereference,
            ns;

  int l1 = s1.length,
      l2 = s2.length,
      l  = l1 + l2;

  //TODO: true-branch breaks if no space is available in s1
  if(s1.isAscii && s2.isRunes)
    ns = s1.toRunes();
  else{
    Pointer p = (append && l <= s1.max) ? s1.pointer : stringdup(s1);
    ns = p.dereference;
  }

  for(int i = 0; i < l2; i++)
    ns[l1+i] = s2[i];
  return ns;
}

addc(VMPointer s, VMPointer m, VMPointer d){
  Pointer p = addString(s, m, s.address == d.address);
  if(p != d)
    overwritePointer(d, p);
}

insc(VMWord s, VMWord m, VMPointer d){
  //TODO: Can (should?) be optimized when in-place insertion is possible
  VMString str;
  String text = '';
  int char = s();
  int index = m();

  if(!d.isNil){
    str = d.dereference;
    text = str.text;
  }

  if(index > text.runes.length)
    error(exBounds);

  text = text.substring(0, index) + '$char' + text.substring(index);
  Pointer p = str2vmstr(text);
  overwritePointer(d, p);
}
