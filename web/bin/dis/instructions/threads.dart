part of dis;

const int PQUANTA = 2048;

spawn(VMPointer s, VMWord d){
  Prog p = newprog(currun);
  p.R.entry = R.entry;
  p.R.PC = R.entry + d();

  //debug(p.R.PC);
  newstack(p);
  //unframe();
}

newstack(Prog p){


  int hack = 0 + 128; //temp debug
  int ss = hack + 256; //initial stack size

  VMFrame f = R._s.dereferenceAs(VMFrame);
  TypeDescriptor t = f.type;
  f.lr = nil;
  f.mr = nil;
  f.fp = nil;
  int len = max(ss, hack + t.size + sizeof(VMStkext));
  VMStkext ns = malloc(VMStkext, len).dereference;

  ns.reg.TR = typeID(t);
  ns.reg.SP = nil;
  ns.reg.TS = nil;
  ns.reg.EX = nil;

  p.R.EX = ns.stack.pointer;
  p.R.TS = ns.stack.pointer + len;
  p.R.FP = ns.reg.tos.fu.pointer;
  p.R.SP = ns.reg.tos.fu.pointer + hack + t.size;


  memmove(new Pointer<VMByte>.typed(p.R.FP), new Pointer<VMByte>.typed(f), t.size);
  f = p.R._FP.dereferenceAs(VMFrame);
  f.t = nil;
}

unframe(){
  VMFrame f = R._FP.dereferenceAs(VMFrame);
  TypeDescriptor t = f.type;
  R.SP = R.FP + t.size;
  f = R._s.dereferenceAs(VMFrame);
}

/*
void
unframe(void)
{
  Type *t;
  Frame *f;
  Stkext *sx;

  f = (Frame*)R.FP;
  t = f->t;
  if(t == nil)
    t = SEXTYPE(f)->reg.TR;

  R.SP = R.FP+t->size;

  f = T(s);
  if(f->t == nil) {
    sx = SEXTYPE(f);
    R.TS = sx->reg.TS;
    R.EX = sx->reg.EX;
    free(sx);
  }
}
*/

/*

void
extend(void)
{
  int l;
  Type *t;
  Frame *f;
  Stkext *ns;

  t = R.s;
  l = R.M->m->ss;
  /* 16 bytes for Stkext record keeping */
  if(l < t->size+16)
    l = 2*t->size+16;
  ns = mallocz(l, 0);
  if(ns == nil)
    error(exNomem);

  ns->reg.TR = t;
  ns->reg.SP = R.SP;
  ns->reg.TS = R.TS;
  ns->reg.EX = R.EX;
  f = ns->reg.tos.fr;
  f->t  = nil;
  f->mr = nil;
  R.s = f;
  R.EX = ns->stack;
  R.TS = ns->stack + l;
  R.SP = ns->reg.tos.fu + t->size;

  if (t->np)
    initmem(t, f);
}

void
unextend(Frame *f)
{
  Stkext *sx;
  Type *t;

  sx = SEXTYPE(f);
  R.SP = sx->reg.SP;
  R.TS = sx->reg.TS;
  R.EX = sx->reg.EX;
  t = sx->reg.TR;
  if (t->np)
    freeptrs(f, t);
  free(sx);
}
*/