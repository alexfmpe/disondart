part of dis;

newObject(VMWord s, VMPointer d, {clear:false}){
  TypeDescriptor t = typeWithID(s());
  VMHeap h = heap(t);
  VMObject o = H2D(VMObject, h);
  overwritePointer(d, o.pointer);
}

newObjectZ(VMWord s, VMPointer d) =>
    newObject(s,d, clear: true);
