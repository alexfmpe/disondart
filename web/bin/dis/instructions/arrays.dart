part of dis;

indx(VMPointer<VMArray> s, VMWord d){
  if(s.isNil)
    error(exBounds);
  VMArray a = s.dereference;
  if(d >= a.len)
    error(exBounds);
  var ptr = new Pointer<VMByte>.typed(a.data);
  return ptr + d*a.type.size;
}

_ind(VMPointer<VMArray> s, VMWord d, int size){
  if(s.isNil)
    error(exBounds);
  VMArray a = s.dereference;
  if(d >= a.len)
    error(exBounds);
  var ptr = new Pointer<VMByte>.typed(a.data);
  return ptr + d*size;
}

indb(VMPointer<VMArray> s, VMWord d) => _ind(s,d,sizeof(VMByte));
indw(VMPointer<VMArray> s, VMWord d) => _ind(s,d,sizeof(VMWord));
indl(VMPointer<VMArray> s, VMWord d) => _ind(s,d,sizeof(VMLong));
indf(VMPointer<VMArray> s, VMWord d) => _ind(s,d,sizeof(VMDouble));

newArray(VMWord s, VMWord m, VMPointer d, [bool clear = false]){
  TypeDescriptor t = typeWithID(m());
  int len = s();
  acheck(t.size, len);
  VMHeap h = heaparray(t, len, clear:clear);
  VMArray a = H2D(VMArray, h);
  overwritePointer(d, a.pointer);
}

newArrayZ(VMWord s, VMWord m, VMPointer d) =>
    newArray(s,m,d,true);



acheck(int elementSize, int elementNumber){
  if(elementNumber < 0)
    error(exNegsize);
}


slice(VMWord s, VMWord m, VMPointer d){
  VMArray a = d.dereference;
  int start = s();
  int end = m();

  int n = end - start;
  if(d.isNil){
    if(n == 0)  return;
    else        error(exNilref);
  }

  //middle test is redundant; also, what if (0 > start)?
  if(n < 0 || a.len < start || a.len < end)
    error(exBounds);

  VMHeap h = heap(Tarray);
  VMArray slice = H2D(VMArray, h);
  TypeDescriptor t = a.type;

  slice.len = n;
  slice.data = a.data + start * t.size;
  slice.type = t;
  t.ref++;

  if(a.isRoot){
    slice.root = a;
    h = D2H(a);
    d.pointTo(slice);
  }
  else{
    a = a._root.dereference;
    slice.root = a;
    h = D2H(a);
    h.ref++;
    overwritePointer(d, slice.pointer);
  }
  setMark(h);
}

sliceAssign(VMPointer<VMArray> s, VMWord m, VMPointer<VMArray> d){
  VMArray src = s.dereference;
  VMArray dst = d.dereference;
  int start = m();
  if(s.isNil)
    return;
  if(d.isNil)
    error(exNilref);
  if(m < 0 || start + src.len > dst.len)
    error(exBounds);

  TypeDescriptor t = dst.type;
  int l = src.len * t.size;
  Pointer dp = new Pointer<VMByte>.typed(dst) + start * t.size,
          sp = new Pointer<VMByte>.typed(src),
          ep = dp + l;

  //Needed to prevent premature frees in case of overlapping slices.
  if(t.np != 0){
    if(dp > sp){
      sp = sp + l;
      while(ep < dp){
        ep -= t.size;
        sp -= t.size;
        incmem(t, sp);
        freePointers(t, ep);
      }
    }
    else{
      while(dp < ep){
        incmem(t, sp);
        freePointers(t, dp);
        dp += t.size;
        sp += t.size;
      }
    }
  }
  memmove(dp, sp, src.len*t.size);
}
