part of dis;

int c2i(VMPointer p) => c2num(p, int);
int c2l(VMPointer p) => c2num(p, int);
int c2f(VMPointer p) => c2num(p, double);

num c2num(VMPointer p, t){
  if(p.isNil) return 0;
  VMString s = p.dereference;
  return t.parse(s.text);
}


i2c(VMWord s,   VMPointer d)  => num2c(s, d);
l2c(VMLong s,   VMPointer d)  => num2c(s, d);
f2c(VMDouble s, VMPointer d)  => num2c(s, d);

num2c(VMNumber n, VMPointer p){
  VMString str = newAscii(16);
  str.text = n().toString();
  overwritePointer(p, str.pointer);
}

Pointer<VMString> str2vmstr(String text){
  int len = text.runes.length;
  VMString str = isAscii(text) ? newAscii(len) : newRunes(len);
  str.text = text;
  return str.pointer;
}

cvtac(VMPointer s, VMPointer d){
  VMArray a = s.dereference;
  String text = UTF8.decode(a);
  Pointer p = str2vmstr(text);
  overwritePointer(d, p);
}

cvtca(VMPointer s, VMPointer d){
  VMString str = s.dereference;
  List<int> l = UTF8.encode(str.text);
  VMHeap h = heaparray(Tbyte, l.length);
  VMArray a = H2D(VMArray, h);
  for(int i = 0; i < l.length; i++)
    a[i] = l[i];
  overwritePointer(d, a.pointer);
}

