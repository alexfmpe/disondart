part of dis;

makeFrame(TypeDescriptor t, VMPointer d){
  Pointer nsp = new Pointer<VMByte>.typed(R.SP) + t.size;
  VMFrame f = view(VMFrame, [R.SP]);
  R.SP = nsp.value;
  f.type = t;
  f.mr = nil;
  if(t.np != 0)
    initmem(t, f.pointer);

  d.pointTo(f);
}

frame(VMWord s, VMPointer d){
  TypeDescriptor t = typeWithID(s());
  makeFrame(t,d);
}

//hardcoded to sys mod for now
mframe(VMPointer s, VMWord m, VMPointer d){
  TypeDescriptor t = findRuntab(m).type;
  makeFrame(t, d);
}
