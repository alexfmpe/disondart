part of dis;

VMList cons(int size, VMPointer p){
  var h = heap(Tlist, extra: size);
  VMList l = H2D(VMList, h);
  l.type = null;
  l.tail = p;
  if(! p.isNil)
    setMark(D2H(p));

  return l;
}

VMList consx(PointerMixin src, int size, PointerMixin old){
  var l = cons(size, old.dereference);
  memmove(l.data.pointer, src, size);
  return l;
}

VMList constype(PointerMixin src, TypeDescriptor t, PointerMixin old){
  var l = consx(src, t.size, old);
  l.type = t;
  if(t != null)
    t.ref++;
  return l;
}

consnum(VMNumber n, VMPointer d) =>
    consx(n.pointer, n.byteSize, d);

consptr(VMPointer p, VMPointer d){
  var l = constype(p.pointer, Tpointer, d);
  incref(p);
}

consmm(VMPointer s, VMWord m, VMPointer d) =>
    consx(s, m(), d);

consmp(VMPointer s, VMWord m, VMPointer d){
  var t = typeWithID(m());
  consx(s, t.size, d);
  incmem(t, s);
}




headnum(VMPointer<VMList> s, VMNumber d){
  VMList l = s.dereference;
  var ptr = new Pointer(l.data, d.runtimeType);
  d(ptr.dereference);
}

headptr(VMPointer<VMList> s, VMPointer d){
  VMList l = s.dereference;
  movePointer(d, l.data.pointer);
}

headmm(VMPointer<VMList> s, VMWord m, VMPointer d){
  VMList l = s.dereference;
  memmove(d, l.data.pointer, m());
}

headmp(VMPointer<VMList> s, VMWord m, VMPointer d){
  VMList l = s.dereference;
  movmp(d, m, l.data.pointer);
}


tail(VMPointer s, VMPointer d){
  VMList l = s.dereference;
  var ptr = new Pointer<VMByte>.typed(l.tail);
  movePointer(d, s);
}


