part of dis;

incref(PointerMixin s){
  if(!s.isNil){
    VMHeap h = D2H(s);
    h.ref++;
    setMark(h);
  }
}

movePointer(PointerMixin d, PointerMixin s){
  incref(s);
  overwritePointer(d, s);
}

movm(PointerMixin s, VMWord m, PointerMixin d) =>
    memmove(d,s,typeWithID(m()).size);

movmp(PointerMixin s, VMWord m, PointerMixin d){
  TypeDescriptor t = typeWithID(m());
  incmem(t, s);
  freePointers(t, d);
  memmove(d, s, t.size);
}

