part of dis;

typedef PseudoInstruction(List<String> tokens);
Map<String, PseudoInstruction> pseudoInstructions =
{
  'entry'   : ignore,
  'desc'    : pseudo_desc,
  'var'     : ignore,
  'string'  : pseudo_string,
  'word'    : pseudo_word,
  'byte'    : pseudo_byte,
  'module'  : ignore,
  'source'  : ignore,
  'link'    : ignore,
  'ldts'    : pseudo_ldts,
  'ext'     : pseudo_ext,
};
ignore(t){}

pseudo_ldts(List<String> tokens){}

int importCount = 0;
pseudo_ext(List<String> tokens){
  ///TODO: use null terminated strings within data instead of Dis strings
  VMImport imp = ldt.imports[importCount];
  importCount++;
  imp.sig = toInt(tokens[1]);
  imp.name = str2vmstr(unquote(tokens[2]));
}

pseudo_desc(List<String> tokens){
  int id    = toInt(tokens[0].substring(1)),
      size  = toInt(tokens[1]);
  List<int> map = tdmap(size, tokens[2]);

  /// will break if TD aren't ordered; do we care?
  moduleTypes.add(dtype(nofree, size, map));
}

pseudo_string(List<String> tokens){
  int offset = mpOffset(tokens[0]);
  VMPointer ptr = view(VMPointer, [R.MP + offset, VMString]);
  String s = unquote(tokens[1]);
  VMString str = str2vmstr(s).dereference;
  ptr.pointTo(str);
}

PseudoInstruction
  pseudo_word = pseudo_primitive(VMWord),
  pseudo_byte = pseudo_primitive(VMByte);

PseudoInstruction pseudo_primitive(Type t) =>
    (List<String> tokens){
      String adr = tokens[0];
      int    val = toInt(tokens[1]);

      if(new RegExp(r'ldt\+0').hasMatch(adr)){
        int size = sizeof(VMWord) + val * sizeof(VMImport);
        ldt = malloc(VMImports, size).dereference;
        ldt.nEntries = val;
        return;
      }
      int offset = mpOffset(adr);
      var p = view(t, [R.MP + offset]);
      p(val);
};


mpOffset (s) => moduleOffset(s, 'mp'  );
ldtOffset(s) => moduleOffset(s, 'ldt' );


moduleOffset(String s, String mod){
  var re = new RegExp('@$mod' + r'\+(\d+)');
  Match m = re.firstMatch(s);
  return toInt(m[1]);
}