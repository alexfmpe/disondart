part of dis;

_negf(VMReg R) {
  R.dst = -R.src;
}

_addb(VMReg R) {
  R.dst = R.mid + R.src;
}

_addw(VMReg R) {
  R.dst = R.mid + R.src;
}

_addl(VMReg R) {
  R.dst = R.mid + R.src;
}

_addf(VMReg R) {
  R.dst = R.mid + R.src;
}

_divb(VMReg R) {
  R.dst = R.mid / R.src;
}

_divw(VMReg R) {
  R.dst = R.mid / R.src;
}

_divl(VMReg R) {
  R.dst = R.mid / R.src;
}

_divf(VMReg R) {
  R.dst = R.mid / R.src;
}

_mulb(VMReg R) {
  R.dst = R.mid * R.src;
}

_mulw(VMReg R) {
  R.dst = R.mid * R.src;
}

_mull(VMReg R) {
  R.dst = R.mid * R.src;
}

_mulf(VMReg R) {
  R.dst = R.mid * R.src;
}

_subb(VMReg R) {
  R.dst = R.mid - R.src;
}

_subw(VMReg R) {
  R.dst = R.mid - R.src;
}

_subl(VMReg R) {
  R.dst = R.mid - R.src;
}

_subf(VMReg R) {
  R.dst = R.mid - R.src;
}

_modb(VMReg R) {
  R.dst = R.mid % R.src;
}

_modw(VMReg R) {
  R.dst = R.mid % R.src;
}

_modl(VMReg R) {
  R.dst = R.mid % R.src;
}

_andb(VMReg R) {
  R.dst = R.mid & R.src;
}

_andw(VMReg R) {
  R.dst = R.mid & R.src;
}

_andl(VMReg R) {
  R.dst = R.mid & R.src;
}

_orb(VMReg R) {
  R.dst = R.mid | R.src;
}

_orw(VMReg R) {
  R.dst = R.mid | R.src;
}

_orl(VMReg R) {
  R.dst = R.mid | R.src;
}

_xorb(VMReg R) {
  R.dst = R.mid ^ R.src;
}

_xorw(VMReg R) {
  R.dst = R.mid ^ R.src;
}

_xorl(VMReg R) {
  R.dst = R.mid ^ R.src;
}

_shlb(VMReg R) {
  R.dst = R.mid << R.src;
}

_shlw(VMReg R) {
  R.dst = R.mid << R.src;
}

_shll(VMReg R) {
  R.dst = R.mid << R.src;
}

_shrb(VMReg R) {
  R.dst = R.mid >> R.src;
}

_shrw(VMReg R) {
  R.dst = R.mid >> R.src;
}

_shrl(VMReg R) {
  R.dst = R.mid >> R.src;
}

_lsrw(VMReg R) {
  R.dst = R.mid >> R.src;
}

_lsrl(VMReg R) {
  R.dst = R.mid >> R.src;
}

_expf(VMReg R) {
  R.dst = R.mid.raise(R.src);
}

_expw(VMReg R) {
  R.dst = R.mid.raise(R.src);
}

_expl(VMReg R) {
  R.dst = R.mid.raise(R.src);
}

_beqb(VMReg R) {
  if(R.src == R.mid) branch(R.dst);
}

_beqw(VMReg R) {
  if(R.src == R.mid) branch(R.dst);
}

_beql(VMReg R) {
  if(R.src == R.mid) branch(R.dst);
}

_beqf(VMReg R) {
  if(R.src == R.mid) branch(R.dst);
}

_beqc(VMReg R) {
  if(R.src == R.mid) branch(R.dst);
}

_bneb(VMReg R) {
  if(R.src != R.mid) branch(R.dst);
}

_bnew(VMReg R) {
  if(R.src != R.mid) branch(R.dst);
}

_bnel(VMReg R) {
  if(R.src != R.mid) branch(R.dst);
}

_bnef(VMReg R) {
  if(R.src != R.mid) branch(R.dst);
}

_bnec(VMReg R) {
  if(R.src != R.mid) branch(R.dst);
}

_bgeb(VMReg R) {
  if(R.src >= R.mid) branch(R.dst);
}

_bgew(VMReg R) {
  if(R.src >= R.mid) branch(R.dst);
}

_bgel(VMReg R) {
  if(R.src >= R.mid) branch(R.dst);
}

_bgef(VMReg R) {
  if(R.src >= R.mid) branch(R.dst);
}

_bgec(VMReg R) {
  if(R.src >= R.mid) branch(R.dst);
}

_bgtb(VMReg R) {
  if(R.src >  R.mid) branch(R.dst);
}

_bgtw(VMReg R) {
  if(R.src >  R.mid) branch(R.dst);
}

_bgtl(VMReg R) {
  if(R.src >  R.mid) branch(R.dst);
}

_bgtf(VMReg R) {
  if(R.src >  R.mid) branch(R.dst);
}

_bgtc(VMReg R) {
  if(R.src >  R.mid) branch(R.dst);
}

_bleb(VMReg R) {
  if(R.src <= R.mid) branch(R.dst);
}

_blew(VMReg R) {
  if(R.src <= R.mid) branch(R.dst);
}

_blel(VMReg R) {
  if(R.src <= R.mid) branch(R.dst);
}

_blef(VMReg R) {
  if(R.src <= R.mid) branch(R.dst);
}

_blec(VMReg R) {
  if(R.src <= R.mid) branch(R.dst);
}

_bltb(VMReg R) {
  if(R.src <  R.mid) branch(R.dst);
}

_bltw(VMReg R) {
  if(R.src <  R.mid) branch(R.dst);
}

_bltl(VMReg R) {
  if(R.src <  R.mid) branch(R.dst);
}

_bltf(VMReg R) {
  if(R.src <  R.mid) branch(R.dst);
}

_bltc(VMReg R) {
  if(R.src <  R.mid) branch(R.dst);
}

_consb(VMReg R) {
  consnum(R.src,R.dst);
}

_consw(VMReg R) {
  consnum(R.src,R.dst);
}

_consl(VMReg R) {
  consnum(R.src,R.dst);
}

_consf(VMReg R) {
  consnum(R.src,R.dst);
}

_consptr(VMReg R) {
  consptr(R.src,R.dst);
}

_consm(VMReg R) {
  consmm(R.src,R.dst,R.mid);
}

_consmp(VMReg R) {
  consmp(R.src,R.dst,R.mid);
}

_headb(VMReg R) {
  headnum(R.src,R.dst);
}

_headw(VMReg R) {
  headnum(R.src,R.dst);
}

_headl(VMReg R) {
  headnum(R.src,R.dst);
}

_headf(VMReg R) {
  headnum(R.src,R.dst);
}

_headptr(VMReg R) {
  headptr(R.src,R.dst);
}

_headm(VMReg R) {
  headmm(R.src,R.dst,R.mid);
}

_headmp(VMReg R) {
  headmp(R.src,R.dst,R.mid);
}

_tail(VMReg R) {
  tail(R.src,R.dst);
}

_newa(VMReg R) {
  newArray(R.src,R.mid,R.dst);
}

_newaz(VMReg R) {
  newArrayZ(R.src,R.mid,R.dst);
}

_slicea(VMReg R) {
  slice(R.src,R.mid,R.dst);
}

_slicela(VMReg R) {
  sliceAssign(R.src,R.mid,R.dst);
}

_indb(VMReg R) {
  R.mid = indb(R.src,R.dst);
}

_indw(VMReg R) {
  R.mid = indw(R.src,R.dst);
}

_indl(VMReg R) {
  R.mid = indl(R.src,R.dst);
}

_indf(VMReg R) {
  R.mid = indf(R.src,R.dst);
}

_indx(VMReg R) {
  R.mid = indx(R.src,R.dst);
}

_jmp(VMReg R) {
  branch(R.dst);
}

_goto(VMReg R) {
  goto(R.src,R.dst);
}

_frame(VMReg R) {
  frame(R.src,R.dst);
}

_mframe(VMReg R) {
  mframe(R.src,R.mid,R.dst);
}

_call(VMReg R) {
  call(R.src,R.dst);
}

_mcall(VMReg R) {
  mcall(R.src,R.mid,R.dst);
}

_spawn(VMReg R) {
  spawn(R.src,R.dst);
}

_ret(VMReg R) {
  ret();
}

_exit(VMReg R) {
  terminate();
}

_raise(VMReg R) {
  raise(R.src);
}

_lea(VMReg R) {
  R.dst = R.src.address;
}

_lenc(VMReg R) {
  R.dst = R.src.dereference.length;
}

_lenl(VMReg R) {
  R.dst = R.src.dereference.length;
}

_lena(VMReg R) {
  R.dst = R.src.dereference.length;
}

_movb(VMReg R) {
  R.dst = R.src;
}

_movw(VMReg R) {
  R.dst = R.src;
}

_movl(VMReg R) {
  R.dst = R.src;
}

_movf(VMReg R) {
  R.dst = R.src;
}

_movp(VMReg R) {
  movePointer(R.dst,R.src);
}

_movm(VMReg R) {
  movm(R.dst,R.src,R.mid);
}

_movmp(VMReg R) {
  movmp(R.dst,R.src,R.mid);
}

_tcmp(VMReg R) {
  typeCompare(R.src,R.dst);
}

_new(VMReg R) {
  newObject(R.src,R.dst);;
}

_newz(VMReg R) {
  newObjectZ(R.src,R.dst);;
}

_cvtfl(VMReg R) {
  R.dst = R.src;
}

_cvtfw(VMReg R) {
  R.dst = R.src;
}

_cvtlf(VMReg R) {
  R.dst = R.src;
}

_cvtlw(VMReg R) {
  R.dst = R.src;
}

_cvtwl(VMReg R) {
  R.dst = R.src;
}

_cvtwf(VMReg R) {
  R.dst = R.src;
}

_cvtbw(VMReg R) {
  R.dst = R.src;
}

_cvtwb(VMReg R) {
  R.dst = R.src;
}

_cvtsw(VMReg R) {
  R.dst = R.src;
}

_cvtws(VMReg R) {
  R.dst = R.src;
}

_cvtrf(VMReg R) {
  R.dst = R.src;
}

_cvtfr(VMReg R) {
  R.dst = R.src;
}

_cvtcf(VMReg R) {
  R.dst = c2f(R.src);
}

_cvtcw(VMReg R) {
  R.dst = c2i(R.src);
}

_cvtcl(VMReg R) {
  R.dst = c2l(R.src);
}

_cvtfc(VMReg R) {
  f2c(R.src,R.dst);
}

_cvtwc(VMReg R) {
  i2c(R.src,R.dst);
}

_cvtlc(VMReg R) {
  l2c(R.src,R.dst);
}

_cvtac(VMReg R) {
  cvtac(R.src,R.dst);
}

_cvtca(VMReg R) {
  cvtca(R.src,R.dst);
}

