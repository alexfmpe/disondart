part of dis;

Map<String, Specification> specs = {
  "negf" : new Specification(_negf, [
    ['s', VMDouble],
    ['d', VMDouble],
  ]),
  "addb" : new Specification(_addb, [
    ['s', VMByte],
    ['m', VMByte],
    ['d', VMByte],
  ]),
  "addw" : new Specification(_addw, [
    ['s', VMWord],
    ['m', VMWord],
    ['d', VMWord],
  ]),
  "addl" : new Specification(_addl, [
    ['s', VMLong],
    ['m', VMLong],
    ['d', VMLong],
  ]),
  "addf" : new Specification(_addf, [
    ['s', VMDouble],
    ['m', VMDouble],
    ['d', VMDouble],
  ]),
  "divb" : new Specification(_divb, [
    ['s', VMByte],
    ['m', VMByte],
    ['d', VMByte],
  ]),
  "divw" : new Specification(_divw, [
    ['s', VMWord],
    ['m', VMWord],
    ['d', VMWord],
  ]),
  "divl" : new Specification(_divl, [
    ['s', VMLong],
    ['m', VMLong],
    ['d', VMLong],
  ]),
  "divf" : new Specification(_divf, [
    ['s', VMDouble],
    ['m', VMDouble],
    ['d', VMDouble],
  ]),
  "mulb" : new Specification(_mulb, [
    ['s', VMByte],
    ['m', VMByte],
    ['d', VMByte],
  ]),
  "mulw" : new Specification(_mulw, [
    ['s', VMWord],
    ['m', VMWord],
    ['d', VMWord],
  ]),
  "mull" : new Specification(_mull, [
    ['s', VMLong],
    ['m', VMLong],
    ['d', VMLong],
  ]),
  "mulf" : new Specification(_mulf, [
    ['s', VMDouble],
    ['m', VMDouble],
    ['d', VMDouble],
  ]),
  "subb" : new Specification(_subb, [
    ['s', VMByte],
    ['m', VMByte],
    ['d', VMByte],
  ]),
  "subw" : new Specification(_subw, [
    ['s', VMWord],
    ['m', VMWord],
    ['d', VMWord],
  ]),
  "subl" : new Specification(_subl, [
    ['s', VMLong],
    ['m', VMLong],
    ['d', VMLong],
  ]),
  "subf" : new Specification(_subf, [
    ['s', VMDouble],
    ['m', VMDouble],
    ['d', VMDouble],
  ]),
  "modb" : new Specification(_modb, [
    ['s', VMByte],
    ['m', VMByte],
    ['d', VMByte],
  ]),
  "modw" : new Specification(_modw, [
    ['s', VMWord],
    ['m', VMWord],
    ['d', VMWord],
  ]),
  "modl" : new Specification(_modl, [
    ['s', VMLong],
    ['m', VMLong],
    ['d', VMLong],
  ]),
  "andb" : new Specification(_andb, [
    ['s', VMByte],
    ['m', VMByte],
    ['d', VMByte],
  ]),
  "andw" : new Specification(_andw, [
    ['s', VMWord],
    ['m', VMWord],
    ['d', VMWord],
  ]),
  "andl" : new Specification(_andl, [
    ['s', VMLong],
    ['m', VMLong],
    ['d', VMLong],
  ]),
  "orb" : new Specification(_orb, [
    ['s', VMByte],
    ['m', VMByte],
    ['d', VMByte],
  ]),
  "orw" : new Specification(_orw, [
    ['s', VMWord],
    ['m', VMWord],
    ['d', VMWord],
  ]),
  "orl" : new Specification(_orl, [
    ['s', VMLong],
    ['m', VMLong],
    ['d', VMLong],
  ]),
  "xorb" : new Specification(_xorb, [
    ['s', VMByte],
    ['m', VMByte],
    ['d', VMByte],
  ]),
  "xorw" : new Specification(_xorw, [
    ['s', VMWord],
    ['m', VMWord],
    ['d', VMWord],
  ]),
  "xorl" : new Specification(_xorl, [
    ['s', VMLong],
    ['m', VMLong],
    ['d', VMLong],
  ]),
  "shlb" : new Specification(_shlb, [
    ['s', VMWord],
    ['m', VMByte],
    ['d', VMByte],
  ]),
  "shlw" : new Specification(_shlw, [
    ['s', VMWord],
    ['m', VMWord],
    ['d', VMWord],
  ]),
  "shll" : new Specification(_shll, [
    ['s', VMWord],
    ['m', VMLong],
    ['d', VMLong],
  ]),
  "shrb" : new Specification(_shrb, [
    ['s', VMWord],
    ['m', VMByte],
    ['d', VMByte],
  ]),
  "shrw" : new Specification(_shrw, [
    ['s', VMWord],
    ['m', VMWord],
    ['d', VMWord],
  ]),
  "shrl" : new Specification(_shrl, [
    ['s', VMWord],
    ['m', VMLong],
    ['d', VMLong],
  ]),
  "lsrw" : new Specification(_lsrw, [
    ['s', VMWord],
    ['m', VMUWord],
    ['d', VMWord],
  ]),
  "lsrl" : new Specification(_lsrl, [
    ['s', VMWord],
    ['m', VMULong],
    ['d', VMWord],
  ]),
  "expf" : new Specification(_expf, [
    ['s', VMWord],
    ['m', VMDouble],
    ['d', VMDouble],
  ]),
  "expw" : new Specification(_expw, [
    ['s', VMWord],
    ['m', VMWord],
    ['d', VMWord],
  ]),
  "expl" : new Specification(_expl, [
    ['s', VMWord],
    ['m', VMLong],
    ['d', VMLong],
  ]),
  "beqb" : new Specification(_beqb, [
    ['s', VMByte],
    ['m', VMByte],
    ['d', VMWord],
  ]),
  "beqw" : new Specification(_beqw, [
    ['s', VMWord],
    ['m', VMWord],
    ['d', VMWord],
  ]),
  "beql" : new Specification(_beql, [
    ['s', VMLong],
    ['m', VMLong],
    ['d', VMWord],
  ]),
  "beqf" : new Specification(_beqf, [
    ['s', VMDouble],
    ['m', VMDouble],
    ['d', VMWord],
  ]),
  "beqc" : new Specification(_beqc, [
    ['s', VMString],
    ['m', VMString],
    ['d', VMWord],
  ]),
  "bneb" : new Specification(_bneb, [
    ['s', VMByte],
    ['m', VMByte],
    ['d', VMWord],
  ]),
  "bnew" : new Specification(_bnew, [
    ['s', VMWord],
    ['m', VMWord],
    ['d', VMWord],
  ]),
  "bnel" : new Specification(_bnel, [
    ['s', VMLong],
    ['m', VMLong],
    ['d', VMWord],
  ]),
  "bnef" : new Specification(_bnef, [
    ['s', VMDouble],
    ['m', VMDouble],
    ['d', VMWord],
  ]),
  "bnec" : new Specification(_bnec, [
    ['s', VMString],
    ['m', VMString],
    ['d', VMWord],
  ]),
  "bgeb" : new Specification(_bgeb, [
    ['s', VMByte],
    ['m', VMByte],
    ['d', VMWord],
  ]),
  "bgew" : new Specification(_bgew, [
    ['s', VMWord],
    ['m', VMWord],
    ['d', VMWord],
  ]),
  "bgel" : new Specification(_bgel, [
    ['s', VMLong],
    ['m', VMLong],
    ['d', VMWord],
  ]),
  "bgef" : new Specification(_bgef, [
    ['s', VMDouble],
    ['m', VMDouble],
    ['d', VMWord],
  ]),
  "bgec" : new Specification(_bgec, [
    ['s', VMString],
    ['m', VMString],
    ['d', VMWord],
  ]),
  "bgtb" : new Specification(_bgtb, [
    ['s', VMByte],
    ['m', VMByte],
    ['d', VMWord],
  ]),
  "bgtw" : new Specification(_bgtw, [
    ['s', VMWord],
    ['m', VMWord],
    ['d', VMWord],
  ]),
  "bgtl" : new Specification(_bgtl, [
    ['s', VMLong],
    ['m', VMLong],
    ['d', VMWord],
  ]),
  "bgtf" : new Specification(_bgtf, [
    ['s', VMDouble],
    ['m', VMDouble],
    ['d', VMWord],
  ]),
  "bgtc" : new Specification(_bgtc, [
    ['s', VMString],
    ['m', VMString],
    ['d', VMWord],
  ]),
  "bleb" : new Specification(_bleb, [
    ['s', VMByte],
    ['m', VMByte],
    ['d', VMWord],
  ]),
  "blew" : new Specification(_blew, [
    ['s', VMWord],
    ['m', VMWord],
    ['d', VMWord],
  ]),
  "blel" : new Specification(_blel, [
    ['s', VMLong],
    ['m', VMLong],
    ['d', VMWord],
  ]),
  "blef" : new Specification(_blef, [
    ['s', VMDouble],
    ['m', VMDouble],
    ['d', VMWord],
  ]),
  "blec" : new Specification(_blec, [
    ['s', VMString],
    ['m', VMString],
    ['d', VMWord],
  ]),
  "bltb" : new Specification(_bltb, [
    ['s', VMByte],
    ['m', VMByte],
    ['d', VMWord],
  ]),
  "bltw" : new Specification(_bltw, [
    ['s', VMWord],
    ['m', VMWord],
    ['d', VMWord],
  ]),
  "bltl" : new Specification(_bltl, [
    ['s', VMLong],
    ['m', VMLong],
    ['d', VMWord],
  ]),
  "bltf" : new Specification(_bltf, [
    ['s', VMDouble],
    ['m', VMDouble],
    ['d', VMWord],
  ]),
  "bltc" : new Specification(_bltc, [
    ['s', VMString],
    ['m', VMString],
    ['d', VMWord],
  ]),
  "consb" : new Specification(_consb, [
    ['s', VMByte],
    ['d', VMList],
  ]),
  "consw" : new Specification(_consw, [
    ['s', VMWord],
    ['d', VMList],
  ]),
  "consl" : new Specification(_consl, [
    ['s', VMLong],
    ['d', VMList],
  ]),
  "consf" : new Specification(_consf, [
    ['s', VMDouble],
    ['d', VMList],
  ]),
  "consptr" : new Specification(_consptr, [
    ['s', VMPointer],
    ['d', VMList],
  ]),
  "consm" : new Specification(_consm, [
    ['s', VMPointer],
    ['m', VMWord],
    ['d', VMList],
  ]),
  "consmp" : new Specification(_consmp, [
    ['s', VMPointer],
    ['m', VMWord],
    ['d', VMList],
  ]),
  "headb" : new Specification(_headb, [
    ['s', VMList],
    ['d', VMByte],
  ]),
  "headw" : new Specification(_headw, [
    ['s', VMList],
    ['d', VMWord],
  ]),
  "headl" : new Specification(_headl, [
    ['s', VMList],
    ['d', VMLong],
  ]),
  "headf" : new Specification(_headf, [
    ['s', VMList],
    ['d', VMDouble],
  ]),
  "headptr" : new Specification(_headptr, [
    ['s', VMList],
    ['d', VMPointer],
  ]),
  "headm" : new Specification(_headm, [
    ['s', VMList],
    ['m', VMWord],
    ['d', VMPointer],
  ]),
  "headmp" : new Specification(_headmp, [
    ['s', VMList],
    ['m', VMWord],
    ['d', VMPointer],
  ]),
  "tail" : new Specification(_tail, [
    ['s', VMList],
    ['d', VMList],
  ]),
  "newa" : new Specification(_newa, [
    ['s', VMWord],
    ['m', VMWord],
    ['d', VMArray],
  ]),
  "newaz" : new Specification(_newaz, [
    ['s', VMWord],
    ['m', VMWord],
    ['d', VMArray],
  ]),
  "slicea" : new Specification(_slicea, [
    ['s', VMWord],
    ['m', VMWord],
    ['d', VMArray],
  ]),
  "slicela" : new Specification(_slicela, [
    ['s', VMArray],
    ['m', VMWord],
    ['d', VMArray],
  ]),
  "indb" : new Specification(_indb, [
    ['m', VMWord],
    ['s', VMArray],
    ['d', VMWord],
  ]),
  "indw" : new Specification(_indw, [
    ['m', VMWord],
    ['s', VMArray],
    ['d', VMWord],
  ]),
  "indl" : new Specification(_indl, [
    ['m', VMWord],
    ['s', VMArray],
    ['d', VMWord],
  ]),
  "indf" : new Specification(_indf, [
    ['m', VMWord],
    ['s', VMArray],
    ['d', VMWord],
  ]),
  "indx" : new Specification(_indx, [
    ['m', VMWord],
    ['s', VMArray],
    ['d', VMWord],
  ]),
  "jmp" : new Specification(_jmp, [
    ['d', VMWord],
  ]),
  "goto" : new Specification(_goto, [
    ['s', VMWord],
    ['d', VMWord],
  ]),
  "frame" : new Specification(_frame, [
    ['s', VMWord],
    ['d', VMFrame],
  ]),
  "mframe" : new Specification(_mframe, [
    ['s', VMPointer],
    ['m', VMWord],
    ['d', VMFrame],
  ]),
  "call" : new Specification(_call, [
    ['s', VMFrame],
    ['d', VMWord],
  ]),
  "mcall" : new Specification(_mcall, [
    ['s', VMFrame],
    ['m', VMWord],
    ['d', VMPointer],
  ]),
  "spawn" : new Specification(_spawn, [
    ['s', VMFrame],
    ['d', VMWord],
  ]),
  "ret" : new Specification(_ret, [
  ]),
  "exit" : new Specification(_exit, [
  ]),
  "raise" : new Specification(_raise, [
    ['s', VMPointer],
  ]),
  "lea" : new Specification(_lea, [
    ['s', VMWord],
    ['d', VMWord],
  ]),
  "lenc" : new Specification(_lenc, [
    ['s', VMString],
    ['d', VMWord],
  ]),
  "lenl" : new Specification(_lenl, [
    ['s', VMList],
    ['d', VMWord],
  ]),
  "lena" : new Specification(_lena, [
    ['s', VMArray],
    ['d', VMWord],
  ]),
  "movb" : new Specification(_movb, [
    ['s', VMByte],
    ['d', VMByte],
  ]),
  "movw" : new Specification(_movw, [
    ['s', VMWord],
    ['d', VMWord],
  ]),
  "movl" : new Specification(_movl, [
    ['s', VMLong],
    ['d', VMLong],
  ]),
  "movf" : new Specification(_movf, [
    ['s', VMDouble],
    ['d', VMDouble],
  ]),
  "movp" : new Specification(_movp, [
    ['s', VMPointer],
    ['d', VMPointer],
  ]),
  "movm" : new Specification(_movm, [
    ['s', VMPointer],
    ['m', VMWord],
    ['d', VMPointer],
  ]),
  "movmp" : new Specification(_movmp, [
    ['s', VMPointer],
    ['m', VMWord],
    ['d', VMPointer],
  ]),
  "tcmp" : new Specification(_tcmp, [
    ['s', VMPointer],
    ['d', VMPointer],
  ]),
  "new" : new Specification(_new, [
    ['s', VMWord],
    ['d', VMPointer],
  ]),
  "newz" : new Specification(_newz, [
    ['s', VMWord],
    ['d', VMPointer],
  ]),
  "cvtfl" : new Specification(_cvtfl, [
    ['s', VMDouble],
    ['d', VMLong],
  ]),
  "cvtfw" : new Specification(_cvtfw, [
    ['s', VMDouble],
    ['d', VMWord],
  ]),
  "cvtlf" : new Specification(_cvtlf, [
    ['s', VMLong],
    ['d', VMDouble],
  ]),
  "cvtlw" : new Specification(_cvtlw, [
    ['s', VMLong],
    ['d', VMWord],
  ]),
  "cvtwl" : new Specification(_cvtwl, [
    ['s', VMWord],
    ['d', VMLong],
  ]),
  "cvtwf" : new Specification(_cvtwf, [
    ['s', VMWord],
    ['d', VMDouble],
  ]),
  "cvtbw" : new Specification(_cvtbw, [
    ['s', VMByte],
    ['d', VMWord],
  ]),
  "cvtwb" : new Specification(_cvtwb, [
    ['s', VMWord],
    ['d', VMByte],
  ]),
  "cvtsw" : new Specification(_cvtsw, [
    ['s', VMShort],
    ['d', VMWord],
  ]),
  "cvtws" : new Specification(_cvtws, [
    ['s', VMWord],
    ['d', VMShort],
  ]),
  "cvtrf" : new Specification(_cvtrf, [
    ['s', VMSingle],
    ['d', VMDouble],
  ]),
  "cvtfr" : new Specification(_cvtfr, [
    ['s', VMDouble],
    ['d', VMSingle],
  ]),
  "cvtcf" : new Specification(_cvtcf, [
    ['s', VMString],
    ['d', VMDouble],
  ]),
  "cvtcw" : new Specification(_cvtcw, [
    ['s', VMString],
    ['d', VMWord],
  ]),
  "cvtcl" : new Specification(_cvtcl, [
    ['s', VMString],
    ['d', VMLong],
  ]),
  "cvtfc" : new Specification(_cvtfc, [
    ['s', VMDouble],
    ['d', VMString],
  ]),
  "cvtwc" : new Specification(_cvtwc, [
    ['s', VMWord],
    ['d', VMString],
  ]),
  "cvtlc" : new Specification(_cvtlc, [
    ['s', VMLong],
    ['d', VMString],
  ]),
  "cvtac" : new Specification(_cvtac, [
    ['s', VMPointer],
    ['d', VMPointer],
  ]),
  "cvtca" : new Specification(_cvtca, [
    ['s', VMPointer],
    ['d', VMPointer],
  ]),
};