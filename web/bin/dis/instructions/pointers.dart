part of dis;

overwritePointer(PointerMixin d, PointerMixin s){
  var tmp = new Pointer<VMByte>.typed(d.value);
  d.pointTo(s.value);
  destroy(tmp);
}



typeCompare(VMPointer s, VMPointer d){
  if(s.isNil)
    return;
  if(d.isNil)
    error(exTcheck);
  if(D2H(s).type != D2H(d).type)
    error(exTcheck);
}

