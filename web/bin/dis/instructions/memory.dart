part of dis;

memmove(PointerMixin d, PointerMixin s, int m){
  if(d.value > s.value){
    s += m;
    d += m;
    while(m >= 0){
      d[0] = s[0];
      s--;
      d--;
      m--;
    }
  }
  else{
    while(m >= 0){
      d[0] = s[0];
      s++;
      d++;
      m--;
    }
  }
}

memset(PointerMixin d, int i, int size){
  while(size-- > 0){
    d[0] = i;
    d++;
  }
}