library dis;

//temp hack
import '../bin.dart';

import 'dart:io';
import 'dart:math';
import 'dart:convert';
import 'dart:mirrors';
import 'dart:collection';
import 'dart:typed_data';

part 'code/code.dart';
part 'code/operands.dart';
part 'code/from_assembly.dart';

part 'instructions/math.dart';
part 'instructions/moves.dart';
part 'instructions/lists.dart';
part 'instructions/arrays.dart';
part 'instructions/frames.dart';
part 'instructions/pseudo.dart';
part 'instructions/memory.dart';
part 'instructions/threads.dart';
part 'instructions/strings.dart';
part 'instructions/objects.dart';
part 'instructions/modules.dart';
part 'instructions/pointers.dart';
part 'instructions/channels.dart';
part 'instructions/functions.dart';
part 'instructions/conversions.dart';
part 'instructions/control_flow.dart';

part 'instructions/opcodes.dart';
part 'instructions/dsl/generate.dart';
part 'instructions/dsl/instructions.dart';
part 'instructions/dsl/specification.dart';

part 'instructions/generated/specifications.dart';
part 'instructions/generated/implementations.dart';

part 'interpreter/runt.h.dart';
part 'interpreter/prog.dart';
part 'interpreter/scheduler.dart';
part 'interpreter/exceptions.dart';
part 'interpreter/interpreter.dart';
part 'interpreter/type_descriptors.dart';

part 'memory/heap.dart';
part 'memory/pool.dart';
part 'memory/blocks.dart';
part 'memory/memory_region.dart';
part 'memory/virtual_address.dart';

part 'sys/sysmod.dart';
part 'sys/formats.dart';
part 'sys/implementations.dart';

part 'tests/maps.dart';
part 'tests/types.dart';
part 'tests/blocks.dart';
part 'tests/assembly.dart';
part 'tests/instructions/math.dart';

part 'types/mixins.dart';
part 'types/memory.dart';
part 'types/pointer.dart';
part 'types/abstract.dart';
part 'types/primitive.dart';
part 'types/composite.dart';
part 'types/factories.dart';
part 'types/switch_case.dart';

part 'utils/map.dart';
part 'utils/misc.dart';
part 'utils/list.dart';
part 'utils/files.dart';
part 'utils/types.dart';
part 'utils/output.dart';
part 'utils/binary.dart';
part 'utils/strings.dart';
part 'utils/functions.dart';
part 'utils/conversion.dart';

main() => bin_main();