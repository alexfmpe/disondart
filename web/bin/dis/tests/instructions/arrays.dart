String arrays0 = r'''
  movw  $-1, 0(FP)
  newa  $16, $0, 0(FP)
  indb  0(MP),  0(FP), $1
  indw  4(MP),  0(FP), $1
''';

String arrays1 = r'''
  movw  $-1, 0(FP)
  newa  $16, $0, 0(FP)
  indf  0(MP),  0(FP), $1
  indl  4(MP),  0(FP), $1
''';

String array2 = r'''
  movw  $-1, 0(FP)
  newa  $16, $0, 0(FP)
  indb  0(MP),  0(FP), $1
  indw  4(MP),  0(FP), $1
  indl  8(MP),  0(FP), $1
  indf 12(MP),  0(FP), $1
  lena  0(FP),  4(FP)

''';


String array3 = r'''
  movw  $-1, 0(FP)
  movw  $-1, 4(FP)
  movw  $-1, 8(FP)
  movw  $-1, 12(FP)
  newa  $16, $0, 0(FP)
  newa  $32, $0, 4(FP)
  lena  0(FP), 0(MP)
  lena  4(FP), 4(MP)
''';

String array4 = r'''
  movw  $-1     0(FP)
  movw  $-1     0(MP)
  newa  $16     $0      0(FP)
  newa  $16     $0      0(MP)
  lena  0(FP)   4(FP)
  movp  0(MP)   0(FP)
  exit
''';

String array5 = r'''
movw  $0x11     0x10(FP)
movw  $0x22     0x14(FP)
movw  $0x33     0x18(FP)
movw  $0x44     0x1C(FP)
movw  $-1       0x20(FP)
movw  $-1       0x24(FP)
newa  $16       $0        0x20(FP)
newa  $16       $0        0x24(FP)
movp  0x20(FP)  0x24(FP)        
ret
''';

String array6 = r'''
movw  $0x11     0x10(FP)
movw  $0x22     0x14(FP)
movw  $0x33     0x18(FP)
movw  $0x44     0x1C(FP)
movw  $-1       0x20(FP)
movw  $-1       0x24(FP)
newa  $32       $257        0x20(FP)
newa  $32       $257        0x24(FP)
movp  0x20(FP)  0x24(FP)        
movp  $-1       0x24(FP)
movp  $-1       0x20(FP)
ret
''';

String array7 = r'''
movw  $0x11     0x10(FP)
movw  $0x22     0x14(FP)
movw  $0x33     0x18(FP)
movw  $0x44     0x1C(FP)
movw  $-1       0x20(FP)
movw  $-1       0x24(FP)
newa  $32       $256        0x20(FP)
newa  $32       $0          0x24(FP)
movp  0x20(FP)  0x24(FP)
movp  $-1       0x24(FP)
movp  $-1       0x20(FP)
ret
desc    $0  1     ""
string  @mp+0     "ABCDEF"
word    @mp+4     0xAABBCCDD
byte    @mp+8     0xEE
''';

String ctrlflow = r'''
  movw  $0x11   0(FP)
  movw  $0x11   4(FP)
  bnew  0(FP)   4(FP) $0x60
  movw  $0x33   8(FP)
  movw  $0x44  12(FP)
  exit
''';