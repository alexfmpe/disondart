part of dis;

String currentAssembly = 'add';
Map<String, String> assembly = {


'add' : r'''
        load    0(mp),$0,12(mp)
        frame   $1,44(fp)
        movp    4(mp),32(44(fp))
        movw    $3,36(44(fp))
        lea     40(fp),16(44(fp))
        mcall   44(fp),$0,12(mp)
        ret
        entry   0, 2
        desc    $0,16,"f0"
        desc    $1,40,"0080"
        desc    $2,48,"00c0"
        var     @mp,16
        string  @mp+0,"$Sys"
        string  @mp+4,"%d\n"
        module  Command
        link    2,0,0x4244b354,"init"
        ldts    @ldt,1
        word    @ldt+0,1
        ext     @ldt+4,0xac849033,"print"
        source  "/mylimbo/src/add.m"
;

''',

'addZ' : r'''
  frame $1        52(FP)
  movw  $0x11     32(52(FP))
  movw  $0x22     36(52(FP))
  lea   48(FP)    16(52(FP))
  call  52(FP)    $13

  frame   $2      48(FP)
  movw    $-1     32(48(FP))
  movp    0(MP)   32(48(FP))
  movw    48(FP)  36(48(FP))
  lea     40(FP)  16(48(FP))
  mcall   48(FP)  $1    12(MP)

  movw  48(FP)    0(FP)
  ret

  addw  32(FP)    36(FP)      0(16(FP))
  ret

  desc  $0    16    "f0"
  desc  $1    40    "00"
  desc  $2    48    ""

  string  @mp+0     "0x11+0x22=%d"
  word    @ldt+0    2
  ext     @ldt+4    0x616977e8    "millisec"
  ext     @ldt+20   0xac849033    "print"
''',


'factorial' : r'''
  frame $1      48(FP)
  movw  $3     32(48(FP))
  lea   44(FP)  16(48(FP))
  call  48(FP)  $11 
  
  frame   $1      48(FP)
  movw    $-1     32(48(FP))
  movp    0(MP)   32(48(FP))
  movw    44(FP)  36(48(FP))
  lea     40(FP)  16(48(FP))
  mcall   48(FP)  $1    12(MP)
  ret

  bnew  $0      32(FP)      $14
  movw  $1      0(16(FP))
  ret
  frame $1      40(FP)
  subw  $1      32(FP)      32(40(FP))
  lea   36(FP)  16(40(FP))
  call  40(FP)  $11
  mulw  36(FP)  32(FP)      0(16(FP))
  ret

  desc  $0    16    "f0"
  desc  $1    48    ""

  string  @mp+0     "10!=%d"
  word    @ldt+0    2
  ext     @ldt+4    0x616977e8    "millisec"
  ext     @ldt+20   0xac849033    "print"
''',


'millisecs' : r'''
  movw  $0x11111111     32(FP)
  movw  $0x22222222     36(FP)

  mframe  12(MP)  $0    44(FP)
  lea     40(FP)  16(44(FP))
  mcall   44(FP)  $0    12(MP)

  mframe  12(MP)  $0    44(FP)
  lea     40(FP)  16(44(FP))
  mcall   44(FP)  $0    12(MP)

  ret

  desc    $0  1     ""
  string  @mp+0     "ABCDEF"
  word    @mp+4     0xAABBCCDD
  byte    @mp+8     0xEE
  word    @ldt+0    3
  ext     @ldt+4    0x616977e8    "millisec"
  ext     @ldt+20   0xac849033    "print"
  ext     @ldt+32   0xe67bf126    "sleep"
''',

'sleepTest' : r'''
  movw  $0x11111111     32(FP)
  movw  $0x22222222     36(FP)

  mframe  12(MP)  $0    44(FP)
  lea     40(FP)  16(44(FP))
  mcall   44(FP)  $0    12(MP)

  mframe  12(MP)  $2    44(FP)
  movw    $1500   32(44(FP))
  lea     40(FP)  16(44(FP))
  mcall   44(FP)  $2    12(MP)

  mframe  12(MP)  $0    44(FP)
  lea     40(FP)  16(44(FP))
  mcall   44(FP)  $0    12(MP)

  ret
  desc    $0  1     ""
  string  @mp+0     "ABCDEF"
  word    @mp+4     0xAABBCCDD
  byte    @mp+8     0xEE
  word    @ldt+0    3
  ext     @ldt+4    0x616977e8    "millisec"
  ext     @ldt+20   0xac849033    "print"
  ext     @ldt+32   0xe67bf126    "sleep"
''',

'systest' : r'''
  mframe  12(MP)  $0    44(FP)
  lea     40(FP)  16(44(FP))
  mcall   44(FP)  $0    12(MP)

  mframe  12(MP)  $2    44(FP)
  movw    $0x10   32(44(FP))
  mcall   44(FP)  $2    12(MP)

  mframe  12(MP)  $0    44(FP)
  lea     40(FP)  16(44(FP))
  mcall   44(FP)  $0    12(MP)

  mframe  12(MP)  $1      44(FP)
  movw    $-1     32(44(FP))
  movp    0(MP)   32(44(FP))
  movw    40(FP)  36(44(FP))
  lea     40(FP)  16(44(FP))
  mcall   44(FP)  $1    12(MP)

  ret
  desc    $0  1     ""
  string  @mp+0     "Slept:%-u#8.4xWW"
  word    @ldt+0    3
  ext     @ldt+4    0x616977e8    "millisec"
  ext     @ldt+20   0xac849033    "print"
  ext     @ldt+32   0xe67bf126    "sleep"
''',
};