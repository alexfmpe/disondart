part of dis;

List numbers = ['zero', 'one', 'two', 'three'];
Map inverses =
{
 'up'   : 'down',
 'left' : 'right',
};

Function number = doubleMap(numbers);
Function inverse = doubleMap(inverses);

test_dual(x){
  Map dual = dualMap(x);
  values(x).forEach((e){
    assert(e == x[dual[e]]);
    });
  keys(x).forEach((k){
    assert(k == dual[x[k]]);
  });
}



test_maps(){
  test_dual(numbers);
  test_dual(inverses);
}
