part of dis;

test_types(){
  List<Type> types = [VMArray, VMList, VMByte, VMPointer];
  List<Function> tests = [isReference, isPrimitive];

  print(types.map(isReference).toList());
  print(types.map(isPrimitive).toList());
}