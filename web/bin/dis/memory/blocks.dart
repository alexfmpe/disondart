part of dis;

String noBlock = 'noAvailableBlockException';

class BlockStats{
  int times = 0;
  int total = 0;
  update(size){
    times += 1;
    total += size;
  }
  toString() => '$times for a total of $total';
}

class Blocks extends SplayTreeMap<int,int>{
  var stats = new BlockStats();
  bool join;

  Blocks(int size, [bool this.join = true]){
    if(size > 0)
      this[0] = size;
  }

  addBlock(int address, int size){
    stats.update(size);
    if(join){
      int prev = lastKeyBefore(address);
      int next = firstKeyAfter(address);
      bool before = (prev != null) && (prev     + this[prev]  == address);
      bool after  = (next != null) && (address  + size        == next   );

      if(before && after){
        this[prev] += size + this[next];
        remove(next);
        return prev;
      }
      if(before){
        this[prev] += size;
        return prev;
      }
      if(after){
        this[address] = size + this[next];
        remove(next);
        return address;
      }
    }
    this[address] = size;
    return address;
  }

  int cutBlock(int size){
    for(var addr in this.keys){
      if(this[addr] < size)
        continue;

      this[addr] -= size;
      var result = addr + this[addr];
      if(this[addr] == 0)
        remove(addr);
      return result;
    }
    throw noBlock;
  }

  toString(){
    var m = new SplayTreeMap<int, String>();
    this.forEach((k,v) => m[k] = toHex(v));
    return m.toString();
  }
}