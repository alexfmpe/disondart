part of dis;

alignTo(int address, [int align = 16]) =>
    address + (-address % align);

var nil = new Pointer<VMByte>.typed(0);
var H   = new Pointer<VMByte>.typed(0xFFFFFFFF);

const List emptyList = const [];

class Pool{
  String name;
  int pnum, quanta, chunk, monitor, nbrk, lastfree;
  int maxSize, restrictedSize, currentSize, arenasize, hw;
  Function move;
  // Lock l;
  // Bhdr root, chain;
  MemoryRegion memory;
  Blocks freeBlocks, usedBlocks;

  get nalloc  => usedBlocks.stats.times;
  get nfree   => freeBlocks.stats.times;

  get totalAlloced  => usedBlocks.stats.total;
  get totalFreed    => freeBlocks.stats.total;

  Pool(this.name, this.pnum, this.maxSize, this.quanta,
       this.chunk, this.monitor, this.restrictedSize){
    int size = 256*4;
    memory = new MemoryRegion(size);
    freeBlocks = new Blocks(size);
    usedBlocks = new Blocks(0, false);
  }
  get offset => pnum << 28;

  free(int address){
    int size = usedBlocks[address];
    usedBlocks.remove(address);
    freeBlocks.addBlock(address, size);
    return size;
  }

  alloc(int size){
    int addr = freeBlocks.cutBlock(size);
    usedBlocks.addBlock(addr, size);
    return addr;
  }


  get stats =>  '$name allocs:\t' + usedBlocks.stats.toString() + '\n' +
                '$name frees:\t'  + freeBlocks.stats.toString();

  toString() => '$name:\n$memory';
}




Pool  mainMemory  = new Pool("main",  0,   32*1024*1024, 15,  512*1024, 0, 31*1024*1024),
      heapMemory  = new Pool("heap",  1,   32*1024*1024, 15,  512*1024, 0, 31*1024*1024),
      imageMemory = new Pool("image", 2,   32*1024*1024+256, 15, 4*1024*1024, 1, 31*1024*1024);

class Table{
  get n => pools.length;
  List<Pool> pools;
  Table(this.pools);
}

Table table = new Table([mainMemory, heapMemory, imageMemory]);

Pool selectPool(int address){
  int poolID = address >> 28;
  var l = [mainMemory, heapMemory, imageMemory];
  var p = l[poolID];
  return p;
}

Pointer malloc(Type t, [int n]) => alloc(mainMemory, t, n);
Pointer halloc(Type t, [int n]) => alloc(heapMemory, t, n);

Pointer alloc(Pool p, Type t, [int n]){
  if(n == null)
    n = sizeof(t);
  var size = alignTo(n, p.quanta+1);
  int addr = p.alloc(size) + p.offset;
  if(DEBUG_ENABLED)
    print('|${p.name}| alloc asked: ${toHex(n)} \t allocated: ${toHex(size)} bytes @ ${toHex(addr)}');
  return new Pointer(addr, t);
}

int free(PointerMixin ptr){
  int addr = ptr.value;
  Pool p = selectPool(addr);
  int size = p.free(addr - p.offset);
  if(DEBUG_ENABLED)
    print('|${p.name}| free on:\t${toHex(addr)} \t size: ${toHex(size)}');
  return size;
}

