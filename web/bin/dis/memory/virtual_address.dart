part of dis;

class VirtualAddress extends Object with VMComparable, PseudoInt{
  int realAddress;
  int poolAddress;
  Pool pool;
  VirtualAddress(int address, [Pool p]){
    if(p == null){
      pool = selectPool(address);
      poolAddress = address - pool.offset;
      realAddress = address;
    }
    else{
      pool = p;
      poolAddress = address;
      realAddress = address + pool.offset;
    }
  }

  get bd => pool.memory.bd;

  toInt() => realAddress;
  createPseudo(i) => new VirtualAddress(i);

  operator ==(VirtualAddress vaddr) =>
      this.realAddress == vaddr.realAddress;

  operator <(VirtualAddress vaddr) =>
      this.realAddress < vaddr.realAddress;

  toString() =>
      '${pool.name} $poolAddress ($realAddress)';
}