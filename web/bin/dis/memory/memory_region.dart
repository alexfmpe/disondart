part of dis;

class MemoryRegion extends Object with ListMixin<int>{
  ByteData bd;
  int length;
  MemoryRegion(int this.length){
    bd = new ByteData(length);
  }

  operator [](int index)              => bd.getUint8(index);
  operator []=(int index, int value)  => bd.setUint8(index, value);

  toString() => new Matrix(this, 16).toString();
}



