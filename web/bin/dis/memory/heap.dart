part of dis;

const mutator = 0;
const propagator = 3;

setMark(VMHeap h){
  if(h.color != mutator){
    h.color = propagator;
    // nprop = 1; ????
  }
}

VMHeap D2H(x){
  int addr =  (x is PointerMixin) ? x.value :
              (x is VMObject)     ? x.address :
              throw 'lolwut';
  return new Pointer<VMHeap>.typed(addr-sizeof(VMHeap)).dereference;
}




VMObject H2D(Type t, VMHeap h) =>
    new Pointer(h.address + sizeof(VMHeap), t).dereference;

/*
 *  Heap initialization
 */

VMHeap nheap(int n){
  var ph = halloc(VMHeap, n + sizeof(VMHeap));
  if(ph.isNil)
    error(exHeap);

  VMHeap h = view(VMHeap, [ph.value]);
  h.type = null;
  h.ref = 1;
  h.color = mutator;
  return h;
}

VMHeap heap(TypeDescriptor t, {extra: 0, clear: false}){
  VMHeap h = nheap(t.size + extra);
  h.type = t;
  t.ref++;
  //if(zeroset)
  //memset(H2D(void*, h), 0, t->size);

  if(t.np != 0)
    initmem(t, H2D(VMByte, h).pointer);

  return h;
}

VMHeap heaparray(TypeDescriptor t, int length, {clear:false}){
  VMHeap h = heap(Tarray, extra: length*t.size);
  VMArray a = H2D(VMArray, h);
  a.type = t;
  a.len = length;
  a.root = H;
  a.data = a.address + sizeof(VMArray);
  initarray(t, a);
  return h;
}


/*
 *  Pointer initialization
 */

initarray(TypeDescriptor t, VMArray a){
  t.ref++;
  if(t.np == 0)
    return;

  var p = new Pointer<VMByte>.typed(a.data);
  for(int i = 0; i < a.len; i++){
    initmem(t, p);
    p += t.size;
  }
}

initmem(TypeDescriptor t, PointerMixin p) =>
    memset(new Pointer(p.value, VMByte), -1, t.size);
    //forEachPointer(t, p, (w, b) => w[7-b] = H);

incmem(TypeDescriptor t, PointerMixin p) =>
    forEachPointer(t, p, (w,b){
      if(w[7-b] != H){
        VMHeap h = D2H(w[7-b]);
        h.ref++;
        setMark(h);
      }
    });

forEachPointer(TypeDescriptor t, Pointer p, callback(Pointer w, int b)){
  var w = new Pointer(p.value, VMWord, 2);
  t.map.forEach((c){
    if(c != 0)
      for(int b = 0; b < 8; b++)
        if(bits(c, b, b+1) == 1)
          callback(w, b);
    w += 8;
  });
}
////////////////////////////////////////////////////////

destroy(PointerMixin p){
  if(p.isNil)
    return;
  VMHeap h = D2H(p);
  h.ref--;
  if(h.ref > 0)
    return;
  var t = h.type;
  if(t != null)
    t.free(h, 0);
  free(h.pointer);
}

freeArray(VMHeap h, int swept){
  VMArray a = H2D(VMArray, h);
  var t = a.type;

  if(swept == 0){
    destroy(a._root);
    if(t.np != 0){
      var v = new Pointer(a.data(), VMByte);
      for(int i = 0; i < a.len(); i++){
        freePointers(t, v);
        v += t.size;
      }
    }
  }
  t.ref--;
  if(t.ref == 0)
    freeType(t);
}

freeList(VMHeap h, int swept){
  VMList l = H2D(VMList, h);
  var t = l.type;
  freePointers(t, l.data.pointer);
  t.ref--;
  if(t.ref == 0)
    freeType(t);
  destroy(l.tail);
}


freeString(VMHeap h, int swept){}

freePointers(TypeDescriptor t, Pointer p){
  if(t.np != 0)
    forEachPointer(t, p, (w, b) =>
        destroy(w[7-b]));
}

//empty until types are moved back into vm memory
freeType(TypeDescriptor t){}


////////////////////////////////////////////////////////




