part of dis;

writeFile(String path, Object data){
  File f = new File(path);
  f.openWrite()
    ..write(data)
    ..close();
}