part of dis;

int bits(int num, int start, int end) =>
    (num >> start) & bitmask(start,end);

int bitmask(int start, int end) =>
    (1 << (end - start)) - 1;