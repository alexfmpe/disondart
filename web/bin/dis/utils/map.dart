part of dis;

//disjoint sets only
doubleMap(original, [Type domain]){
  if(domain == null)
    domain = (original is List) ? 1.runtimeType : ''.runtimeType;

  Map dual = dualMap(original);
  return (key) => (hasType(key, domain)) ? original[key] : dual[key];
}

dualMap(x){
  Map m = new Map();
  keys(x).forEach((k) => m[x[k]]= k);
  return m;
}

hasType(obj, Type t) => obj.runtimeType == t;


indexOf(List l, x) =>
  (x is Predicate) ? keys(l).firstWhere((int k) => x(l[k]), orElse: constant(-1))
                   : l.indexOf(x);


Iterable keys(x) =>
    (x is List) ? range(0, x.length) :
    (x is Map)  ? x.keys :
    throw 'lolwut';

Iterable values(x) =>
    (x is List) ? x :
    (x is Map)  ? x.values :
    throw 'lolwut';

typedef DoubleMap(x);
typedef Iterable Collector(x);
typedef bool Predicate<T>(x);

