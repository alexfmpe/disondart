part of dis;

Function compose(Function a, Function b) =>
    (x) => a(b(x));

Function chain([List<Function> l]) =>
    l.fold(identity, compose);
