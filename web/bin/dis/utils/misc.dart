part of dis;

notNull(x) => x != null;
isPositive(num n) => n >= 0;
identity(x) => x;
constant(x) => () => x;
/*
Function equals(n) =>
    (x) => (n == x);
*/
List splitNum(num) => [num % 256, num ~/ 256];
num joinBytes(x,y) => x*256+y;

bool isAscii(String s) =>
  s.runes.every(isAsciiChar);

bool isAsciiChar(int i) =>
    i <= 0x7F;