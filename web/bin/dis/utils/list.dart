part of dis;

permutate(List list, List<int> permutation) =>
    permutation.where(isPositive)
               .map((e) => list[e])
               .toList();


List flatten(List<List> ll) =>
    ll.expand(identity).toList();

class Matrix{
  List<List> rows;
  String columnLabels;

  Matrix(List l, int nColumns, [int nRows]){
    if(nRows == null)
      nRows = (l.length / nColumns).ceil();
    rows = range(0, nRows-1).map(
        (int r) => l.sublist(r*nColumns, (r+1)*nColumns)).toList();

    rows.add(l.sublist((nRows-1) * nColumns, l.length));
    columnLabels = toHex(range(0, nColumns)).join('  ');
  }

  toString() => ' $columnLabels \n' +  rows.map(toHex).join('\n') + '\n';
}


List<int> range(int a, int b, [int inc = 1]) =>
    new List.generate(((b-a)/inc).ceil(), (n) => a+n*inc, growable: true);


List<String> lineSplit(String s)  => nonEmptySplit(s, '\n');
List<String> spaceSplit(String s) => nonEmptySplit(s,  ' ');

List<String> nonEmptySplit(String str, String separator) =>
    str.split(separator)
       .map((l) => l.trim())
       .where(isNotEmpty)
       .toList();

bool isNotEmpty(String l) => l.isNotEmpty;


map(Iterable<Iterable> argLists, Function f) =>
    argLists.map((Iterable l) =>
        Function.apply(f, l.toList()));

Iterable<Iterable> zip(List<List> lists){
  int len = lists.map((l) => l.length).reduce(min);
  return range(0, len).map((i) => lists.map((l) => l[i]));


}

fold(Iterable l, Function combine, [initialValue = 0]) =>
    l.fold(initialValue, combine);

List unfold(seed, Function unspool, [Function finished]){
  if(finished == null)
    finished = (x) => (0 == x);

  if(finished(seed))
    return [];

  List pair = unspool(seed);
  List l = [pair[0]];
  return l..addAll(unfold(pair[1], unspool, finished));
}

List unfoldr(seed, Function unspool, [Function finished]) =>
    unfold(seed, unspool, finished).reversed.toList();


binarySearch(List l, key, Function partitionSelector){
  find(int start, int stop){
    if(start > stop) return -1;
    int i = (start + stop) >> 1;
    var x = l[i];
    int n = partitionSelector(x, key);
    return  (n < 0) ? find(start, i-1):
            (n > 0) ? find(i+1, stop):
            i;
  }

  return find(0, l.length-1);
}