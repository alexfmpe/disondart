part of dis;

typedef T Transform<T> (T t);

Transform<String> appender(String suffix) =>
    (String str) => str + suffix;

Transform<String> preppender(String prefix) =>
    (String str) => prefix + str;

append(String suffix, String str) =>
    appender(suffix)(str);

preppend(String suffix, String str) =>
    preppender(suffix)(str);