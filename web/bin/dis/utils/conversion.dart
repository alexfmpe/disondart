part of dis;

var conversionException = 'Conversion Not Supported';

toInt(x) =>
    (x is int)    ? x :
    (x is double) ? x.toInt():
    (x is String) ? int.parse(x) :
    (x is List)   ? fold(x.map((e) => toInt(e)), joinBytes) :
    throw conversionException;

toFloat(x) =>
  (x is double) ? x :
  (x is int)    ? x.toDouble() :
  (x is String) ? double.parse(x) :
  throw conversionException;

toHex(x) =>
  (x is int)      ? x.toRadixString(16).toUpperCase().padLeft(2, '0'):
  (x is String)   ? x:
  (x is List)     ? x.map(toHex).toList():
  throw conversionException;

toBytes(x) =>
    unfoldr(toInt(x), splitNum);
