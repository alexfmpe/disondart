part of dis;

Map<Type, int> sizes = {};

int sizeof(Type t){
  int size = sizes[t];
  if(size == null){
    size = computeSize(t);
    sizes[t] = size;
  }
  return size;
}

int computeSize(Type t) =>
    view(t, [0]).byteSize;
