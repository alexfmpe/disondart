library bin;

import 'rewrite/rewrite.dart' as rewrite;
import 'dis/dis.dart' as dis;
import 'test/distest.dart' as distest;

const bool
  main_generate = false,
  main_test = true,
  main_run = false,
  main_other = true
;

main() => bin_main();

bin_main(){
    if(main_generate)
      dis.generate(dis.ISA_DSL);
    if(main_run)
      dis.interpret(dis.assembly[dis.currentAssembly]);
    if(main_test)
      distest.runTests();
    if(main_other)
      rewrite.other();
}
