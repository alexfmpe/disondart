part of distest;

test_stable() =>
    group('STABLE', (){

      List<String> tests = ['factorial']; //add, sleeptest, millisecs, systest]
      isStable(String ass){
        interpret(ass);
        return true;
      }

      for(String name in tests)
        test(name, () => expect(isStable(assembly[name]), equals(true)));
    });

