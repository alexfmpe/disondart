part of distest;

test_format() =>
    group('FORMAT', (){

      List      i1 = [0x0FF],
                i2 = [-0x0FF],
                i3 = [3,2,5],
                i4 = [10,20],
                r1 = [12.34],
                c1 = [97],
                s1 = ['XYZ'],
                s2 = ['ABC', 'DEF'],

                no = [];


      Map<String, List> tests = {
        'i1a' : [i1, '%d',  '255' ],
        'i1b' : [i1, '%o',  '377' ],
        'i1c' : [i1, '%x',  'ff'  ],
        'i1d' : [i1, '%X',  'FF'  ],
        'i2a' : [i2, '%#5.3x',  '-0x0ff'],
        'i3a' : [i3, '%*.*d',   ' 05'   ],
        'i4a' : [i4, '%d%d',    '1020'  ],

        'r1a' : [r1, '%8.4f',   ' 12.3400'    ],
        'r2a' : [r1, '%10.4E',  ' 1.2340E+1'  ],
        'r2b' : [r1, '%+11.4e', ' +1.2340e+1' ],

        'c1a' : [c1, '%3c', '  a'],

        's1a' : [s1, '%s', 'XYZ'  ],
        's1b' : [s1, '%-3.2s', 'XY ' ],
        's1c' : [s1, '   %s   ', '   XYZ   ' ],
        's2a' : [s2, ' %s %s ',  ' ABC DEF ' ],



        'n1a' : [no, 'abc',  'abc'],
        'n2a' : [no, 'a b c',  'a b c'],
        'n3a' : [no,  'abc ',   'abc '],
        'n4a' : [no, ' abc',   ' abc'],
        'n5a' : [no, ' abc ',  ' abc '],

        '%'   : [no, '%%%%%%', '%%%'],
      };

      for(String name in tests.keys){
        List l          = tests[name];
        FakeArgs fa     = new FakeArgs(l[0]);
        String spec     = l[1];
        String expected = l[2];
        String out      = formatOutput(spec, fa.reset());

        test(name, () => expect(out, equals(expected)));
      }
    });


class FakeArgs extends VarArgs{
  int i = 0;
  List<Object> l;

  FakeArgs(this.l) : super(null);

  advance(Type t) =>
      l[i++];

  reset() {
      i = 0;
      return this;
  }
}
