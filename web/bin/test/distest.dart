library distest;

//temp hack
import '../bin.dart';
import '../dis/dis.dart';
import 'package:unittest/unittest.dart';

part 'interpreter/assembly.dart';
part 'sys/print.dart';

runTests() => tests.forEach((t) => t());

List<Function> tests = <Function>
[
  //test_format,
  //test_stable,
];

main() => bin_main();