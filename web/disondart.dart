import 'dart:html';
import 'package:web_ui/web_ui.dart';

//for warnings
var memory;
var toHex;

@observable List<List<String>> buffer;

int pos(row,cell) => start + row * columns + cell;

update(int row, int col){
  memory[pos(row,col)] = int.parse(buffer[row][col], radix:16);
  buffer[row][col] = buffer[row][col].toUpperCase();
}


rebuffer(){
  buffer = [];
  List<String> list;
  range(0, rows).forEach((i){
    list = memory.sublist(start + i*columns, start + (i+1)*columns).map(toHex).toList();
    buffer.add(toObservable(list));
  });
  buffer = toObservable(buffer);
}


main(){
  init();
}

init(){
  rebuffer();
}

@observable
int rows  = 4;
int columns = 16;
int start = 0;


get $rows  => '$rows' ;
get $start => '$start';


set $rows (String x) => rows  = _(x);
set $start(String x) => start = _(x);

int _(String x) => int.parse(x);
String $(x) => x.toString();

//String toHex(x) => _(x).toRadixString(16).toUpperCase();

alert (x) => window.alert('$x');

List<int> range(int a, int b, [int inc = 1]) => new List.generate((b-a)~/inc, (n) => a+n*inc, growable: true);
List<String> rangeStr(int a, int b, [int inc = 1]) => range(a,b,inc).map($).toList();
List<int> indices(List list) => range(0, list.length);

int zero(n)     => 0;
int identity(n) => n;
